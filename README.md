# MMS = Model Management System

The MMS system for managing models (Model Management System) is a tool
created and designed for facilitating the more common modelling tasks
arising in project work.

MMS allows the analysis and handling of variables, the design of
models and their estimation, diagnosis or forecasting using different
techniques or mathematical algorithms. MMS also facilitates the
visualisation of results and the preparation of reports.

MMS aspires to be a modelling tool of reference and hence has been
designed seeking the flexibility and modularity necessary to adapt to
user demands and the incorporation of new techniques and modelling
concepts.

MMS has been developed in TOL and for TOL as a collection of classes
and functionalities that allow the incorporation of modelling concepts
into the programming environment. Moreover, MMS is integrated with the
graphical interface TOLBase, giving its objects characteristic icons
and context menus, and introducing specific forms for the design,
editing and visualisation of these objects.

MMS also has an object storing system that allows continued work and
the incremental maintenance of models, as well as the possibility of
sharing designs and results with other users, independently of the
project in which they have been developed.
