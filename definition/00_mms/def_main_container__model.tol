
  ////////////////////////////////////////////////////////////////////////////
  //BBL #MMS::@MainContainer
  // MODELS
  ////////////////////////////////////////////////////////////////////////////

  // * FindModel
  // * GetModel & GetModel_Load
  // * GetModels
  // * _AppendModel
  // * RemoveModel
  // * RemoveModels
  // * CreateModel
  // * ReplaceModel

  ////////////////////////////////////////////////////////////////////////////
  //BBL $FindModel%es Encuentra el �ndice de un modelo en el contenedor.
  Real FindModel(Anything info) 
  ////////////////////////////////////////////////////////////////////////////
  { MMS.FindIndexByInfo.Advanced(_.models, _DecodeInfo(info)) };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $GetModel%es Obtiene un modelo del contenedor.
  @Model GetModel(Anything info)
  ////////////////////////////////////////////////////////////////////////////
  { MMS.ObtainByInfo.Advanced(_.models, _DecodeInfo(info)) };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $GetModel_Load%es Obtiene un modelo del contenedor y en caso de \
  //BBL no encontrarlo intenta cargarlo desde la ubicaci�n indicada.
  @Model GetModel_Load(Anything info, Anything where)
  ////////////////////////////////////////////////////////////////////////////
  { GetObject_Load("Model", info, where) };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $GetModels%es Obtiene el conjunto de modelos del contenedor.
  Set GetModels(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { Copy(_.models) };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $_AppendModel%es A�ade un modelo al contenedor.
  Real _AppendModel(@Model model) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Text identifier = model::GetIdentifier(?);
    If(FindModel(identifier), {
      //BBL $_AppendModel/Error.ObjectExists
      //BBL %es Ya existe un modelo con el identificador '%1'.
      Real BBL.Error("#MMS::@MainContainer$_AppendModel/"
        <<"Error.ObjectExists", [[identifier]]);
    0}, {
      Set Append(_.models, [[PutLocalName(identifier, model)]], True);
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $RemoveModel%es Elimina un modelo del contenedor.
  Real RemoveModel(Anything info)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real index = FindModel(info);
    Real If(index, {
      @Model model = _.models[index];
      Set RemoveByIndex(_.models, index);
      Real model::_.Destroy(?);
      NameBlock model := NULL(?);
    1}, {
      //BBL $RemoveModel/Error.ObjectNotFound
      //BBL %es No se encuentra el modelo.
      Real BBL.Error("#MMS::@MainContainer$RemoveModel/"
        <<"Error.ObjectNotFound", Empty);
    0})
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $RemoveModels%es Elimina todos los modelos del contenedor.
  Real RemoveModels(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    While(Card(_.models), {
      Real RemoveModel(1);
    1});
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $CreateModel%es Crea un modelo.
  @Model CreateModel(NameBlock args)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real nerror = Copy(NError);
    @Model model = @Model::Advanced(args, _this);
    If(NError==nerror, If(_AppendModel(model), model))
  };

  ////////////////////////////////////////////////////////////////////////////
  //BBL $ReplaceModel%es Crea un modelo y en caso necesario reemplaza a \
  //BBL otro existente con el mismo identificador.
  @Model ReplaceModel(NameBlock args)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text identifier = @MainObject::ObtainIdentifier(args);
    Real If(FindModel(identifier), RemoveModel(identifier));
    CreateModel(args)
  };

  ////////////////////////////////////////////////////////////////////////////
  //! OBSOLETO
  @Model DuplicateModel(Anything info)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real MMS.Warning("OBSOLETO: Utilice el m�todo Duplicate del objeto modelo", 
      "Container::DuplicateModel");
    @Model model = GetModel(info);
    model::Duplicate(?)
  };

  ////////////////////////////////////////////////////////////////////////////
