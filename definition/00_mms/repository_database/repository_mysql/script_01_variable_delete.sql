﻿
DROP TABLE mms_c_interval_dates;
DROP TABLE mms_d_interval;

DROP TABLE mms_f_base_variable_tag;
DROP TABLE mms_f_base_variable_link;
DROP TABLE mms_d_base_variable;

DROP TABLE mms_f_variable_data_serie;
DROP TABLE mms_f_variable_data_matrix;
DROP TABLE mms_c_variable_data;
DROP TABLE mms_f_variable_tag;
DROP TABLE mms_f_variable_link;
DROP TABLE mms_d_variable;

DROP TABLE mms_d_dataset;