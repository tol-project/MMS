
//////////////////////////////////////////////////////////////////////////////
Class @VData
//////////////////////////////////////////////////////////////////////////////
{
  // Clase abstracta destinada a la definici�n de la informaci�n 
  // para la obtenci�n de los datos. 
  // Presenta una clase derivada y una variante de la que derivan otras 
  // dos clases:
  //  * @VariableD
  //  * @VDataI (variante)
  //    + @VariableI
  //    + @VScenario

  // El atributo _.type (m�todo GetType) es el tipo (gram�tica o clase) de  
  // los datos de la variable. El m�todo GetGrammar devuelve la gram�tica 
  // de los datos de la variable.
  Text _.type;
  Text _.expression;

  ////////////////////////////////////////////////////////////////////////////
  Text GetIdentifier(Real void); //V
  ////////////////////////////////////////////////////////////////////////////

  // Information relativa al tipo de datos:
  //  * Text GetType
  //  * Text GetGrammar
  //  * Real IsRandom

  ////////////////////////////////////////////////////////////////////////////
  Text GetType(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Copy(_.type) };

  ////////////////////////////////////////////////////////////////////////////
  Real SetType(Text type)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Case(type==_.type, {
      // Si el tipo queda igual no se hace nada
    0}, type <: @VData::GetType.Possibilities(?), {
      Text _.type := type;
      //# Real MMS.Trace_OnChange(ClassOf(_this)<<"::SetType");
      Real _OnChange(?);
    1}, True, {
      Real MMS.Error("Tipo de datos '"<<type<<"' no v�lido",
        "@VData::SetType");
    0})
  };

  ////////////////////////////////////////////////////////////////////////////
  Text GetGrammar(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_.type <: Grammars(?), Copy(_.type), {
      Text aux = Replace(_.type, "RandVar::@", "");
      Tokenizer(aux, ".")[1]
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real IsRandom(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Not(_.type <: Grammars(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Text GetExpression(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Copy(_.expression) };

  ////////////////////////////////////////////////////////////////////////////
  Text _GetExpression_DeclaredGrammar(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_.type <: [["Serie", "Matrix"]] & TextLength(_.expression), {
      If(TextSub(_.expression, 1, TextLength(_.type)+1)!=(_.type<<" "), {
        _.type<<" "<<_.expression
      }, _.expression)
    }, _.expression)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real SetExpression(Text expression)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Text newExpression = @VData::AsStandardExpression(expression);
    If(_.expression==newExpression, {
      // Si la expresi�n queda igual no se hace nada
    0}, {
      Text _.expression := newExpression;
      //# Real MMS.Trace_OnChange(ClassOf(_this)<<"::SetExpression");
      Real _OnChange(?);
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  // Funciones para obtener datos:
  //  * GetData[.]
  //  * GetData.Extended[.]
  // => heredados por @VariableI, @VariableD y @VScenario

  ////////////////////////////////////////////////////////////////////////////
  Anything GetData(Real void) { GetData.(?)[1] };
  ////////////////////////////////////////////////////////////////////////////
  Set GetData.(Real void); //V
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  Anything GetData.Extended(Real void) { GetData.Extended.(?)[1] };
  ////////////////////////////////////////////////////////////////////////////
  Set GetData.Extended.(Real void); //V
  ////////////////////////////////////////////////////////////////////////////

  // Information relativa a los datos:

  ////////////////////////////////////////////////////////////////////////////
  @Interval GetInterval(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Interval::Default(GetDomain(?), GetBegin(?), GetEnd(?))
  }; 

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.GetDomain = 
    "Espacio de dominio sobre el que se definen los datos.\n"
    "Para las series temporales (Serie) es el fechado (TimeSet).";
  Anything GetDomain(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text grammar = GetGrammar(?);
    If(grammar=="Serie", {
      @Anything data = @Anything(GetData(?));
      If(RandVar::IsRandom($data), $data::GetDating(?), Dating($data))
    }, {
      @Anything data = @Anything(GetData(?));
      Real rows = If(RandVar::IsRandom($data), $data::GetRows(?), 
        Rows($data)); 
      Real columns = If(RandVar::IsRandom($data), $data::GetColumns(?), 
        Columns($data));
      Text name = "C."<<rows<<"x"<<columns;
      VMatrix domain = Zeros(rows, columns);
      PutName(name, domain)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.GetDomainName = 
    "Nombre del espacio de dominio sobre el que se definen los datos.\n"
    "Para las series temporales coinicide con el nombre de su fechado.";
  ////////////////////////////////////////////////////////////////////////////
  Text GetDomainName(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Name(GetDomain(?)) };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.GetBegin = 
    "Posici�n del primer dato. Es una fecha para Serie y una matriz fila \n"
    "con el par (fila,columna) para Matrix.";
  ////////////////////////////////////////////////////////////////////////////
  Anything GetBegin(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text grammar = GetGrammar(?);
    Anything begin = Case(grammar=="Serie", {
      @Anything data = @Anything(GetData(?));
      If(RandVar::IsRandom($data), $data::_.begin, First($data))
    }, grammar=="Matrix", {
      Matrix Row(1,1)
    }, True, ?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _.autodoc.member.GetEnd = 
    "Posici�n del �ltimo dato. Es una fecha para Serie y una matriz fila \n"
    "con el par (fila,columna) para Matrix.";
  ////////////////////////////////////////////////////////////////////////////
  Anything GetEnd(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text grammar = GetGrammar(?);
    Anything end = Case(grammar=="Serie", {
      @Anything data = @Anything(GetData(?));
      If(RandVar::IsRandom($data), $data::_.end, Last($data))
    }, grammar=="Matrix", {
      @Anything data = @Anything(GetData(?));
      Matrix Row(Rows($data), Columns($data))
    }, True, ?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetStatistics(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set statistics = DataStatistics(GetData(?));
    PutName(GetIdentifier(?), statistics)
  };

  // Otras:

  ////////////////////////////////////////////////////////////////////////////
  Real _OnChange(Real void); //V
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  // Auxiliares:

  ////////////////////////////////////////////////////////////////////////////
  Static Text ObtainTypeFromExpression(Text expression)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text firstWord = Tokenizer(TextTrim(expression), " ")[1];
    If(firstWord <: @VData::GetType.Possibilities(?), firstWord, {
      Real MMS.Error("No se puede resolver el tipo de datos a partir de la "
        <<"expresi�n de la variable. Ind�quese expl�citamente mediante el "
        <<"argumento _.type", "@VData::ObtainTypeFromExpression");
      If(False, ?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Text AsStandardExpression(Text expression)
  ////////////////////////////////////////////////////////////////////////////
  {
    // El car�cter de escape por defecto es el %.
    // Si quiere escaparse para usarlo como operador ha de duplicarse.
    // Se modifica el uso compatible anterior con los caracteres '�' y '_':
    Text expression2 = TextReplaceEscapeChars(expression, [["�", "_"]], "%");
    TextTrim.(expression2, [[";", " ", "\n", "\t"]])
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set GetType.Possibilities(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    [[ "Serie",
       "RandVar::@Serie.Random", "RandVar::@Serie.Sample", 
         "RandVar::@Serie.Normal", "RandVar::@Serie.BoxCoxNormal", 
         "RandVar::@Serie.ARIMA",
       "Matrix"
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set DAt.GetSpc(Set dAt)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text name = Name($dAt);
    Text type = GrammarOrClass($dAt);
    Set object. = Case(TextFind(type, "@"), {
      If(IsInstanceOf($dAt, "RandVar::@Grammar.Random"), {
        ($dAt)::GetSpc(name)
      }, {
        Real MMS.Error("No se sabe almacenar una instancia de: "
          <<ClassOf($dAt), "@VData::DAt.GetSpc");
        If(False, ?)
      })
    }, type=="Serie", {
      If(ObjectExist("TimeSet", "::"<<DatingName($dAt)), {
        MatSerSet(SerMat($dAt), Eval("::"<<DatingName($dAt)), First($dAt))
      }, [[$dAt]])
    }, True, [[$dAt]]);
    // Se intenta evitar guardar variables insanas recolocando su fechado.
    Set Data. = [[
      Text Name = name;
      Text Type = type;
      Set Object. = object.
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Set DAt.Spc(Set data.)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text name = data.::Name;
    Text type = data.::Type;
    @Anything dAt = If(TextFind(type, "@"), {
      @Anything(Eval(type<<"::Spc(data.::Object.)"))
    }, {
      @Anything(data.::Object.[1])
    });
    Anything { PutName(name, dAt[1]) };
    dAt
  }
};
//////////////////////////////////////////////////////////////////////////////
