
  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.First(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real maxDegree = GetMaximumDegree(?);
    Anything first = GetFirst(?); // == parent::GetFirst(?)
    If(Grammar(first)=="Date" & maxDegree,
      Date Succ(first, GetDomain(?), -maxDegree), first)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Last(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real minDegree = GetMinimumDegree(?);
    Anything last = GetLast(?); // == parent::GetLast(?)
    If(Grammar(last)=="Date" & minDegree, 
      Date Succ(last, Eval(GetDomainName(?)), -minDegree), last)
  };

  ////////////////////////////////////////////////////////////////////////////
  // El filtro del t�rmino explicativo es el factor que es afectado 
  // del par�metro lineal (o polinomio omega). Es el valor del t�rmino 
  // explicativo cuando el par�metro lineal (o polinomio omega) vale 1.
  // En el caso de un t�rmino explicativo lineal, el filtro coincide con  
  // el input en el intervalo de definici�n del submodelo.
  Anything GetInitialFilter(Real void) { GetInitialFilter.(?)[1] };
  ////////////////////////////////////////////////////////////////////////////
  Set GetInitialFilter.(Real void) { SubData.(GetInitialFilter.Full.(?)) };
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  Anything GetInitialFilter.Full(Real void) { GetInitialFilter.Full.(?)[1] };
  ////////////////////////////////////////////////////////////////////////////
  // Set GetInitialFilter.Full.(Real void); (particular para cada t�rmino)
  ////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////////////////////////////
  Anything GetInitialData(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Anything filter0 = GetInitialFilter.(?);
    Text grammar = Grammar($filter0);
    Case(grammar=="Serie", {
      Serie GetInitialOmega(?) : $filter0
    }, grammar=="Matrix", {
      @ParameterLinear parameterLinear = GetParameterLinear(1);
      Matrix $filter0 * parameterLinear::GetInitialValue(?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Set _GetEffectiveFilter_Parameter.(Real index)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Devuelve el input (o filtro de t�rmino explicativo) efectivo 
    // para el par�metro indicado
    @Anything filterData = GetInitialFilter.(?);
    @ParameterLinear parameter = GetParameterLinear(index);
    Real degree = parameter::GetDegree(?);
    If(degree!=0 & Grammar($filterData)=="Serie", {
      Date first = GetFirst(?);
      Date last = GetLast(?);
      Polyn monome = If(degree>=0, B**degree, F**Abs(degree));
      @Serie(SubSer(monome:(Serie $filterData), first, last))
    }, filterData)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set _CheckRegularity_Parameter(Real index, Real showMode)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ParameterLinear parameter = GetParameterLinear(index);
    // Test del par�metro
    If(parameter::IsIndependent(?), {
      // Test del input (o filtro de t�rmino explicativo) efectivo asociado
      @Anything effFilter = _GetEffectiveFilter_Parameter.(index);
      Real min = DAtMin(effFilter);
      Real max = DAtMax(effFilter);
      Real all0 = If(IsUnknown(min) | IsUnknown(max), False, min==0 & max==0);
      If(all0, {
	    Real degree = parameter::GetDegree(?);
        Real If(show, MMS.Error("El input del t�rmino '"<<GetIdentifier(?)
          <<"' es completamente nulo en el intervalo de uso\n"
          <<"  para el par�metro de grado "<<degree<<".", 
          "@ExpTermLinear::CheckRegularity"));
        Set [[ PutLocalName(parameter::GetIdentifier(?), "Input:AllZeros") ]]
      }, Copy(Empty))
   }, Copy(Empty))
  };

  ////////////////////////////////////////////////////////////////////////////
  Set CheckRegularity(Real showMode)
  ////////////////////////////////////////////////////////////////////////////
  {
    // showMode: Indica si mostrar (101) o no (100) los errores, las
    //   advertencias o mensajes. Por defecto se asume que se mostrar�n.
    Real show = If(SameReal(showMode, 100), False, True);
    // El chequeo de regularidad comprueba que el input no es cero en todo el
    // intervalo de uso, y de serlo al menos el par�metro correspondiente
    // a cada intervalo (considerando los retardos) no es independiente.
    // Un t�rmino explicativo omega puede verse como una combinaci�n lineal
    // de t�rminos explicativos de un s�lo par�metro. El t�rmino es regular
    // si todos los subt�rminos lo son.
    // De acuerdo al convenio de las funciones de chequeo se devuelve
    // un conjunto con un texto indicativo del problema cuyo nombre
    // ser� el identificador del objeto que lo produce.
    // La respuesta a "se encuentran problemas" se obtiene con el Card
    // de la respuesta: si �ste es 0 indica que pasa el chequeo.
    // V�ase el m�todo: <submodel>::CheckMulticollinearity
    Set parametersLinear = GetParametersLinear(?);
    If(Card(parametersLinear)==0, {
      Real If(show, MMS.Error("El t�rmino '"<<GetIdentifier(?)
        <<"' no tiene par�metros.", "@ExpTermOmega::CheckRegularity"));
      Set [[ PutLocalName(GetIdentifier(?), "NoParameters") ]]
    }, {
      SetConcat(For(1, Card(parametersLinear), Set (Real index) {
        _CheckRegularity_Parameter(index, showMode)
      }))
    })
  };
