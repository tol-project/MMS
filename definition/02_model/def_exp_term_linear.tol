
//////////////////////////////////////////////////////////////////////////////
Class @ExpTermLinear : @ExpTerm
//////////////////////////////////////////////////////////////////////////////
{
  //H: Set _parent_; //@Submodel
  Static Text _.subclass = "Linear";
  //H: Text _.name;
  //H: Text _.description;
  //H: Real _.isActive;
  //H: Real _.isAdditive;
  //H: Set _.input_;
  //H: Set _.parametersLinear;

  #Embed "def_exp_term__uni_input.tol";
  #Embed "def_exp_term__multi_linear.tol";
  #Embed "def_exp_term__regularity.tol"; 

  ////////////////////////////////////////////////////////////////////////////
  Set GetInitialFilter.Full.(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock input = GetInput(?);
    input::GetData.(100)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetInput.First(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { GetFilter.First(?) };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetInput.Last(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { GetFilter.Last(?) };

  ////////////////////////////////////////////////////////////////////////////
  Real _OnChangeIdentifier_Childs(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Se actualizan los identificadores de los objetos hijos:
    Set EvalSet(_.parametersLinear, Real (@Parameter parameter) {
      parameter::_.UpdateIdentifier(?)
    });
    Real 1
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetSpecification(Real mode)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Por compatibilidad con versiones anteriores
    Real maxDegree = GetMaximumDegree(?);
    Real minDegree = GetMinimumDegree(?);
    Text subclass = If(maxDegree==0 & minDegree==0, "Omega", "Linear");
    Set ExpTermLinear. = [[
      Text Subclass = subclass;
      Text Name = _.name;
      Text Description = _.description;
      Real IsActive = _.isActive;
      Real IsAdditive = _.isAdditive;
      Set Inputs = _GetLinkSet.Spc(_.input_);
      Set ParametersLinear = EvalSet(_.parametersLinear, 
        Set (NameBlock obj) { obj::GetSpecification(mode) })   
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermLinear Spc(Set expTerm., @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTermLinear expTerm = @ExpTermLinear::New([[
      Text _.name = expTerm.::Name;
      Text _.description = expTerm.::Description;
      Real _.isActive = expTerm.::IsActive;
      Real _.isAdditive = If(ObjectExist("Real", "expTerm.::IsAdditive"), 
        expTerm.::IsAdditive, False)
    ]], parent);
    Real If(Card(expTerm.::Inputs), expTerm::SetInput(expTerm.::Inputs[1]));
    Set EvalSet(expTerm.::ParametersLinear, 
      @ParameterLinear (Set parameter.) {
      expTerm::CreateParameterLinear_Spc(parameter.)
    });
    expTerm
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermLinear Advanced(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(ObjectExist("Polyn", "args::_.omega") 
      | ObjectExist("Polyn", "args::_.transferFunction"), {
      @ExpTermLinear::Omega(args, parent)
    }, {
      @ExpTermLinear::Linear(args, parent)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermLinear Linear(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTermLinear expTermLinear = @ExpTermLinear::New(args, parent);
    Real Case(ObjectExist("NameBlock", "args::_.input"), {
      expTermLinear::SetInput_Create(args::_.input)
    }, ObjectExist("Text", "args::_.inputName"), {
      expTermLinear::SetInput(args::_.inputName)
    });
    Real If(ObjectExist("Real", "args::_.coefficient"), {
      NameBlock expTermLinear
        ::CreateParameterLinear_Coefficient(args::_.coefficient);
    1});
    expTermLinear
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermLinear Omega(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTermLinear expTerm = @ExpTermLinear::New(args, parent);
    Real Case(ObjectExist("NameBlock", "args::_.input"), {
      expTerm::SetInput_Create(args::_.input)
    }, ObjectExist("Text", "args::_.inputName"), {
      expTerm::SetInput(args::_.inputName)
    });
    Set parameters = Case(ObjectExist("Polyn", "args::_.omega"), {
      expTerm::CreateParametersLinear_Omega(args::_.omega)
    }, ObjectExist("Polyn", "args::_.transferFunction"), {
      expTerm::CreateParametersLinear_Omega(args::_.transferFunction)
    }, True, Copy(Empty));
    Real If(ObjectExist("Real", "args::_.fixed"), {
      If(args::_.fixed, {
        Set EvalSet(parameters, Real (@ParameterLinear parameter) {
          parameter::SetIsFixed(True)
        });
      1}, 0)
    }, 0);
    expTerm
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermLinear New(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real nerror = Copy(NError);
    Real isAdditive = getOptArg(args, "_.isAdditive", False);
    Real Case(Not(isAdditive), 0, parent::GetSubclass(?)=="D", {
      Real MMS.Warning("Los submodelos de output discreto no admiten "
        "t�rminos aditivos puros.", "@ExpTermLinear:New");
      isAdditive := False
    });      
    @ExpTermLinear expTermLinear = [[
      Set _parent_ = [[parent]];
      Text _.name = MMS.PrepareName(args::_.name, "@ExpTermLinear::New");
      Text _.description = getOptArg(args, "_.description", "");
      Real _.isActive = getOptArg(args, "_.isActive", True);
      Real _.isAdditive = isAdditive;
      Set _.parametersLinear = Copy(Empty);
      Set _.input_ = Copy(Empty)
    ]];
    If(NError==nerror, expTermLinear)
  }
};
//////////////////////////////////////////////////////////////////////////////
Class @ExpTermOmega : @ExpTermLinear
//////////////////////////////////////////////////////////////////////////////
{
  Static Text _.subclass = "Linear"
};
//////////////////////////////////////////////////////////////////////////////
