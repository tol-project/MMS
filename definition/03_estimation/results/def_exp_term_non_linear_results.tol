//////////////////////////////////////////////////////////////////////////////
// FILE:    def_exp_term_non_linear_results.tol
// PURPOSE: Define la clase para manejar los resultados relativos a
//          los términos explicativos no lineales.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @ExpTermNonLinear.Results : @ExpTerm.Results
//////////////////////////////////////////////////////////////////////////////
{
  //H: Set _expTerm_; //@ExpTerm

  //!L [Limitación]
  //!L Los términos no lineales no admiten parámetros de omitidos

  //!L Set _.parametersMissing = Copy(Empty);
  //H: Set _.parametersLinear = Copy(Empty);
  Set _.parametersNonLinear = Copy(Empty);

  // Métodos:
  // H GetIdentifier ,, GetName
  // H GetFirst ,, GetLast
  // * GetParametersLinear
  // * GetParametersMissing
  // * GetOmega
  // * GetFilter.Name
  // * GetFilter.First ,, GetFilter.Last

  // Resultados:
  // * GetFilter [|.Extended]
  // * GetData   [|.Extended]

  //  Parámetros:

  //H: Real FindParameterLinear(Anything info);
  //H: @Parameter.Results GetParameterLinear(Anything info);
  //H: Set GetParametersLinear(Real void);
  //H: Real FindParameterMissing(Anything info);
  //H: @Parameter.Results GetParameterMissing(Anything info);
  //H: Set GetParametersMissing(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Set GetParametersNonLinear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Copy(_.parametersNonLinear) };

  ////////////////////////////////////////////////////////////////////////////
  Polyn GetOmega(Real void) //[TF]
  ////////////////////////////////////////////////////////////////////////////
  {
    SetSum(EvalSet(GetParametersLinear(?), Polyn (@Parameter.Results p) { 
      p::GetMonomial(?)
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real GetMaximumDegree(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { (_expTerm_[1])::GetMaximumDegree(?) };

  ////////////////////////////////////////////////////////////////////////////
  Real GetMinimumDegree(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { (_expTerm_[1])::GetMinimumDegree(?) };

  //H: Real IsScenariosSensitive

  //H: Text GetInput.Name
  //H: Anything GetInput.First
  //H: Anything GetInput.Last
  //H: Anything GetInput ,, Set GetInput.
  //H: Anything GetInput.Extended ,, Set GetInput.Extended.
  //H: Anything GetInput_Scenarios
  //H: Anything GetInput.Original
  //H: Anything GetInput.Original.Extended
  //H: Anything GetInput.Missings

  ////////////////////////////////////////////////////////////////////////////
  Text GetFilter.Name(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { (_expTerm_[1])::GetIdentifier(?)<<"_Filter" };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.First(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { (_expTerm_[1])::GetFilter.First(?) };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Last(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { (_expTerm_[1])::GetFilter.Last(?) };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    @Anything filterE = @Anything(GetFilter.Extended(?));
    @Anything filter = DAtSub(filterE, GetFilter.First(?), GetFilter.Last(?));
    PutName(GetFilter.Name(?), $filter)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Extended(Real void) { _UseCache(_GetFilter.Extended) };
  Anything _GetFilter.Extended(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NonLinearFilter nonLinearFilter = (_expTerm_[1])::_.nonLinearFilter[1];
    //!! estamos obviando el orden
    Set parameters = EvalSet(GetParametersNonLinear(?), 
      Real (@ParameterNonLinear p) { p::GetValue(?) });
    //! Los inputs intervienen completos en el filtro
    Set inputs = [[ GetInput.Extended.(?)[1] ]];
    @Anything filterE =
      @Anything(nonLinearFilter::Filter(parameters, inputs));
    PutName(GetFilter.Name(?), $filterE)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter_Scenarios(Set scenarios)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real sensitive = IsScenariosSensitive(scenarios);
    If(sensitive, {
      @NonLinearFilter nonLinearFilter = (_expTerm_[1])::_.nonLinearFilter[1];
      //!! estamos obviando el orden
      Set parameters = EvalSet(GetParametersNonLinear(?), 
        Real (@ParameterNonLinear p) { p::GetValue(?) });
      //! Los inputs intervienen completos en el filtro
      Set inputs = [[ GetInput_Scenarios(scenarios) ]];
      nonLinearFilter::Filter(parameters, inputs)
    }, GetInput.Extended(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Original(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { GetFilter(?) };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Original.Extended(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { GetFilter.Extended(?) };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFilter.Missings(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    @Anything filter = @Anything(GetFilter(?));
    @Anything filterO = @Anything(GetFilter.Original(?));
    @Anything filterM = @Anything(DataGetMissings($filter, $filterO));
    PutName(GetFilter.Name(?)<<"_Missings", $filterM)
  };

  // Datos:

  //H: Anything GetData(Real void);
  //H: Set GetData.(Real void);
  //H: Anything GetData.Extended(Real void);
  ////////////////////////////////////////////////////////////////////////////
  Set GetData.Extended.(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie filterE = @Serie(GetFilter.Extended(?));
    Polyn omega = GetOmega(?); //[TF]
    @Serie dataE = @Serie(omega : $filterE);
    Serie PutName(GetIdentifier(?), $dataE);
    dataE
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetData_Scenarios(Set scenarios)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie filterS = @Serie(GetFilter_Scenarios(scenarios));
    Polyn omega = GetOmega(?); //[TF]
    @Serie dataS = @Serie(omega : $filterS);
    PutName(GetIdentifier(?), $dataS)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetData_Scenarios.Difference(Set scenarios)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie filterE = @Serie(GetFilter.Extended(?));
    @Serie filterS = @Serie(GetFilter_Scenarios(scenarios));
    Polyn omega = GetOmega(?); //[TF]
    Serie omega : ($filterS - $filterE)
  };

  //H: Anything GetData.Interrupted(Real void);
  ////////////////////////////////////////////////////////////////////////////
  Anything GetData.Interrupted.Extended(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Serie filterOE = @Serie(GetFilter.Original.Extended(?));
    Polyn omega = GetOmega(?); //[TF]
    @Serie dataIE = @Serie(omega : $filterOE);
    PutName(GetIdentifier(?), $dataIE)
  };

  //H: Anything GetData.Missings(Real void);

  ////////////////////////////////////////////////////////////////////////////
  // Inicialización

  //H: Real _.Initialize(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Set _GetChilds(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.parametersLinear << _.parametersNonLinear //!L << _.parametersMissing
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _BuildChilds(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _InitializeParametersLinear(?);
    Real _InitializeParametersNonLinear(?);
    //!L Real _InitializeParametersMissing(?);
    2
  };

  //H: Real _InitializeParametersLinear(Real void);
  //!L Real _InitializeParametersMissing(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Real _InitializeParametersNonLinear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {    
    @Model.Results model.R = GetModel(?);
    Set parameters = (_expTerm_[1])::GetParametersNonLinear(?), 
    Set _.parametersNonLinear := EvalSet(parameters, 
      @Parameter.Results (@ParameterLinear parameter) {
      @Parameter.Results parameter.R = 
        @Parameter.Results::Default(_this, parameter);
      Real model.R::_.AppendParameter(parameter.R);
      WithName(parameter.R::GetName(?), parameter.R)
    });
    If(Card(_.parametersNonLinear), SetIndexByName(_.parametersNonLinear), 0)
  };

  //H: Real _.InitializeCache.Results(Real void);
  //H: Real _InitializeResults(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermNonLinear.Results Default(@Submodel.Results parent, 
    @ExpTerm expTerm)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Text class = ClassName.AsReference("@ExpTermNonLinear", parent, ".");
    NameBlock expTerm.R = Instance(class, [[ 
      Set _parent_ = [[parent]];
      Set _expTerm_ = [[expTerm]]
    ]]);
    Real expTerm.R::_.Initialize(?);
    PutName(expTerm.R::GetName(?), expTerm.R)
  }
};
//////////////////////////////////////////////////////////////////////////////
