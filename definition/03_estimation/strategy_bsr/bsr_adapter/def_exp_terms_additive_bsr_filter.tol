
//////////////////////////////////////////////////////////////////////////////
Class @ExpTerms.Additive.BSRFilter : @ObjectCache
//////////////////////////////////////////////////////////////////////////////
{
  Set _submodel_;
  Set _expTerms_;
  Text _segment;
  Set _interval; // Intervalo del submodelo
  Matrix __parameters; // Matriz de par�metros (atributo din�mico)

  ////////////////////////////////////////////////////////////////////////////
  // Identifica al filtro (obligatorio)
  Text get.name(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    "AdditiveBlk::"<<(_submodel_[1])::GetIdentifier(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Identifica al segmento (obligatorio)
  Text get.segmentName(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { _segment };

  ////////////////////////////////////////////////////////////////////////////
  // Obtiene la informaci�n de los par�metros no lineales (auxiliar)
  Set get.params(Real void)
  { _UseCache(_get.params) };
  ////////////////////////////////////////////////////////////////////////////
  Set _get.params(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Set effectives = ObtainEffectiveMObjects_Unique(SetConcat(
      EvalSet(_expTerms_, Set (@ExpTerm expTerm) {
      expTerm::GetParametersLinear(?)
    })));
    EvalSet(effectives, Set (@MObjectPrior effective) {
      MMS.Get.Bsr.Param.Info(effective)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  // Obtiene los nombres de los par�metros no lineales (obligatorio)
  Set get.colNames(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    EvalSet(get.params(?), Text(Set info) { info[1] })
  };

  ////////////////////////////////////////////////////////////////////////////
  // Inicializa el filtro (obligatorio)
  Real initialize(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix parameters0 = SetCol(SetConcat(EvalSet(_expTerms_, 
      Set (@ExpTerm expTerm) { 
      EvalSet(expTerm::GetParametersLinear(?),
        Real (@ParameterLinear parameter) { parameter::GetInitialValue(?) })
    })));
    Matrix __parameters := parameters0;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  // Obtiene los valores iniciales de los par�metros no lineales (obligatorio)
  // Se llama justo tras inicializar el filtro
  Matrix get.parameter(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { __parameters };

  ////////////////////////////////////////////////////////////////////////////
  // Actualiza el atributo din�mico __parameter (auxiliar)
  Real set.parameter(Matrix parameters)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Matrix __parameters := parameters;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix get.output(Real void) { _UseCache(_get.output) };
  ////////////////////////////////////////////////////////////////////////////
  Matrix _get.output(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @MVariable output = (_submodel_[1])::GetOutput(?);
    Anything data = output::GetVariable(?)::GetData(?);
    Matrix MMS.Get.Data.Matrix(data, _interval[1], _interval[2])
  };

  ////////////////////////////////////////////////////////////////////////////
  // Obtiene los inputs como una matriz �nica por columnas (auxiliar)
  Matrix get.input(Real void) { _UseCache(_get.input) };
  ////////////////////////////////////////////////////////////////////////////
  Matrix _get.input(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    BinGroup("|", EvalSet(_expTerms_, Matrix (@ExpTerm expTerm) {
      @MVariable input = expTerm::GetInput(?);
      Set degrees = EvalSet(expTerm::GetParametersLinear(?),
        Real (@ParameterLinear parameter) { parameter::GetDegree(?) });
      Anything data = input::GetVariable(?)::GetData(?);
      Case(Grammar(data)=="Serie", {
        MMS.AsFilterMatrix(data, _interval[1], _interval[2], degrees)
      }, Grammar(data)=="Matrix", {
        Real If(Card(degrees)>1 | degrees[1]!=0, {
          Real MMS.Warning("Se han encontrado par�metros lineales de grado "
            <<"distinto de 0 en un filtro no lineal de tipo Matrix.", 
            "@ExpTerms.Additive.BSRFilter::eval")
        });
        MMS.Get.Data.Matrix(data , _interval[1], _interval[2])
      }, True, {
        Real MMS.Error("Gram�tica de los datos ("<<Grammar(data)
          <<") inesperada.", "@ExpTerms.Additive.BSRFilter::eval");
        If(False, ?)
      })
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  // Devuelve el intervalo de dominio del par�metro i-�simo para un 
  // determinado conjunto de valores de los par�metros (obligatorio)
  Set get.bounds(Real index, Matrix paramValues) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Set info = get.params(?)[index];
    Set localBounds = GetBoundsInPolytope(Mat2VMat(get.input(?)), 
      Mat2VMat(get.output(?)), Mat2VMat(paramValues), index);
    If(Card(localBounds), {
      Real lower = Max(info->Prior.LowerBound, localBounds::Lower);
      Real upper = Min(info->Prior.UpperBound, localBounds::Upper);
      [[lower, upper]]
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  // Devuelve la matriz del filtro para un determinado conjunto de valores
  // de los par�metros (obligatorio)
  // Este m�todo ha de actualizar el atributo din�mico para que pueda
  // ser utilizado por el m�todo: get.priorLogDens
  Matrix eval(Matrix paramValues)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real set.parameter(paramValues);
    // Los par�metros llegan en una columna
    // Los inputs llegan en una matriz por columnas
    Set output. = [[ get.output(?) ]];
    Set input. = [[ get.input(?) ]];
    NameBlock t = (_submodel_[1])::GetTransformation(?);
    t::Direct(output.[1]) - t::Direct(output.[1] - input.[1] * paramValues)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Obtiene la distribuci�n a priori de los par�metros (auxiliar)
  Set get.priors(Real void) { _UseCache(_get.priors) };
  ////////////////////////////////////////////////////////////////////////////
  Set _get.priors(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    SetConcat(EvalSet(_expTerms_, Set (@ExpTerm expTerm) {
      EvalSet(expTerm::GetParametersLinear(?),
        Set (@ParameterLinear parameter) {
        If(parameter::HasPrior(?), {
          @Constraint prior = parameter::GetPrior(?);
         If(prior::IsActive(?), {[[
            Real Mean = prior::GetMean(?);
            Real Sigma = prior::GetSigma(?)
          ]]}, Copy(Empty))
        }, Copy(Empty))
      })
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  // Devuelve la evaoluaci�n del log de la funci�n de densidad del prior 
  // evaluada para el par�metro i-�simo (opcional)
  Real get.priorLogDens(Real index)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set priors = get.priors(?);
    If(Card(priors[index]), {
      Real logDens = Log(DensNormal(MatDat(__parameters, index, 1), 
        priors[index]["Mean"], priors[index]["Sigma"]));
      If(IsUnknown(logDens), Real -10**10, logDens)
    }, 0)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTerms.Additive.BSRFilter Default(@Submodel submodel,
    Set expTerms, Text segment, Set interval)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTerms.Additive.BSRFilter expTermAdditiveFilter = [[
      Set _submodel_ = [[submodel]];
      Set _expTerms_ = expTerms;
      Text _segment = segment;
      Set _interval = interval; // Intervalo del output
      Matrix __parameters = Rand(0,0,0,0) // Atributo din�mico
    ]]
  }
};
//////////////////////////////////////////////////////////////////////////////
