
//////////////////////////////////////////////////////////////////////////////
Class @Submodel.BSRImport : @ObjectCache
//////////////////////////////////////////////////////////////////////////////
{
  Set _submodel_;

  ////////////////////////////////////////////////////////////////////////////
  // API del Import (*:standard, �:(_mode), +:primary, -:joint)
  //  * Text Get.Name()
  //  * (Text|Real) Get.Sigma2()
  //  * Text Get.SigmaPrior()
  //  * (@BSR.NoiseTimeInfo|Empty) Get.TimeInfo()
  //  * Real Get.ARIMA.Size()
  //  * @ARIMAStruct Get.ARIMA.Factor()
  //  * Text Get.Cov() -- opcional
  //  � Real Get.Param.Size()
  //  � @Bsr.Param.Info Get.Param(Real iP)
  //  * Real Get.Missing.Size()
  //  * @Bsr.Missing.Info Get.Missing(Real iP)
  //  * Real Get.Equation.Size()
  //  - (Text|Real) Get.Equation.Output(Real iE)
  //  - Real Get.Equation.Input.Size(Real iE)
  //  - (Text|Real) Get.Equation.Input.Coef(Real iE, Real iT)
  //  - Text Get.Equation.Input.Param(Real iE, Real iT)
  //  + VMatrix Get.OutputVMatrix()
  //  + VMatrix Get.InputVMatrix()
  //  * NameBlock Get.Constraints.Handler()
  //  + Set{NameBlock} Get.NonLinearFilters()

  ////////////////////////////////////////////////////////////////////////////
  // Nombre del segmento
  Text Get.Name(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    (_submodel_[1])::GetIdentifier(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Identificador (Text) o valor (Real) para la sigma2 del segmento
  Anything Get.Sigma2(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // El par�metro de dispersi�n en los submodelos de respuesta cualitativa
    // GLM para una distribuci�n binomial se considera 1
    Text type = (_submodel_[1])::GetType(?);
    Case(type=="Linear", {
      @ParameterSigma2 sigma2 = 
        (_submodel_[1])::GetNoise(?)::GetParameterSigma2(?);
      "SigmaBlk::"<<(_submodel_[1])::GetIdentifier(?)<<"::sigma2"
    }, type<:[["Probit", "Logit"]], {
      Real 1.0
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  // Devuelve la expresi�n del prior en la sigma2
  Text Get.SigmaPrior(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text type = (_submodel_[1])::GetType(?);
    Case(type=="Linear", {
      @ParameterSigma2 sigma2 = 
        (_submodel_[1])::GetNoise(?)::GetParameterSigma2(?);
      Real initialSigma2 = sigma2::GetInitialValue(?);
      Real sigmaWeight = sigma2::GetIsFixed(?);
      "@Bsr.Sigma.Prior.InverseGamma("<<initialSigma2<<","<<sigmaWeight<<")"
    }, type<:[["Probit", "Logit"]], {
      Text ""
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Set Get.IntervalInfo(Real void) { _UseCache(_Get.IntervalInfo) };
  ////////////////////////////////////////////////////////////////////////////
  Set _Get.IntervalInfo(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text grammar = (_submodel_[1])::GetGrammar(?);
    Case(grammar=="Serie", {
      Serie data = (_submodel_[1])::GetData(?);
      Date first = (_submodel_[1])::GetFirst(?);
      Date last = (_submodel_[1])::GetLast(?);
      Text datingName = DatingName(data);
      Real If(Not(ObjectExist("TimeSet", datingName)), {
        TimeSet MakeGlobal(Dating(data));
      1});
      [[ Date first, Date last, TimeSet Dating(data), 
        Set @BSR.NoiseTimeInfo(Dating(data), first, last) ]]
    }, grammar=="Matrix", {
      Matrix first = (_submodel_[1])::GetFirst(?); //! relativas al dominio
      Matrix last = (_submodel_[1])::GetLast(?); //! relativas al dominio
      @VMatrix domain. = @VMatrix((_submodel_[1])::GetDomain(?));
      [[ Matrix first, Matrix last, VMatrix domain.[1], Set Copy(Empty) ]]
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetFirst(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Get.IntervalInfo(?)[1] };

  ////////////////////////////////////////////////////////////////////////////
  Anything GetLast(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { Get.IntervalInfo(?)[2] };

  ////////////////////////////////////////////////////////////////////////////
  // Si el segmento es temporal retorna informacion asociada al segmento
  Set Get.TimeInfo(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Get.IntervalInfo(?)[4]
  };

  ////////////////////////////////////////////////////////////////////////////
  Set Get.ARIMA(Real void) { _UseCache(_Get.ARIMA) };
  ////////////////////////////////////////////////////////////////////////////
  Set _Get.ARIMA(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text type = (_submodel_[1])::GetType(?);
    Case(type=="Linear", {
      @Noise noise = (_submodel_[1])::GetNoise(?);
      If(noise::GetSubclass(?)!="ARIMA", Copy(Empty), 
        noise::GetInitialARIMA(?))
    }, type<:[["Probit", "Logit"]], {
      Copy(Empty)
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  // Retorna el n�mero de factores ARIMA del segmento
  Real Get.ARIMA.Size(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Card(Get.ARIMA(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  // Retorna el f-�simo factor ARIMA del segmento
  Set Get.ARIMA.Factor(Real f)
  ////////////////////////////////////////////////////////////////////////////
  {
    Get.ARIMA(?)[f] 
  };

  ////////////////////////////////////////////////////////////////////////////
  Text Get.Cov(Real void) { _UseCache(_Get.Cov) };
  ////////////////////////////////////////////////////////////////////////////
  Text _Get.Cov(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text type = (_submodel_[1])::GetType(?);
    Case(type=="Linear", {
      @Noise noise = (_submodel_[1])::GetNoise(?);
      Case(noise::GetSubclass(?)=="ARIMA", {
        "VMatrix Eye("<<Get.Equation.Size(?)<<");"
      }, noise::GetRelativeCovariance.Type(?), {
        VMatrix cov = noise::GetRelativeCovariance_Active(?);
        //! No se tiene acceso a Get.Doc.Path(?) que es del objeto Primary
        //! as� que se utiliza la variable local "path.prefix" del m�todo
        //! que llama a �ste. V�ase BysMcmc/bsr/_import.tol
        Real Ois.Store([[cov]], path.prefix<<"Cov_"<<Get.Name(?)<<".oza"); 
        "VMatrix { Include("<<Qt("./Cov_"<<Get.Name(?)<<".oza")<<")[1] };"
      }, True, {
        "VMatrix Eye("<<Get.Equation.Size(?)<<");"
      })
    }, type<:[["Probit", "Logit"]], {
      "VMatrix Eye("<<Get.Equation.Size(?)<<");"
    }) 
  };

  ////////////////////////////////////////////////////////////////////////////
  Set Get.Params(Real void) { _UseCache(_Get.Params) };
  ////////////////////////////////////////////////////////////////////////////
  Set _Get.Params(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Todos los t�rminos explicativos poseen al menos un par�metro lineal
    Set expTerms0 = MMS.SelectActive((_submodel_[1])::GetExpTerms(?));
    // [AdditiveApproximation] Se filtran los t�rminos aditivos seg�n 
    // si ha de usar la aproximaci�n de los t�rminos aditivos o no.
    Set expTerms = If(AdditiveApproximation, expTerms0, {
      Select(expTerms0, Real (@ExpTerm expTerm) { 
        Not(expTerm::IsAdditive(?)) 
      })
    });
    Set effectives = ObtainEffectiveMObjects_Unique(SetConcat(
      EvalSet(expTerms, Set (@ExpTerm expTerm) {
      expTerm::GetParametersLinear(?)
    })));
    EvalSet(effectives, Set (@MObjectPrior effective) {
      MMS.Get.Bsr.Param.Info(effective)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  // Retorna el n�mero de par�metros lineales del segmento
  Real Get.Param.Size(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Card(Get.Params(?))
  }; 

  ////////////////////////////////////////////////////////////////////////////
  // Retorna la informaci�n para el iP-�simo par�metro
  Set Get.Param(Real iP)
  ////////////////////////////////////////////////////////////////////////////
  {
    Get.Params(?)[iP]
  };

  ////////////////////////////////////////////////////////////////////////////
  Set _Get.Missings.Y(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @MVariable output = (_submodel_[1])::GetOutput(?);
    //WriteLn("*** Se construyen los omitidos del output. ***");
    Real (_submodel_[1])::BuildOutputParametersMissing(?);
    Set parametersMissing = output::GetParametersMissing(?);
    Set interval = Get.IntervalInfo(?);
    Select(EvalSet(parametersMissing, Set (@ParameterLinear pMissing) {
      MMS.Get.Bsr.Missing.Info(pMissing, interval, [[
        Real owner.type = 0; //Output
        Text owner.name = output::GetIdentifier(?);
        Real owner.index = 1
      ]], 0)
    }), Card)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set _Get.Missings.X(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set expTerms0 = MMS.SelectActive((_submodel_[1])::GetExpTerms(?));
    // [AdditiveApproximation] Se filtran los t�rminos aditivos seg�n 
    // si ha de usar la aproximaci�n de los t�rminos aditivos o no.
    Set expTerms = If(AdditiveApproximation, expTerms0, {
      Select(expTerms0, Real (@ExpTerm expTerm) {
        Not(expTerm::IsAdditive(?)) 
      })
    });
    // Hay tantos inputs por t�rmino como par�metros lineales
    // linearIndex nos sirve para llevar la cuenta
    Real linearIndex = 0;
    SetConcat(EvalSet(expTerms, Set (@ExpTerm expTerm) {
      // Todos los t�rminos explicativos poseen al menos un input
      // y por tanto pueden tener par�metros de omitido
      // Sin embargo no se tratar�n los omitidos en t�rminos no-lineales
      // ya que deber�an entrar como par�metros en el filtro no lineal.
      Set parametersLinear = expTerm::GetParametersLinear(?);
      If(Not(expTerm::GetSubclass(?) <: [[ "Linear", "Omega" ]]), {
        Real linearIndex := linearIndex + Card(parametersLinear);
        //! Se admite que no hay omitidos en los inputs no lineales
        Copy(Empty)
      }, {
        // Se construyen y solicitan los omitidos del input
        @MVariable input = expTerm::GetInput(?);
        Real If(input::GetGrammar(?)=="Serie", {
          Set degrees = EvalSet(expTerm::GetParametersLinear(?), 
            Real (@ParameterLinear parameter) { parameter::GetDegree(?) });
          Real min = SetMin(degrees);
          Real max = SetMax(degrees);
          Set interval = Get.IntervalInfo(?);
          Date first = Succ(interval[1], interval[3], -max);
          Date last = Succ(interval[2], interval[3], -min);
          input::BuildParametersMissing(first, last)
        }, {
          Set interval = Get.IntervalInfo(?);
          input::BuildParametersMissing_Domain(interval[1], interval[2], 
            interval[3])
        });
        Set parametersMissing = input::GetParametersMissing(?);
        // Se indican los omitidos para el input de cada par�metro lineal.
        // En BSR cada par�metro lineal tiene su input y por ello debemos
        // indicar el omitido para cada uno de ellos.
        SetConcat(EvalSet(parametersLinear, Set (@ParameterLinear pLinear) {
          Real linearIndex := linearIndex + 1;
          Real delay = pLinear::GetDegree(?);
          Select(EvalSet(parametersMissing, Set (@ParameterLinear pMissing) {
            MMS.Get.Bsr.Missing.Info(pMissing, Get.IntervalInfo(?), [[
              Real owner.type = 1; //Input
              Text owner.name = input::GetIdentifier(?);
              Real owner.index = Copy(linearIndex)
            ]], delay)
          }), Card)
        }))
      })
    }))
  };

  ////////////////////////////////////////////////////////////////////////////
  Set Get.Missings(Real void) { _UseCache(_Get.Missings) };
  ////////////////////////////////////////////////////////////////////////////
  Set _Get.Missings(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _Get.Missings.Y(?) << _Get.Missings.X(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Retorna el n�mero de par�metros asociados a los omitidos del segmento
  Real Get.Missing.Size(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Card(Get.Missings(?))    
  };

  ////////////////////////////////////////////////////////////////////////////
  // Retorna la informaci�n para el iP-�simo omitido
  Set Get.Missing(Real iP)
  ////////////////////////////////////////////////////////////////////////////
  {  
    Get.Missings(?)[iP]
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix Get.Matrix.Y(Real void) { _UseCache(_Get.Matrix.Y) };
  ////////////////////////////////////////////////////////////////////////////
  Matrix _Get.Matrix.Y(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Anything data = (_submodel_[1])::GetData(?);
    Matrix MMS.Get.Data.Matrix(data, GetFirst(?), GetLast(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix Get.Matrix.X(Real void) { _UseCache(_Get.Matrix.X) };
  ////////////////////////////////////////////////////////////////////////////
  Matrix _Get.Matrix.X(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Se tiene en cuenta la naturaleza de la BoxCox. (V�ase #973)
    // Se obtiene la primera derivada de la transformaci�n sobre las 
    // observaciones para usar en la aproximaci�n de los t�rminos aditivos.
    @MVariable output = (_submodel_[1])::GetOutput(?);
    @Anything obs = @Anything(output::GetVariable(?)::GetData(?));
    @Anything bcD1_obs =  If((_submodel_[1])::GetSubclass(?)=="D", {
      @Anything($obs^0)
    }, {
      @Transformation trn = (_submodel_[1])::GetTransformation(?);
      @Anything(trn::DirectD1($obs))
    });
    Set expTerms0 = MMS.SelectActive((_submodel_[1])::GetExpTerms(?));
    // [AdditiveApproximation] Se filtran los t�rminos aditivos seg�n 
    // si ha de usar la aproximaci�n de los t�rminos aditivos o no.
    Set expTerms = If(AdditiveApproximation, expTerms0, {
      Select(expTerms0, Real (@ExpTerm expTerm) {
        Not(expTerm::IsAdditive(?)) 
      })
    });
    // Se devuelve un input para cada par�metro lineal
    //! En el caso de los t�rminos no lineales habr�a que devolver
    //! un input representando el valor inicial correspondiente del filtro
    Set parameterData = SetConcat(EvalSet(expTerms, Set (@ExpTerm expTerm) {
      // Se obtiene el valor inicial del input �nico o el filtro:
      Anything data = expTerm::GetInitialFilter(?);
      Case(Grammar(data)=="Serie", {
        // Se admite que s�lo las series tienen m�s de un par�metro lineal
        Set pars = expTerm::GetParametersLinear(?);
        Set EvalSet(pars, Serie (@ParameterLinear parameter) {
          Real degree = parameter::GetDegree(?);
          Polyn pol = If(degree>=0, B**degree, F**Abs(degree));
          If(expTerm::IsAdditive(?), (pol:data) * $bcD1_obs, pol:data)
        })
      }, Grammar(data)=="Matrix", {
       [[ If(expTerm::IsAdditive(?), data $* $bcD1_obs, data) ]]
      })
    }));
    Matrix Group("ConcatColumns", EvalSet(parameterData, 
      Matrix (Anything data) {
      MMS.Get.Data.Matrix(data, GetFirst(?), GetLast(?))
    }), Matrix Rand(Get.Equation.Size(?), 0, ?, ?))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  // Retorna el n�mero de ecuaciones del segmento
  Real Get.Equation.Size(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Rows(Get.Matrix.Y(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  // [primary.import] 
  // Matriz del output
  VMatrix Get.OutputVMatrix(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Mat2VMat(Get.Matrix.Y(?))
  };

  ////////////////////////////////////////////////////////////////////////////
  // [primary.import] 
  // Matriz del input
  VMatrix Get.InputVMatrix(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Mat2VMat(Get.Matrix.X(?))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  NameBlock Get.Constraints.Handler(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { BysMcmc::Bsr::Import::Unconstrained(0) }; 
 
  ////////////////////////////////////////////////////////////////////////////
  // [primary.import] 
  Set Get.NonLinearFilters(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // -----------------------------------------------------------------------
    // Filtro para modelo Probit.
    Text submodelType = (_submodel_[1])::GetType(?);
    Set probitFilters = If(submodelType=="Probit", {
      Text grammar = (_submodel_[1])::GetGrammar(?);
      Set [[ BysMcmc::Bsr::Gibbs::ProbitFilter(
        Real 1,
        Text Get.Name(?), 
        Anything Get.Matrix.Y(?),
        Date If(grammar=="Matrix", UnknownDate, GetFirst(?)),
        Date If(grammar=="Matrix", UnknownDate, GetLast(?))
      ) ]]
    }, Copy(Empty));
    // -----------------------------------------------------------------------
    // Fitro de los t�rminos explicativos no lineales
    Set expTerms = MMS.SelectActive((_submodel_[1])::GetExpTerms(?));
    // -----------------------------------------------------------------------
    // Fitro de los t�rminos explicativos de tipo Ratio
    Set expTermsRatio = Select(expTerms, Real (@ExpTerm expTerm) {
      expTerm::GetSubclass(?)=="Ratio"
    });
    Set ratioFilters = EvalSet(expTermsRatio, 
      NameBlock (@ExpTermRatio term) {
      Serie data = CalInd(W, Get.IntervalInfo(?)[3]) 
        << term::GetInput(?)::GetData(100);
      Real maxDegree = SetMax(term::GetParametersLinear.Degree(?));
      NameBlock deltaF = BysMcmc::Bsr::Gibbs::DeltaTransferWithFixedInitValues(
        Text segmentName = (_submodel_[1])::GetIdentifier(?)<<"::Noise",
        // Nombre del t�rmino explicativo
        Text inputName = term::GetName(?), 
        // Nombre de los par�metros lineales
        Set linBlkNames = EvalSet(term::GetParametersLinear(?), 
          Text (@ParameterLinear p) { p::GetIdentifier(?) }
        ); 
        Polyn omega = Numerator(term::GetInitialRatio(?)), 
        Polyn delta = Denominator(term::GetInitialRatio(?)),
        Matrix x0 = MMS.Get.Data.Matrix(data, 
          Date Succ(GetFirst(?), Get.IntervalInfo(?)[3] ,-maxDegree), 
          Date GetLast(?)),
        Matrix x = MMS.Get.Data.Matrix(data, GetFirst(?), GetLast(?)),
        False // No se usa ganancia unitaria
      );
      deltaF
    });
    // -----------------------------------------------------------------------
    // Fitro de los t�rminos explicativos de tipo NonLinear
    Set expTermsNonLinear = Select(expTerms, Real (@ExpTerm expTerm) {
      expTerm::GetSubclass(?)=="NonLinear"
    });
    Set nonLinearFilters = EvalSet(expTermsNonLinear, 
      NameBlock (@ExpTermNonLinear term) {
      @ExpTermNonLinear.BSRFilter::Default(term, Get.Name(?)<<"::Noise", 
        Get.IntervalInfo(?))
    });
    // -----------------------------------------------------------------------
    // Fitro de los t�rminos explicativos aditivos puros
    // [AdditiveApproximation] Se filtran los t�rminos aditivos seg�n 
    // si ha de usar la aproximaci�n de los t�rminos aditivos o no.
    Set expTermsAdditive = If(AdditiveApproximation, Copy(Empty), {
      Select(MMS.SelectActive((_submodel_[1])::GetExpTerms(?)), 
        Real (@ExpTerm expTerm) { expTerm::IsAdditive(?)})
    });
    //Filtro no lineal de tipo efecto aditivo con output en logaritmos
    Set additiveFilter = If(Card(expTermsAdditive), {
      @ExpTerms.Additive.BSRFilter addFilter = 
        @ExpTerms.Additive.BSRFilter::Default(_submodel_[1], expTermsAdditive, 
          Get.Name(?)<<"::Noise", Get.IntervalInfo(?));
      Real addFilter::initialize(?);
      [[ addFilter ]]
    }, Copy(Empty));
    // -----------------------------------------------------------------------
    probitFilters << ratioFilters << nonLinearFilters << additiveFilter
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @Submodel.BSRImport Default(@Submodel submodel)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text submodelType = submodel::GetType(?);
    If(submodelType<:[["Linear", "Probit"]], {
      @Submodel.BSRImport submodelImport = [[
        Set _submodel_ = [[submodel]]
      ]]
    },  {
      Real MMS.Error("El submodelo '"<<submodel::GetName(?)
        <<"' es de tipo '"<<submodelType
        <<"' y no puede estimarse con la estrategia BSR.",
        "@Submodel.BSRImport::Default");
      If(False, ?)
    })
  }
};
//////////////////////////////////////////////////////////////////////////////
