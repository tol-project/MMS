
//////////////////////////////////////////////////////////////////////////////
Class @StrategyMultiNLO : @Strategy
//////////////////////////////////////////////////////////////////////////////
{
  Static Text _.subclass = "MultiNLO";
  //H: Set _parent_;   // @Estimation
  //H: Set _.settings; // @Settings

  Set _.substrategies; // No persistente

  //H: Set _.results; // No persistente
  //H: Set _.reports; // No persistente

  ////////////////////////////////////////////////////////////////////////////
  Set GetConditioning(Real void)
  //! Para su acceso por parte de las substrategias -> subcondicionamientos
  ////////////////////////////////////////////////////////////////////////////
  { (_parent_[1])::GetConditioning_Joint(?) };

  ////////////////////////////////////////////////////////////////////////////
  // M�todos de estrategia m�ltiple
  //  * Real Prepare
  //  + Set GetSubstrategies
  //  * Real Execute
  //  * @ResultsAdapter GetResults
  //  * Set GetReports
  //  * Real Clear(Substrategies)

  ////////////////////////////////////////////////////////////////////////////
  Real Prepare(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real build = If(Card(_.substrategies), 0, _BuildSubstrategies(?));
    If(build<0, build, {
      Real Reporter::Initialize(?);
      Real actions = SetSum0(EvalSet(_.substrategies, 
        Real (@Substrategy substrategy) {
        substrategy::Prepare(?)
      }));
      Real Reporter::Finalize(?);
      Real If(actions>0, {
        Set RemoveByName(_.reports, "Preparation.MNodeDefs");
        Reporter::AppendTo(_.reports, "Preparation.MNodeDefs")
      });
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetSubstrategies(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Copy(_.substrategies)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Execute(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _ClearExecution(?);
    Real Prepare(?);
    Set substrategies = GetSubstrategies(?);
    // Condicionamiento
    Set conditioning = (_parent_[1])::GetConditioning(?);
    Real If(Card(conditioning), {
      Real MMS.Warning("La estrategia MultiNLO a�n no admite el 
        condicionamiento de los par�metros.", "@StrategyMultiNLO::Execute")
    });
    // [Execution & Results]
    Real Reporter::Initialize(?);
    Real (_.settings[1])::ApplyGlobalSettings(?);
    //------------------------------------------------------------------------
    // Estimaci�n de los submodelos
    Set results = EvalSet(substrategies, 
      @SubresultsAdapter (@Substrategy substrategy) {
      Real substrategy::Execute(?);
      substrategy::GetSubresults(?)
    });
    @ResultsAdapter resultsA = @ResultsAdapterMultiNLO::Default(results);
    //------------------------------------------------------------------------
    // Valoraci�n de las jerarqu�as
    Real {
      @Model model = (_parent_[1])::GetModel(?);
      If(Card(model::GetHierarchies(?)), {
        Real MMS.Warning("La estrategia MultiNLO a�n no hace ninguna 
          valoraci�n de las jerarqu�as.", "@StrategyMultiNLO::Execute")
      })
    };
    //------------------------------------------------------------------------
    Real (_.settings[1])::RestoreGlobalSettings(?);
    Set _.results := [[ resultsA ]];
    Real Reporter::Finalize(?);
    Real Reporter::AppendTo(_.reports, "Execution");
    If(Reporter::HasErrors(?), -1, 1)
  };

  //H: @ResultsAdapter GetResults(Real void);
  //H: Set GetReports(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Real _BuildSubstrategies(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Reporter::Initialize(?);
    @Model model = (_parent_[1])::GetModel(?);
    Set substrategies.S = EvalSet(MMS.SelectActive(model::GetSubmodels(?)),
      Anything (@Submodel submodel) {
      Text grammar = submodel::GetGrammar(?);
      // grammar: Serie, Matrix 
      Text type = submodel::GetType(?);
      // type: Linear, Logit, Probit
      Case(grammar=="Matrix" & type<:[["Logit", "Probit", "OrderedProbit"]], {
         @SubstrategyQRM::Default(submodel, _this) //Logit/Probit
      }, True, {
        Real MMS.Error("No hay una subestrategia definida para:\n"
          <<"  datos de tipo "<<grammar<<"\n"
          <<"  una funci�n de enlace de tipo "<<type<<".", 
          "@StrategyMultiNLO::_BuildSubstrategies");
        If(False, ?)
      })
    });
    Set _.substrategies := substrategies.S;
    Real Reporter::Finalize(?);
    Real Reporter::AppendTo(_.reports, "Preparation.Substrategies");
    If(Reporter::HasErrors(?), -1, 1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _ClearExecution(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set Remove(_.reports, FindIndexByName(_.reports, "ExecutionAndResults"));
    Set _.results := Copy(Empty);
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set EvalSet(_.substrategies, Real (@Substrategy substrategy) {
      substrategy::Clear(?)
    });
    Set _.substrategies := Copy(Empty);
    Set _.reports := Copy(Empty);
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetNativeSpecification(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Prepare(?);
    Set substrategies = GetSubstrategies(?);
    Set NativeSpecification = EvalSet(substrategies, 
      Anything (@Substrategy substrategy) {
      substrategy::GetNativeSpecification(?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real SaveIsolationFile(Text filename)
  ////////////////////////////////////////////////////////////////////////////
  {
    Ois.Store(GetNativeSpecification(?), filename)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @StrategyMultiNLO New(NameBlock args, @Estimation parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @StrategyMultiNLO strategyMultiNLO = [[
      Set _parent_ = [[ parent ]];
      Set _.settings = [[ getOptArg(args, "_.settings", 
        { @SettingsMultiNLO settings }) ]];
      Set _.substrategies = Copy(Empty);
      Set _.results = Copy(Empty);
      Set _.reports = Copy(Empty)
    ]];
    strategyMultiNLO
  }
};
//////////////////////////////////////////////////////////////////////////////
