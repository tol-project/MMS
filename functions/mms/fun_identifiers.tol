
//////////////////////////////////////////////////////////////////////////////
// Funciones para tratar la creaci�n y la gesti�n de nombres �nicos o 
// identificadores (Identifier)

Text MMS.IdentifierToken = "__";

//////////////////////////////////////////////////////////////////////////////
Set MMS.IdentifierToNames(Text identifier)
//////////////////////////////////////////////////////////////////////////////
{
  Tokenizer(Replace(identifier, MMS.IdentifierToken, "�"), "�")
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.NamesToIdentifier(Set names)
//////////////////////////////////////////////////////////////////////////////
{
  TextJoinWith(Select(names,TextLength), MMS.IdentifierToken)
};

//////////////////////////////////////////////////////////////////////////////
// Funciones para la obtenci�n de objetos en un conjunto
// * Real MMS.FindIndexByInfo(Set set, Anything info)
// * Anything MMS.ObtainByInfo(Set set, Anything info)
// Anything info:
//  + Real: index
//  + Text: identifier
//  + Set: names
//  + NameBlock: -> GetIdentifier(?)

//////////////////////////////////////////////////////////////////////////////
Real MMS.FindIndexByInfo(Set set, Anything info)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(info);
  Real Case(grammar=="Real", {
    If(1<=info & info<=Card(set) & IsInteger(info), info, 0)
  }, grammar=="Text", {
    FindIndexByName(set, info)
  }, grammar=="Set", {
    FindIndexByName(set, MMS.NamesToIdentifier(info))
  }, grammar=="NameBlock", {
    If(ObjectExist("Code", "info::GetIdentifier(?)"), {
      FindIndexByName(set, info::GetIdentifier(?))
    }, {
      Text class = ClassOf(info);
      Real MMS.Warning("Argumento info de tipo NameBlock "
        <<If(TextLength(class), "("<<class<<") ", "")<<"no v�lido.", 
        "MMS.FindIndexByInfo");
    0})
  }, True, {
    Real MMS.Warning("Argumento info de tipo "<<Grammar(info)<<" no v�lido.", 
      "MMS.FindIndexByInfo");
  0})
};

//////////////////////////////////////////////////////////////////////////////
Anything MMS.ObtainByInfo(Set set, Anything info)
//////////////////////////////////////////////////////////////////////////////
{
  Real index = MMS.FindIndexByInfo(set, info);
  If(index, set[index])
};

//////////////////////////////////////////////////////////////////////////////
Real MMS.FindIndexByInfo.Advanced(Set set, Anything info)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(info);
  Real Case(grammar=="Real", {
    If(1<=info & info<=Card(set) & IsInteger(info), info, 0)
  }, grammar=="Set", {
    Set sel = Select(set, Real (Anything element) {
      Set elemInfo = MMS.IdentifierToNames(Name(element));
      If(Card(elemInfo)<Card(info), 0, {
        BinGroup("&", For(1, Card(info), Real (Real i) {
          elemInfo[i]==info[i]  
        }))
      })
    });
    Case(Card(sel)==1, {
      FindIndexByName(set, Name(sel[1]))
    }, Card(sel)>1, {
      Set sel2 = Sort(sel, Real(Anything element1, Anything element2) {
        Compare(Name(element2), Name(element1)) 
      });
      Real MMS.Warning("Existe m�s de un objeto que corresponde al "
        <<"criterio seleccionado.\n  Se escogi�: "
        <<Name(sel2[1]), "MMS.FindIndexByInfo.Advanced");
      FindIndexByName(set, Name(sel2[1]))
    }, True, 0)
  }, grammar=="Text", {
    MMS.FindIndexByInfo.Advanced(set, MMS.IdentifierToNames(info))
  }, grammar=="NameBlock", {
    If(ObjectExist("Code", "info::GetIdentifier(?)"), {
      MMS.FindIndexByInfo.Advanced(set, 
        MMS.IdentifierToNames(info::GetIdentifier(?)))
    }, {
      Text class = ClassOf(info);
      Real MMS.Warning("Argumento info de tipo NameBlock "
        <<If(TextLength(class), "("<<class<<") ", "")<<"no v�lido.", 
        "MMS.FindIndexByInfo.Advanced");
    0})
  }, True, {
    Real MMS.Warning("Argumento info de tipo "<<Grammar(info)<<" no v�lido.", 
      "MMS.FindIndexByInfo.Advanced");
  0})
};

//////////////////////////////////////////////////////////////////////////////
Anything MMS.ObtainByInfo.Advanced(Set set, Anything info)
//////////////////////////////////////////////////////////////////////////////
{
  Real index = MMS.FindIndexByInfo.Advanced(set, info);
  If(index, set[index])
};

//////////////////////////////////////////////////////////////////////////////
// Funciones para acortar los identificadores

//////////////////////////////////////////////////////////////////////////////
Text MMS.ShortenIdentifier(Text identifier, Real maxLength)
//////////////////////////////////////////////////////////////////////////////
{
  If(TextLength(identifier) > maxLength, {
    Set names = MMS.IdentifierToNames(identifier);
    If(Card(names)==1, {
      TextSub(identifier, 1, maxLength-2)<<"~~"
    }, {
      // Se considera la terminaci�n del identificador
      Real maxLastLength = (maxLength-4)/2;
      Text lastName = names[Card(names)];
      If(TextLength(lastName) <= maxLastLength, {
        // Si la terminaci�n no excede "maxLastLength" se mantiene completa
        TextSub(identifier, 1, maxLength-4-TextLength(lastName))
          <<"~~__"<<lastName
      }, {
        // Si la terminaci�n excede "maxLastLength" se recorta
        TextSub(identifier, 1, maxLength-maxLastLength-1)<<"~~"
          <<TextSub(lastName, 1-maxLastLength, -1)
      })
    })
  }, identifier)
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.ShortenIdentifier_Index(Text identifier, Real maxLength, Real index)
//////////////////////////////////////////////////////////////////////////////
{
  If(TextLength(identifier) > maxLength, {
    Text txtIndex = "~"<<index;
    TextSub(identifier, 1, maxLength-TextLength(txtIndex))
     <<txtIndex
  }, identifier)
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.ObtainMainIdentifier(NameBlock instance)
//////////////////////////////////////////////////////////////////////////////
{
  Case(ObjectExist("Code", "instance::GetMainIdentifier"), {
    instance::GetMainIdentifier(?)
  }, ObjectExist("Code", "instance::GetParent"), {
    MMS.ObtainMainIdentifier(instance::GetParent(?))
  }, True, "")
};

//////////////////////////////////////////////////////////////////////////////
Set MMS.ObtainMainIdentifiers(Set instances)
//////////////////////////////////////////////////////////////////////////////
{ Unique(EvalSet(instances, MMS.ObtainMainIdentifier)) };

//////////////////////////////////////////////////////////////////////////////
