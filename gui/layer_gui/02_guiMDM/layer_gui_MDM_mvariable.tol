//////////////////////////////////////////////////////////////////////////////
// FILE:    layer_gui_MDM_mvariable.tol
// PURPOSE: Definition of the communication layer between MMS and the GUI for
//          MDM (model variables)
//////////////////////////////////////////////////////////////////////////////

/*
  # Model variable data to be edited
   # (Class @MVariable)
    #                                  - (Set _parent_ // @Model)
    # mvariable_info(name)             - Model variable name (Text _.name)
    # mvariable_info(grammar)          - Grammar (Serie, Matrix, Real)
    # mvariable_info(g_active)         - Global active (IsActive method)
    #                                  - (Set _.variable_ // @Variable)
     # (Class @Variable)
      # mvariable_info(var)            - Variable identifier
    #                                  - (Set _.transformation // @Transformation)
     # (Class @Transformation)
      # mvariable_info(transf)         - Transformation info (It�s a named list)
	#                                  - (Set _.baseParameterMissing)
    #                                  - (Set _.parametersMissing)
*/

////////////////////////////////////////////////////////////////////////////
Set GetInfoMVariable(@MVariable mvar)
////////////////////////////////////////////////////////////////////////////
{
  Text name = mvar::GetName(?);
  Real g_active = mvar::IsActive(?);
  Text abs_id = mvar::GetAbsoluteIdentifier(?);

  Text grammar = mvar::GetGrammar(?);

  @Variable variable = mvar::GetVariable(?);
  Text var = variable::GetIdentifier(?);

  Set transf = If(mvar::HasTransformation(?), {
    @Transformation transformation = mvar::GetTransformation(?);
    Set GetInfoTransformation(transformation)
  }, {
    Set Empty
  });
  
  Set SetOfAnything(name, g_active, var, grammar, transf, abs_id)
};
   
////////////////////////////////////////////////////////////////////////////
Set GetMVariable(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  
  @MVariable mvar = mod::GetMVariable(ident);
  Set GetInfoMVariable(mvar)
};

////////////////////////////////////////////////////////////////////////////
Set GetMVariablesList(Set container, Text details)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);

  Set setInf = EvalSet(mod::GetMVariables(?), Set (@MVariable mvar) {
    Text ident = mvar::GetIdentifier(?);
    Text grammar = mvar::GetGrammar(?);
    Text abs_id = mvar::GetAbsoluteIdentifier(?);
	
    Set If(details == "no", {
      Set SetOfAnything(ident, grammar, abs_id)
	}, {
      Text name = mvar::GetName(?);
	  
      @Variable variable = mvar::GetVariable(?);
      Text var = variable::GetIdentifier(?);
	  
      Text transf = If(mvar::HasTransformation(?), {
        @Transformation transformation = mvar::GetTransformation(?);
        Text transformation::GetName(?)
      }, {
        Text "Ninguna"
      });
	  
      Set SetOfAnything(ident, name, grammar, var, transf, abs_id)
	})
  })
};    

////////////////////////////////////////////////////////////////////////////
Text GetMVariableHelp(Real void)
////////////////////////////////////////////////////////////////////////////
{
  Text help = If(ObjectExist("Text", "MMS::@MVariable::_.autodoc.help"),
    MMS::@MVariable::_.autodoc.help, ""
  )
};

////////////////////////////////////////////////////////////////////////////
Set GetMVariableParameters(Set container)
////////////////////////////////////////////////////////////////////////////
{  
  Text mvariable = container[Card(container)];
  Set model_container = container - [[mvariable]];
  @Model mod = FindModel(model_container);
  @MVariable mvar = mod::GetMVariable(mvariable);

  Set parameters = mvar::GetParametersMissing(?)
};

////////////////////////////////////////////////////////////////////////////
Real CreateMVariableTest(Set info, Set transf, Set container)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CreateMVariableTest");
  WriteLn("name:"+info["name"]);
  WriteLn("var:"+info["var"]);
  Real ShowTransformationTest(transf)
};

////////////////////////////////////////////////////////////////////////////
Real CreateMVariable(Set info, Set transf, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  
  NameBlock args = [[
    Text _.name = info["name"];
    Text _.variableIdentifier = info["var"]
  ]];

  Real If(transf["family"]!="None", {
    @Transformation _.transformation = CreateTransformation(transf);
    NameBlock args := SetToNameBlock(
      NameBlockToSet(args) << [[_.transformation]]
    );
    Real 1
  });

  @MVariable mvariable = mod::CreateMVariable(args);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditMVariableTest(Set info, Set transf, Set container)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("EditMVariableTest");
  WriteLn("name:"+info["name"]);
  WriteLn("var:"+info["var"]);
  Real ShowTransformationTest(transf)
};

////////////////////////////////////////////////////////////////////////////
Real EditMVariable(Set info, Set transf, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  
  @MVariable mvar = mod::GetMVariable(info["ident"]);
  
  Real mvar::SetName(info["name"]);

  Real If(transf["family"]!="None", {
    @Transformation _.transformation = CreateTransformation(transf);
    Real mvar::SetTransformation(_.transformation)
  }, {
    Real mvar::RemoveTransformation(?)
  })
};

////////////////////////////////////////////////////////////////////////////
Text GetMVariableGrammar(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);

  @MVariable mvar = mod::GetMVariable(ident);
  Text grammar = mvar::GetGrammar(?)
};

////////////////////////////////////////////////////////////////////////////
Real PasteMVariables(Text model)
////////////////////////////////////////////////////////////////////////////
{
  //! Usa el nuevo portapeles MMS::Clipboard
  //! Deber�a implementarse como un m�todo de instancia
  //! Quiz� deber�a comprobarse que las variables "seleccionadas" pertenecen
  //! al modelo para evitar errores no controlados
  @Model mod = FindModel(model);
  Set objects = Clipboard::GetObjects(?);
  Set EvalSet(objects, Real (MMS::@Variable var) {

    NameBlock args = [[
      Text _.name = var::GetIdentifier(?);
      Text _.variableIdentifier = var::GetIdentifier(?)
    ]];
	
    @MVariable mvariable = mod::CreateMVariable(args);
	Real 1
  });
  Real 1
};
