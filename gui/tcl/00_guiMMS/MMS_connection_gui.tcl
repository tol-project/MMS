#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_connection_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Connections
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MMSConnectionsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateConnectionsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateConnectionsListDetails"
  set l_details_frame [frame $f.details_Connections]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Connection" \
    -swlist "::MMSConnectionsGui::bmmsconnlist" \
	-fshowitem "::MMSConnectionsGui::_ShowConnectionDetails" \
	-fshowlist "::MMSConnectionsGui::_ShowConnectionsListDetails" \
	-cmenu "yes" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowConnectionsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateConnectionsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details Init
  $l_details_frame.details FillList

  return $l_details_frame
}
  
#/////////////////////////////////////////////////////////////////////////////
proc ShowConnectionsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  _ShowConnectionHelp  

  _ShowConnectionsListDetails "MMS"
}
  
#/////////////////////////////////////////////////////////////////////////////
proc CMenuConnections {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Connection"] \
    -command "::MMSConnectionsGui::NewConnection" \
	-state $_state
}
  
#/////////////////////////////////////////////////////////////////////////////
proc NewConnection {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowConnectionHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMMSConnectionsGui::GetConnectionHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateConnectionDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateConnectionDetails"
  set details_frame [frame $f.details_Connection]
  set view_frame [frame $f.view_Connection]

  set _details [bmmsconnection $details_frame.details]
  set _view [bmmsconnection $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowConnectionDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateConnectionDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowConnectionDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container "MMS"
  set ident [$tree item text $id first]

  _ShowConnectionDetails $container $ident "View" "tree"
}
  

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsconnlist {
# PURPOSE : Defines the snit widget used to
#           list the Connections of MMS
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container to wich the Connections belongs
  option -container \
    -default "MMS" 

  option -details \
    -default "no" -configuremethod "_conf-details"  

  # Defines if list only available Connections
  option -available \
    -default "no" 

  variable mms_connlist
    # mms_connlist(list)  - Connections list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-available { _ available } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-available) $available 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Connections of MMS
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Connections List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]

	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Type"]] \
        [list text -label [mc "Source"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    $tree configure -contextmenu [$self CreateCMenu]

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    focus $tree
    
    set mms_connlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
	if {$options(-available) eq "yes"} {
      set mms_connlist(list) [LayerMMSConnectionsGui::GetConnections_AvailableList]
	} else {
      set mms_connlist(list) [LayerMMSConnectionsGui::GetConnectionsList \
	    $options(-details)]
	}
    #puts "FillList mms_connlist(list)=$mms_connlist(list)"

    $tree item delete all

    foreach {{} it} $mms_connlist(list) {
      array set vinfo $it
      #puts "FillList vinfo=[array get vinfo]"

	  if {[LayerMMSConnectionsGui::IsConnectionAvailable $vinfo(ident)]} {
        set icon [ Bitmap::get connectno16 ]
	  } else {
        set icon [ Bitmap::get connectyes16 ]
	  }
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(type)] \
	      [list $vinfo(sour)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }

      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }
 
  #/////////////////////////////////////////////////////////////////////////////
  method CreateCMenu { } {
  #/////////////////////////////////////////////////////////////////////////////

    menu $tree.cmenu -tearoff 0 \
      -postcommand [mymethod PostCMenu $tree.cmenu]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method PostCMenu {cmenu} {
  #/////////////////////////////////////////////////////////////////////////////

    $cmenu delete 0 end
    $cmenu add command -label [mc "Delete"] \
	  -command "[mymethod Delete]"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Delete { } {
  #/////////////////////////////////////////////////////////////////////////////
  
    set answer [tk_messageBox \
      -message [mc "Do you want to delete the selected objects?"] \
	  -type yesno -icon question]
    if {$answer eq yes} {
      set selected [$tree selection get]
      foreach {item} $selected {
        set name [$tree item text $item 1]
        LayerMMSConnectionsGui::DeleteConnection $name
      }
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
             [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsconnection {
# PURPOSE : Defines the snit widget used to
#           create new Connections or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # The container to wich the Connection belongs
  option -container \
    -default "MMS"
	
  # Identifier of the Connection to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Connections List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  

  variable label_state

  variable widgets
    
  variable connection_info
  # Data of the Connection to be edited
   # connection_info(name)        - Connection Name (Text)
   # connection_info(desc)        - Description (Text)
   # connection_info(type)        - Type (Text) (folder, mysql) 
   # connection_info(sour)        - Source (Text) 
    #                             - type == "folder"
     # connection_info(path)      - Path (Text) 
    #                             - type == "mysql"
     # connection_info(odbc)      - Odbc (Text) 
     # connection_info(sque)      - Schema (Text) 
     # connection_info(user)      - User (Text) 
     # connection_info(pass)      - Password (Text) 

  component dlg

  delegate method * to hull
  delegate option * to hull

  typevariable ListTypes
  # Types of Connections (folder, mysql)

  typeconstructor {

	set ListTypes {"folder" "mysql"}
  }
	
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item 
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Connection
  #///////////////////////////////////////////////////////////////////////////
    
    set f $dlg
    
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Connection"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Connection"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Connection"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Type"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar connection_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    entry $fe.eDesc -textvariable [myvar connection_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc
    
    ComboBox $fe.cbType -values $ListTypes \
      -textvariable [myvar connection_info(type)] -editable false \
      -width 15 -state disabled \
	  -modifycmd [string map [list %W $fe.cbType %M [mymethod UpdateTypeFields]] {
        set idx [%W getvalue]
        if {$idx != -1} {   
		  %M
        }
      }]
    set widgets(type) $fe.cbType

    grid $fe.lName  -row 0 -column 0 -sticky e
    grid $fe.eName  -row 0 -column 1 -sticky w
    grid $fe.lDesc  -row 1 -column 0 -sticky e
    grid $fe.eDesc  -row 1 -column 1 -sticky w
    grid $fe.lType  -row 2 -column 0 -sticky e
    grid $fe.cbType -row 2 -column 1 -sticky w

    label $fe.lHSource -text [mc "Source"] -pady 5 -padx 5
    set fsource [labelframe $fe.fsource \
      -labelwidget $fe.lHSource -relief solid -bd 1]

    label $fsource.lPath -text "[mc "Path"]:" -pady 5 -padx 5
    set widgets(lpath) $fsource.lPath
    
    label $fsource.lOdbc -text "[mc "Odbc"]:" -pady 5 -padx 5
    label $fsource.lSque -text "[mc "Schema"]:" -pady 5 -padx 5
    label $fsource.lUser -text "[mc "User"]:" -pady 5 -padx 5
    label $fsource.lPass -text "[mc "Password"]:" -pady 5 -padx 5
    set widgets(lodbc) $fsource.lOdbc
    set widgets(lsque) $fsource.lSque
    set widgets(luser) $fsource.lUser
    set widgets(lpass) $fsource.lPass

    entry $fsource.ePath -textvariable [myvar connection_info(path)] \
      -width 60 -state readonly
    set widgets(path) $fsource.ePath
    
    entry $fsource.eOdbc -textvariable [myvar connection_info(odbc)] \
      -width 40 -state readonly
    set widgets(odbc) $fsource.eOdbc
    
    entry $fsource.eSque -textvariable [myvar connection_info(sque)] \
      -width 40 -state readonly
    set widgets(sque) $fsource.eSque
    
    entry $fsource.eUser -textvariable [myvar connection_info(user)] \
      -width 20 -state readonly
    set widgets(user) $fsource.eUser
    
    entry $fsource.ePass -textvariable [myvar connection_info(pass)] \
      -width 20 -state readonly
    set widgets(pass) $fsource.ePass

    grid $fsource.lOdbc  -row 0 -column 0 -sticky e
    grid $fsource.eOdbc  -row 0 -column 1 -sticky w
    grid $fsource.lSque  -row 1 -column 0 -sticky e
    grid $fsource.eSque  -row 1 -column 1 -sticky w
    grid $fsource.lUser  -row 2 -column 0 -sticky e
    grid $fsource.eUser  -row 2 -column 1 -sticky w
    grid $fsource.lPass  -row 3 -column 0 -sticky e
    grid $fsource.ePass  -row 3 -column 1 -sticky w
    foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
      grid remove $widgets($w)
    }
	
    grid $fsource.lPath -row 0 -column 0 -sticky e
    grid $fsource.ePath -row 0 -column 1 -sticky w -padx 5

    grid rowconfigure    $fsource 4 -weight 1
    grid columnconfigure $fsource 1 -weight 1

    grid $fsource -row 3 -column 0 -columnspan 2 -sticky news -pady 10 -padx 10

	grid rowconfigure    $fe 4 -weight 1
    grid columnconfigure $fe 2 -weight 1
	
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1

    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
	
    foreach {w} {name desc type \
	             path \
				 odbc sque user pass} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name desc type \
	             path \
				 odbc sque user pass
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateTypeFields {} {
  #/////////////////////////////////////////////////////////////////////////////

    switch -- $connection_info(type) {
      "folder" {
        foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
          grid remove $widgets($w)
        }
        foreach {w} {lpath path} {
          grid $widgets($w)
        }
      }
      "mysql" {
        foreach {w} {lpath path} {
          grid remove $widgets($w)
        }
        foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
          grid $widgets($w)
        }
      }
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set connection_info(name) ""
    set connection_info(desc) ""
	
    set connection_info(type) "folder"
	$self UpdateTypeFields

    set connection_info(path) ""

    set connection_info(odbc) ""
    set connection_info(sque) ""
    set connection_info(user) ""
    set connection_info(pass) ""
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

    array set connection_info [LayerMMSConnectionsGui::GetConnection $ident]
	
	$self UpdateTypeFields
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Connection"]

    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new copy edit} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name desc \
	             path \
				 odbc sque user pass} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {type} {
      $widgets($w) configure -state disabled
    }
    
	$self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Connection"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {desc type \
	             path \
				 odbc sque user pass} {
      $widgets($w) configure -state normal
    }

    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc type \
	             path \
				 odbc sque user pass} {
      $widgets($w) configure -state normal
    }

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Connection"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
    
    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Connection"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    if {$options(-state) eq "Edit"} {
      LayerMMSConnectionsGui::EditConnection connection_info

    } else {                                 ;# New, Copy
      LayerMMSConnectionsGui::CreateConnection connection_info
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }
	  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
