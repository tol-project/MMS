#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with MMS layer
#/////////////////////////////////////////////////////////////////////////////

package require Tk
#package require widget::panelframe
#package require BWidget 1.9
package require BWidget
package require wtree

namespace eval ::MMSGui {

#/////////////////////////////////////////////////////////////////////////////
proc ShowMMSContainer {} {
#/////////////////////////////////////////////////////////////////////////////
  variable _mms_
  
  if { [info exists _mms_(form)] && [winfo exists $_mms_(form)] } {
    if { [$_mms_(form) cget -state] eq "minimized" } {
      $_mms_(form) restore
    }
    $_mms_(form) raise
  } else {
    CreateFormMMSContainer
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateFormMMSContainer {} {
#/////////////////////////////////////////////////////////////////////////////
  variable _mms_
    
  set _mms_(form) [::project::CreateForm -title "[mc {MMS}]" \
    -iniconfig MMS_GUI \
    -width 800 -height 600]
  set f [$_mms_(form) getframe]

  ::MMSGui::CreateMMSTab $f
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateMMSTab {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable _tree
  variable _details

  # Create PanedWindow with 2 horizontal panes
  #set pwh [ PanedWindow $f.pwh -side left -pad 0 -weights available ]
  set pwh [ PanedWindow $f.pwh -side left -pad 0 ]
  grid $pwh -row 0 -column 0 -sticky snew
  grid columnconfigure $f 0 -weight 1
  grid rowconfigure $f 0 -weight 1

  set fh1 [ $pwh add -weight 10 ]
  set fh2 [ $pwh add -weight 2 ]

  # In first pane, add 2 vertical panes
  #set pwv1 [ PanedWindow $fh1.pwv1 -side top -pad 0 -weights available ]
  set pwv1 [ PanedWindow $fh1.pwv1 -side top -pad 0 ]
  grid $pwv1 -row 0 -column 0 -sticky snew
  grid columnconfigure $fh1 0 -weight 1
  grid rowconfigure $fh1 0 -weight 1

  # panel for MMS tree explorer
  set _ptree [ $pwv1 add -weight 5 ]
  # panel for show details
  set _details [ $pwv1 add -weight 10 ]

  # MMS tree explorer
  set _tree [bmmstree $_ptree.mmstree] 
  
  grid $_tree -row 0 -column 0
  grid columnconfigure $_ptree 0 -weight 1
  grid rowconfigure $_ptree 0 -weight 1
  
  # Details frame
  ::MMSGui::CreateMMSDetails $_details
  
  grid columnconfigure $_details 0 -weight 1
  grid rowconfigure $_details 0 -weight 1
  
  # In second pane, add 2 vertical panes too
  #set pwv2 [ PanedWindow $fh2.pwv2 -side top -pad 0 -weights available ]
  set pwv2 [ PanedWindow $fh2.pwv2 -side top -pad 0 ]
  grid $pwv2 -row 0 -column 0 -sticky snew
  grid columnconfigure $fh2 0 -weight 1
  grid rowconfigure $fh2 0 -weight 1

  # panel for output logs
  set _plog [ $pwv2 add -weight 10 ]
  # panel for show information
  set _info [ $pwv2 add -weight 10 ]

  # Output logs
  ::MMSGui::CreateMMSOutput $_plog
  
  grid columnconfigure $_plog 0 -weight 1
  grid rowconfigure $_plog 0 -weight 1

  # Information frame
  ::MMSGui::CreateMMSInfo $_info
  
  grid columnconfigure $_info 0 -weight 1
  grid rowconfigure $_info 0 -weight 1

  # Comunication between panels
  $_tree notify bind $_tree <ActiveItem> \
    "::MMSGui::ShowDetails %T %c"

  bind $_details <<Insert>> \
    "$_tree Insert"
    
  bind $_details <<Refresh>> \
    "$_tree Refresh"

  #$_tree notify bind $_tree <<ItemSelected>> \
    "$_details Edit"
}

# Functions for Output Log

#/////////////////////////////////////////////////////////////////////////////
proc CreateMMSOutput {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable _widgets
  
  #puts "CreateMMSOutput"
  label $f.lOutput -text [mc "Output log"]
  set foutput [labelframe $f.foutput \
    -labelwidget $f.lOutput -relief flat]

  scrollbar $foutput.sy -orient vertical -command "$foutput.text yview"
  scrollbar $foutput.sx -orient horizontal -command "$foutput.text xview"
  
  set _widgets(log) [text $foutput.text -height 4 \
      -wrap none -state disabled \
      -yscrollcommand "$foutput.sy set" \
      -xscrollcommand "$foutput.sx set"]
	  
  $_widgets(log) tag configure errorTag   -foreground red
  $_widgets(log) tag configure warningTag -foreground blue
  set _widgets(log,tag,E) errorTag
  set _widgets(log,tag,W) warningTag
 
  set menu [::BayesText::MenuCreate $_widgets(log)]
  $menu insert 0 separator
  $menu insert 0 command -label [mc "Delete all"] \
    -command [list ::BayesText::DeleteAll $_widgets(log)]
  $menu entryconfigure [mc "Delete"] -state normal
  bind $_widgets(log) <Delete> [list ::BayesText::Delete $_widgets(log)]
 
  grid $foutput.text -row 0 -column 0 -sticky snew
  grid $foutput.sx   -row 1 -column 0 -sticky snew
  grid $foutput.sy   -row 0 -column 1 -sticky snew
  grid columnconfigure $foutput 0 -weight 1
  grid rowconfigure    $foutput 0 -weight 1

  grid $f.foutput -row 0 -column 0 -sticky news
  grid rowconfigure $f 0 -weight 1
  grid columnconfigure $f 0 -weight 1
}

#/////////////////////////////////////////////////////////////////////////////
proc MMS_HciWriter {message} {
#/////////////////////////////////////////////////////////////////////////////
  #variable _widgets
  
  set w_output $::MMSGui::_widgets(log)
  #puts "MMS_HciWriter:w_output=$w_output,message=$message"
  
  $w_output configure -state normal
  set tr_message [string trim $message]
  # {<([^>]+)>([^<]+)</([^>]+)>}
  if { [regexp -- {^<([^>]+)>(.+)</(.+)>} $tr_message -> start content end] } {
    if {($start ne $end) || (($start ne "E") && ($start ne "W"))} {
      $w_output insert end "$message"
    } else { 
      set idx_start [$w_output index "end -1 char"]
      $w_output insert end "$content\n"
      set idx_end [$w_output index "end -1 char"]
      $w_output tag add $::MMSGui::_widgets(log,tag,$start) $idx_start $idx_end
    }
  } else {
    $w_output insert end "$message"
  }
  $w_output configure -state disabled
}

#/////////////////////////////////////////////////////////////////////////////
proc HookHciWriter { } {
#/////////////////////////////////////////////////////////////////////////////

  #puts "HookHciWriter"
  rename ::Tol_HciWriter ::Tol_HciWrite_Hooked
  rename ::MMSGui::MMS_HciWriter ::Tol_HciWriter
}

#/////////////////////////////////////////////////////////////////////////////
proc UnhookHciWriter { } {
#/////////////////////////////////////////////////////////////////////////////

  #puts "UnhookHciWriter"
  rename ::Tol_HciWriter ::MMSGui::MMS_HciWriter
  rename ::Tol_HciWrite_Hooked ::Tol_HciWriter
}

# Functions for Information

variable nbdata
set nbdata(nbdbmanager) [ nbdbmanagerType create %AUTO% ]
set nbdata(notebookdb) [ $nbdata(nbdbmanager) create "__tmp_info__" ]

#/////////////////////////////////////////////////////////////////////////////
proc CreateMMSInfo {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable _widgets
  variable nbdata
  
  #puts "CreateMMSInfo"
  label $f.lInfo -text [mc "Information"]
  set finfo [labelframe $f.finfo \
    -labelwidget $f.lInfo -relief flat]

  #set _widgets(info) [renderpane $finfo.r -linkcommand ::MMSGui::FollowLink \
                      -yscrollcommand "$finfo.scrY set" \
                      -messagecommand puts]
  set _widgets(info) [ markupviewer $finfo.r -db $nbdata(notebookdb) -showtitle 0 ]
					  
  scrollbar $finfo.scrY -orient vertical -command "$finfo.r yview"

  grid $finfo.r -row 0 -column 0 -sticky snew
  grid $finfo.scrY -row 0 -column 1 -sticky "ns"
  grid columnconfigure $finfo 0 -weight 1
  grid rowconfigure    $finfo 0 -weight 1

  ::autoscroll::autoscroll $finfo.scrY

  grid $f.finfo -row 0 -column 0 -sticky news
  grid rowconfigure $f 0 -weight 1
  grid columnconfigure $f 0 -weight 1
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowObjectInfo {icon grammar name content path desc objAddr} {
#/////////////////////////////////////////////////////////////////////////////

  #set w_output $::MMSGui::_widgets(info)
  
  #$w_output render "" [ PageExpander expand $message ]
  set mkinfo [ MarkupHelper::BuildTolbaseInfo $icon $grammar $name $content \
              $path $desc -objaddr $objAddr ]
			  
  ::MMSGui::ShowInfo $mkinfo
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowInfo {message} {
#/////////////////////////////////////////////////////////////////////////////
  variable nbdata

  set w_output $::MMSGui::_widgets(info)
  MarkupHelper::Init
  
  $nbdata(notebookdb) set "MMS_Info" $message
  $w_output showpage "MMS_info"
}

# Functions for Details

#/////////////////////////////////////////////////////////////////////////////
proc GetMMSDetails {} {
#/////////////////////////////////////////////////////////////////////////////
  variable _details
  
  return $_details
}

#/////////////////////////////////////////////////////////////////////////////
proc ClearMMSDetails {} {
#/////////////////////////////////////////////////////////////////////////////
  variable _details
  
  foreach {w} [winfo children $_details] {
    grid remove $w
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateMMSDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable previous_frame
  
  #puts "CreateMMSDetails"
  set previous_frame ""
  set details_frame [frame $f.details_MMS]

  #set _details [bmmsdetails $details_frame.details]

  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news

  DisactivateEdition
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowMMSDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame

  grid $details_frame

  _ShowMMSContainerHelp  

  return $details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowNetworkDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame

  grid $details_frame

  _ShowMMSNetworkHelp  

  return $details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMMSContainerHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMMSGui::GetMMSContainerHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMMSNetworkHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMMSGui::GetMMSNetworkHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowDetails {tree cur_item} {
#/////////////////////////////////////////////////////////////////////////////

  ClearMMSDetails
  set fdetails [$tree item text $cur_item [::MMSGui::FDetailsColumn]]
  if {$fdetails ne ""} {
	$fdetails $tree $cur_item
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowBottonGoto {} {
#/////////////////////////////////////////////////////////////////////////////
  variable btngoto
  
  grid $btngoto
}

#/////////////////////////////////////////////////////////////////////////////
proc HideBottonGoto {} {
#/////////////////////////////////////////////////////////////////////////////
  variable btngoto
  
  grid remove $btngoto
}

#/////////////////////////////////////////////////////////////////////////////
proc SetBottonGoto {btn} {
#/////////////////////////////////////////////////////////////////////////////
  variable btngoto
  
  set btngoto $btn
}

#/////////////////////////////////////////////////////////////////////////////
proc ActivateEdition {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable edition
  variable edit_frame
  variable edit_item

  #puts "ActivateEdition:parent=[winfo parent $f]"
  set edit_frame [winfo parent $f]
  
  set mms_tree [::MMSGui::GetMMSTree]
  set edit_item [$mms_tree item id active]
  
  ShowBottonGoto
  set edition 1
}

#/////////////////////////////////////////////////////////////////////////////
proc DisactivateEdition {} {
#/////////////////////////////////////////////////////////////////////////////
  variable edition
  
  HideBottonGoto
  set edition 0
}

#/////////////////////////////////////////////////////////////////////////////
proc IsEditionActive {} {
#/////////////////////////////////////////////////////////////////////////////
  variable edition

  return $edition
}

#/////////////////////////////////////////////////////////////////////////////
proc GotoEditWindow {} {
#/////////////////////////////////////////////////////////////////////////////
  variable edit_frame
  variable edit_item

  set mms_tree [::MMSGui::GetMMSTree]
  $mms_tree selection clear
  $mms_tree activate $edit_item
  $mms_tree selection add $edit_item
  
  ClearMMSDetails
  grid $edit_frame
}

# Functions for Tree

#/////////////////////////////////////////////////////////////////////////////
proc GetMMSTree {} {
#/////////////////////////////////////////////////////////////////////////////
  variable _tree
  
  return $_tree
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeMMSTree {new_ident new_absid} {
#/////////////////////////////////////////////////////////////////////////////
  variable _tree
  
  set id_active [$_tree item id active]

  $_tree item element configure $id_active first eTXT -text $new_ident
  $_tree item element configure $id_active [AbsIdColumn] eTXT -text $new_absid
  
  $_tree activate root
  $_tree activate $id_active
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandMMS {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _list {
	"DataSets"     "DataSetsGui::ExpandDataSetsList"
	               "DataSetsGui::ShowDataSetsListDetails"
				   "DataSetsGui::CMenuDataSets"
	"Models"       "MDMGui::ExpandModelsList"
       	           "MDMGui::ShowModelsListDetails"
				   "MDMGui::CMenuModels"
	"Estimations"  "EstimationsGui::ExpandEstimationsList"
	               "EstimationsGui::ShowEstimationsListDetails"
				   "EstimationsGui::CMenuEstimations"
	"Forecasts"    "ForecastsGui::ExpandForecastsList"
                   "ForecastsGui::ShowForecastsListDetails"
				   "ForecastsGui::CMenuForecasts"
	"Combinations" "CombinationsGui::ExpandCombinationsList"
	               "CombinationsGui::ShowCombinationsListDetails"
				   "CombinationsGui::CMenuCombinations"
	"Fits"         "FitsGui::ExpandFitsList"
	               "FitsGui::ShowFitsListDetails"
				   "FitsGui::CMenuFits"
  }

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
			
  set icon [::Bitmap::get "Set"]
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list $icon [mc $name]] \
	          [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
	          [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandNetwork {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _list {
    "Repositories" "MMSRepositoriesGui::ExpandRepositoriesList"
	               "MMSRepositoriesGui::ShowRepositoriesListDetails"
				   "MMSRepositoriesGui::CMenuRepositories"
  }

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set row [list [list [::Bitmap::get "list"] [mc "Connections"]] \
			[list "Connections"] \
	        [list ""] \
			[list "MMSConnectionsGui::ShowConnectionsListDetails"] \
			[list "MMSConnectionsGui::CMenuConnections"] \
			[list ""] \
          ]
  $tree insert $row \
    -at child -relative $id -button "no"
			
  set icon [::Bitmap::get "Set"]
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list $icon [mc $name]] \
	          [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
	          [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc PostCMenu {tree} {
#/////////////////////////////////////////////////////////////////////////////

  $tree.cmenu delete 0 end
  set selection [$tree selection get]
  set has_selection [llength $selection]
  set state_selection [expr {$has_selection?"normal":"disable"}]
	  
  if {$has_selection} {
    #$cmenu add separator
    set references [::MMSGui::GetRefSelected $tree]
		
    if {[llength $references]!=0} {
      ::MenuManager::insertEntriesForSelection $tree.cmenu $references
		
    } else {
	  if {$has_selection==1} {
	    set idx_found [string first [::MMSGui::GetMMSTree] $tree]
	    if {$idx_found != -1} {
	      set id [lindex $selection 0]
          set fcmenu [$tree item text $id [::MMSGui::FCMenuColumn]]
          if {$fcmenu ne ""} {
	        $fcmenu $tree
	      }
	    }
	  }
	}
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc GetRefSelected {tree} {
#/////////////////////////////////////////////////////////////////////////////

  set objIdentifiers [list]
  foreach item [$tree selection get 0 end] {
    set abs_id [$tree item text $item last]
	if {$abs_id ne ""} {
      lappend objIdentifiers $abs_id
	}
  }
  if {[llength $objIdentifiers] != 0} {
    set references [ LayerMMSGui::GetObjectsAddress $objIdentifiers ]
  } else {
	set references [list]
  }
  #puts "GetRefSelected:objIdentifiers=$objIdentifiers"
  #puts "GetRefSelected:references=$references"
  return $references
}

#/////////////////////////////////////////////////////////////////////////////
  
# Columns at MMS tree
  # 0 - Element Name (Identifier) - It will always be the first (Zero)
  # 1 - Element Type
  # 2 - Expand Function
  # 3 - Details Function
  # 4 - CMenu Function
  # 5 - Absolute Identifier       - It will always be the last

proc TypeColumn {} {
  return 1
}

proc FExpandColumn {} {
  return 2
}

proc FDetailsColumn {} {
  return 3
}

proc FCMenuColumn {} {
  return 4
}

proc AbsIdColumn {} {
  return 5
}

#/////////////////////////////////////////////////////////////////////////////


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmstree {
# PURPOSE : Defines the snit widget used to
#           list the components of MMS
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  #option

  variable mms_list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the components of MMS
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh, GoToEdit
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc "Refresh"] \
      -helptext [mc "Refresh"] -padx 1 -relief link \
      -compound left -command [mymethod Refresh]
  
    Button $fb.bGotoEdit -image [::Bitmap::get evalwin] -text [mc "Go to Edit"] \
      -helptext [mc "Go to Current Edit Window"] -padx 1 -relief link \
      -compound left -command "::MMSGui::GotoEditWindow"
  
    grid $fb.bRef -row 0 -column 0 -sticky w -padx 2 -pady 2
    grid $fb.bGotoEdit -row 0 -column 1 -sticky e -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
    ::MMSGui::SetBottonGoto	$fb.bGotoEdit
    grid remove $fb.bGotoEdit
	
    # Tree
    set ft [frame $f.ft]

    install tree as ::wtree $ft.tv -table 0 \
      -background white \
      -columns [ list \
        [list {image text} -label ""] \
        [list text] \
        [list text] \
        [list text] \
        [list text] \
        [list text] \
      ]
	  
	$tree column configure "range 1 end" -visible no
	$tree column configure tail -visible no
	$tree column configure first -expand yes -weight 1

	$tree column configure all -itembackground ""
	
    #$tree configure -filter "yes" -columnfilter {0 1}

    $tree notify bind $tree <Expand-before> \
      "$self Expand %I"
	
    $tree configure -contextmenu [$self CreateCMenu]

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    $self FillList
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    if {![info exist tree]} {
      return
    }
    
    $tree item delete all

    set icon [::Bitmap::get "mms_main_container"]
    set row [list [list $icon [mc "MMS Container"]] \
			  [list "MMS"] \
	          [list "MMSGui::ExpandMMS"] \
			  [list "MMSGui::ShowMMSDetails"] \
			  [list ""] \
			  [list "MMS::Container"] \
            ]

    set icon2 [::Bitmap::get "mms_network"]
    set row2 [list [list $icon2 [mc "MMS Network"]] \
			  [list "MMS"] \
	          [list "MMSGui::ExpandNetwork"] \
			  [list "MMSGui::ShowNetworkDetails"] \
			  [list ""] \
			  [list "MMS::Network"] \
            ]

    set id2 [$tree insert $row2 \
      -at end -relative "root"]
    set id [$tree insert $row \
      -at end -relative "root"]
	  
	MMSGui::ExpandMMS $tree $id
	$tree item expand $id

	MMSGui::ExpandNetwork $tree $id2
	$tree item expand $id2
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateCMenu { } {
  #/////////////////////////////////////////////////////////////////////////////

    menu $tree.cmenu -tearoff 0 \
      -postcommand [list ::MMSGui::PostCMenu $tree]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Expand {id} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set name [$tree item text $id 0]
    set fexpand [$tree item text $id [::MMSGui::FExpandColumn]]
    #puts "Expand: name=$name, fexpand=$fexpand"

    if {$fexpand ne ""} {
	  if {[$tree item numchildren $id] == 0} {
	    $fexpand $tree $id
      }	  
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Refresh {} {
  #/////////////////////////////////////////////////////////////////////////////

    set name [$tree item text active 0]
    set fexpand [$tree item text active [::MMSGui::FExpandColumn]]
    #puts "Expand name=$name, fexpand=$fexpand"

    if {$fexpand ne ""} {
	  $fexpand $tree active
	}
	$tree item expand active
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Insert {} {
  #/////////////////////////////////////////////////////////////////////////////

    set parent [$tree item parent active]
	$tree activate $parent
	$self Refresh
	set lastchild [$tree item lastchild $parent]
    $tree selection clear
	$tree activate $lastchild
    $tree selection add $lastchild
  }

}  

}
