#/////////////////////////////////////////////////////////////////////////////
# FILE:    MMS_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMMSGui {

#///////////////////////////////////////////////////////////////////////////
proc GetMMSContainerHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::GetMMSContainerHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetMMSNetworkHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::GetMMSNetworkHelp" \
    "Text" "?"
}

proc TclList2SetOfTextOK { lst } {
  set result [ list ]
  foreach i $lst {
    lappend result \"[string map {\" \\\"} $i]\"
  }
  return "SetOfText([join $result ,])"
}

#///////////////////////////////////////////////////////////////////////////
proc GetObjectsAddress { objIdentifiers } {
#///////////////////////////////////////////////////////////////////////////
  set setOfObj [ TclList2SetOfTextOK $objIdentifiers ]
  set stackLength [llength [ tol::console stack list ] ]
  set i [ expr { $stackLength + 1 } ]
  set tolExpr [ string map [ list %S $setOfObj ] {
    Set __MMS_layer_gui_tmp__ = EvalSet( %S, Text( Text ident ) {
      GetAddressFromObject( Eval( ident ) )
    } )
  } ]
  tol::console stack release __MMS_layer_gui_tmp__
  tol::console eval $tolExpr
  set value ""
  if { [ llength [ tol::console stack list ] ] == $i } {
    set value [ TolObj2TclObj [ list "Console" $i ] ]
    #tol::console stack release [ list "Console" $i ]
  }
  tol::console stack release __MMS_layer_gui_tmp__
  return $value
}

#///////////////////////////////////////////////////////////////////////////
proc GetObjectAddress { objIdentifier } {
#///////////////////////////////////////////////////////////////////////////
  set stackLength [llength [ tol::console stack list ] ]
  set i [ expr { $stackLength + 1 } ]
  set tolExpr [ string map [ list %S $objIdentifier ] {
    Text __MMS_layer_gui_tmp__ = GetAddressFromObject( %S )
  } ]
  tol::console stack release __MMS_layer_gui_tmp__
  tol::console eval $tolExpr
  set value ""
  if { [ llength [ tol::console stack list ] ] == $i } {
    set value [ TolObj2TclObj [ list "Console" $i ] ]
    #tol::console stack release [ list "Console" $i ]
  }
  tol::console stack release __MMS_layer_gui_tmp__
  return $value
}

#///////////////////////////////////////////////////////////////////////////
proc GetIcon { objIdentifier } {
#///////////////////////////////////////////////////////////////////////////
  return [::ImageManager::getIconForInstance \
    [GetObjectAddress $objIdentifier]]
}

#///////////////////////////////////////////////////////////////////////////
proc IsEmptyClipboard {} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFunVoid "MMS::Layer::IsEmptyClipboard" \
    "Real"
}

#///////////////////////////////////////////////////////////////////////////
proc GetClipboard {} {
#///////////////////////////////////////////////////////////////////////////
  
  set lobjects [LayerMMSGui::EvalTolFunVoid "MMS::Layer::GetClipboard" \
    "Set"]
  
  set _objects [list]
  foreach { {} obj } $lobjects {
    lappend _objects $obj
  }
  return $_objects
}


#///////////////////////////////////////////////////////////////////////////
proc NamesToIdentifier {names} {
#///////////////////////////////////////////////////////////////////////////

  set names_set [TclLst2TolSet $names -level 1]

  LayerMMSGui::EvalTolFunArgNoChg "MMS::MMS.NamesToIdentifier" \
    "Text" $names_set
}


#///////////////////////////////////////////////////////////////////////////
proc EvalTolConsole {grammar tol_expr} {
# PURPOSE : Evaluates a Tol expresion at the console.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  
  ::MMSGui::HookHciWriter
  tol::console stack release __tmp_mms_eval_result__
  
  tol::console eval $tol_expr
  
  ::MMSGui::UnhookHciWriter
  
  set vinfo [lindex [TolObj2TclObjNamed [string map \
    [list %Ag $grammar] \
	{
      %Ag __tmp_mms_eval_result__
    } \
  ]] 1]
  
  tol::console stack release __tmp_mms_eval_result__
  
  return $vinfo
}

#///////////////////////////////////////////////////////////////////////////
proc EvalTolFun {function grammar args} {
# PURPOSE : Evaluates a function at the Tol console.
#           "function" is a name of a Tol function.
#           "grammar" is the grammar of the function.
#           "args" are the arguments which will be passed as Texts.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  
  set functionargs \"[lindex $args 0]\"
  foreach {arg} [lrange $args 1 end] { 
    set functionargs "$functionargs, \"$arg\""
  }
  #puts "EvalTolFun:functionargs=$functionargs"

  set tol_expr [string map \
    [list %Af $function %Ag $grammar %Aa $functionargs] \
	{
      %Ag __tmp_mms_eval_result__ = %Af(%Aa)
    }
  ]
  EvalTolConsole $grammar $tol_expr
}

#///////////////////////////////////////////////////////////////////////////
proc EvalTolFunVoid {function grammar} {
# PURPOSE : Evaluates a function with argument "void" at the Tol console.
#           "function" is a name of a Tol function.
#           "grammar" is the grammar of the function.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  
  set tol_expr [string map \
    [list %Af $function %Ag $grammar] \
	{
      %Ag __tmp_mms_eval_result__ = %Af(?)
    }
  ]
  EvalTolConsole $grammar $tol_expr
}

#///////////////////////////////////////////////////////////////////////////
proc EvalTolFunArgNoChg {function grammar args} {
# PURPOSE : Evaluates a function at the Tol console.
#           "function" is a name of a Tol function.
#           "grammar" is the grammar of the function.
#           "args" are other arguments which will be passed whithout changes.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  
  set functionargs [join $args ,]
  #puts "EvalTolFunArgNoChg:functionargs=$functionargs"
  
  set tol_expr [string map \
    [list %Af $function %Ag $grammar %Aa $functionargs] \
	{
      %Ag __tmp_mms_eval_result__ = %Af(%Aa)
    }
  ]
  EvalTolConsole $grammar $tol_expr
}
  
#/////////////////////////////////////////////////////////////////////////////
proc TolText {val} {
# PURPOSE : Converts a Tcl value to a valid value of grammar Text Tol. 
#           For now, only escapes double quotes
#/////////////////////////////////////////////////////////////////////////////

  return [string map [list \" {\"} ] $val]
}

#///////////////////////////////////////////////////////////////////////////
proc TclArr2TolSetNamed {arrayname} {
# PURPOSE : Converts a Tcl array into a Tol Named Set of Text
#///////////////////////////////////////////////////////////////////////////
  upvar $arrayname arrayarg
  
  set names [array names arrayarg]

  if {$names=={}} {
    return "Set Empty"    
  }
  
  set tolset ""
  foreach {name} $names {
    set element "$name=\"[TolText $arrayarg($name)]\""
    set tolset "$tolset, $element"
  }
  set tolset [string trimleft $tolset ", "]
  set tolset "Set {SetOfText($tolset)}"
  return $tolset
}

#///////////////////////////////////////////////////////////////////////////
proc EvalTolConsoleArr {grammar tol_expr} {
# PURPOSE : Evaluates a Tol expresion at the console.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  
  ::MMSGui::HookHciWriter
  tol::console stack release __tmp_mms_eval_result__
  
  tol::console eval $tol_expr
  
  ::MMSGui::UnhookHciWriter

  set vinfo [lindex [TolObj2TclObjNamed [string map \
    [list %Ag $grammar] \
	{
      %Ag __tmp_mms_eval_result__
    } \
  ]] 1]
  
  tol::console stack release __tmp_mms_eval_result__
  
  return $vinfo
}

#///////////////////////////////////////////////////////////////////////////
proc EvalTolFunArr {function grammar arrayname args} {
# PURPOSE : Evaluates a function at the Tol console.
#           "function" is a name of a Tol function.
#           "grammar" is the grammar of the function.
#           "arrayname" is a Tcl array that is converted to a Tol Set of Text.
#           "args" are other arguments which will be passed whithout changes.
#           Returns an object of type "grammar" 
#///////////////////////////////////////////////////////////////////////////
  upvar $arrayname arrayarg
  
  set functionargs [TclArr2TolSetNamed arrayarg]
  if {[llength $args]!=0} { 
    set functionargs "$functionargs, [join $args ,]"
  }
  #puts "EvalTolFunArr:functionargs=$functionargs"
  
  set tol_expr [string map \
    [list %Af $function %Ag $grammar %Aa $functionargs] \
	{
      %Ag __tmp_mms_eval_result__ = %Af(%Aa)
    }
  ]
  EvalTolConsoleArr $grammar $tol_expr
}

#///////////////////////////////////////////////////////////////////////////
proc GetObjectHelp {className} {
#///////////////////////////////////////////////////////////////////////////
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::GetObjectHelp" \
    "Text" \"$className\"
}

#///////////////////////////////////////////////////////////////////////////
proc GetObjectInfo {abs_id} {
#///////////////////////////////////////////////////////////////////////////
  
  set abs_id_text \"[LayerMMSGui::TolText $abs_id]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::GetObjectInfo" \
    "Text" $abs_id_text
}

}
