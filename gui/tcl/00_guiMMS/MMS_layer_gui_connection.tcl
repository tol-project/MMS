#/////////////////////////////////////////////////////////////////////////////
# FILE:    MMS_layer_gui_connection.tcl
# PURPOSE: Communication layer between MMS and the GUI for Connections
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMMSConnectionsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetConnectionTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Connection 1"
    desc "desc of Connection 1"
    sour ""
    type "folder"
  }
}
  
variable get_connection_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetConnection {ident} {
#///////////////////////////////////////////////////////////////////////////
  variable get_connection_test
  
  if { $get_connection_test } {
    return [GetConnectionTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::MMSRepositoriesGui::GetConnection" \
    "Set" $ident
}

#/////////////////////////////////////////////////////////////////////////////
proc GetConnectionsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Connection 1"
	  type "folder"
    }
    {}
    {
      name "Connection 2"
	  type "database"
    }
  }
}
  
variable get_connections_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetConnectionsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_connections_test
  
  if { $get_connections_test } {
    return [GetConnectionsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::MMSRepositoriesGui::GetConnectionsList" \
    "Set" $details
}

#/////////////////////////////////////////////////////////////////////////////
proc GetConnections_AvailableList {} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_connections_test
  
  if { $get_connections_test } {
    return [GetConnectionsListTest]
  }
  LayerMMSGui::EvalTolFunVoid "MMS::Layer::MMSRepositoriesGui::GetConnections_AvailableList" \
    "Set"
}

#///////////////////////////////////////////////////////////////////////////
proc IsConnectionAvailable {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::MMSRepositoriesGui::IsConnectionAvailable" \
    "Real" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc GetConnectionHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MMSRepositoriesGui::GetConnectionHelp" \
    "Text" "?"
}

variable create_connection_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateConnection {connection_info} {
#///////////////////////////////////////////////////////////////////////////
  variable create_connection_test
  upvar $connection_info arrayarg
  
  if { $create_connection_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MMSRepositoriesGui::CreateConnectionTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MMSRepositoriesGui::CreateConnection" \
	  "Real" arrayarg
  }
}

variable edit_connection_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditConnection {connection_info} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_connection_test
  upvar $connection_info arrayarg
  
  if { $edit_connection_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MMSRepositoriesGui::EditConnectionTest" \
	  "Real" arrayarg

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::MMSRepositoriesGui::EditConnection" \
	  "Real" arrayarg
  }
}

#///////////////////////////////////////////////////////////////////////////
proc DeleteConnection {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::MMSRepositoriesGui::DeleteConnection" \
    "Real" $ident
}

}
