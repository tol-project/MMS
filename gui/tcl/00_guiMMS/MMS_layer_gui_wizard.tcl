#/////////////////////////////////////////////////////////////////////////////
# FILE:    MMS_layer_gui_wizard.tcl
# PURPOSE: Communication layer between MMS and the GUI for Wizard
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMMSWizardGui {

variable entry1 ""
variable entry2 ""
variable entry3 ""

    proc save_data {path} {
        variable entry1
        variable entry2
        variable entry3

        set file [tk_getSaveFile -title "Guardar configuración" -defaultextension ".txt" -filetypes {{"Archivo de texto" {.txt}} {"Todos los archivos" {*}}}]

        if {$file != ""} {
            set f [open $file w]
            puts $f "Company: $entry1"
            puts $f "Project: $entry2"
            puts $f "Repository Root: $entry3"
            close $f
        }
		set ::___TclResult__ [list $entry1 $entry2 $entry3]
		set ::changes_saved 1
		destroy $path
    }

    proc load_data {filepath} {
        variable entry1
        variable entry2
        variable entry3

        if {[file exists $filepath]} {
            if {[catch {open $filepath r} f]} {
                return
            }

            while {[gets $f line] >= 0} {
                if {[regexp {^Company:\s*(.*)} $line -> value]} {
                    set entry1 $value
                } elseif {[regexp {^Project:\s*(.*)} $line -> value]} {
                    set entry2 $value
                } elseif {[regexp {^Repository Root:\s*(.*)} $line -> value]} {
                    set entry3 $value
                }
            }
            close $f
        }
    }

    proc onCloseWindow {root initial_values} {
    set ::___TclResult__ $initial_values
	set ::changes_saved 1
    destroy $root
    }

	
	proc test_ui {root args} {
        variable entry1
        variable entry2
        variable entry3

        # Ruta del archivo de configuración
        set config_file_path "c:/config.txt"

        # Inicializar variables de texto de entrada
        set entry1 ""
        set entry2 ""
        set entry3 ""

        # Cargar datos del archivo de configuración
        load_data $config_file_path
		
		# Valores iniciales
		set initial_values [list $entry1 $entry2 $entry3]

        label $root.label#3 -text "Company"
        entry $root.entry#2 -textvariable [namespace current]::entry1

        label $root.label#4 -text "Project"
        entry $root.entry#3 -textvariable [namespace current]::entry2

        label $root.label#5 -text "Repository Root"
        entry $root.entry#4 -textvariable [namespace current]::entry3

        button $root.button#1 -command [list [namespace current]::save_data $root] -text "Guardar datos"
		
		button $root.button#2 -command {
        set ::___TclResult__ $initialValues
		set ::changes_saved 1
        destroy $root
        } -text "Salir"			
			
		
        grid $root.label#3 -in $root -row 1 -column 1
        grid $root.entry#2 -in $root -row 1 -column 2
        grid $root.label#4 -in $root -row 2 -column 1
        grid $root.entry#3 -in $root -row 2 -column 2
        grid $root.label#5 -in $root -row 3 -column 1
        grid $root.entry#4 -in $root -row 3 -column 2
        grid $root.button#1 -in $root -row 4 -column 1
		grid $root.button#2 -in $root -row 4 -column 2
		
		# Vincular el evento de cerrar ventana
        bind $root <Destroy> [list [namespace current]::onCloseWindow $root $initial_values]
		
		# Obtener dimensiones de la pantalla
        set screen_width [winfo screenwidth .]
        set screen_height [winfo screenheight .]
				
		# Obtener dimensiones de la ventana
        update idletasks
        set window_width [winfo reqwidth $root]
	    set window_height [winfo reqheight $root]

        # Calcular las coordenadas del centro de la pantalla
        set x_position [expr {($screen_width - $window_width) / 2}]
        set y_position [expr {($screen_height - $window_height) / 2}]

        # Establecer la posición de la ventana en el centro de la pantalla
        wm geometry $root +$x_position+$y_position
    }

}
