#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_repository_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Repositories
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MMSRepositoriesGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateRepositoriesListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateRepositoriesListDetails"
  set l_details_frame [frame $f.details_Repositories]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Repository" \
    -swlist "::MMSRepositoriesGui::bmmsreplist" \
	-fshowitem "::MMSRepositoriesGui::_ShowRepositoryDetails" \
	-fshowlist "::MMSRepositoriesGui::_ShowRepositoriesListDetails" \
	-onlycreate "yes" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowRepositoriesListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateRepositoriesListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details Init
  $l_details_frame.details FillList

  return $l_details_frame
}
  
#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoriesListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  _ShowRepositoryHelp  

  _ShowRepositoriesListDetails "MMS"
}
  
#/////////////////////////////////////////////////////////////////////////////
proc CMenuRepositories {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Repository"] \
    -command "::MMSRepositoriesGui::NewRepository" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewRepository {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowRepositoryHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMMSRepositoriesGui::GetRepositoryHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateRepositoryDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateRepositoryDetails"
  set details_frame [frame $f.details_Repository]
  set view_frame [frame $f.view_Repository]

  set _details [bmmsrepository $details_frame.details]
  set _view [bmmsrepository $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowRepositoryDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateRepositoryDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container "MMS"
  set ident [$tree item text $id first]

  _ShowRepositoryDetails $container $ident "View" "tree"
}
  
#/////////////////////////////////////////////////////////////////////////////
proc ExpandRepositoriesList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _details "no" 

  set _list [LayerMMSRepositoriesGui::GetRepositoriesList $_details]
  #puts "ExpandRepositoriesList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    set icon [::ImageManager::getIconForInstance \
      [LayerMMSGui::GetObjectsAddress $vinfo(abs_id)]]	
    set row [list [list $icon $vinfo(ident)] \
	          [list "Repository"] \
	          [list "MMSRepositoriesGui::ExpandRepository"] \
			  [list "MMSRepositoriesGui::ShowRepositoryDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandRepository {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _list {
	"DataSets"     ""
	               "MMSRepositoriesGui::ShowRepositoryDataSetsList"
				   ""
	"Models"       ""
       	           "MMSRepositoriesGui::ShowRepositoryModelsList"
				   ""
	"Estimations"  ""
	               "MMSRepositoriesGui::ShowRepositoryEstimationsList"
				   ""
	"Forecasts"    ""
                   "MMSRepositoriesGui::ShowRepositoryForecastsList"
				   ""
	"Combinations" ""
	               "MMSRepositoriesGui::ShowRepositoryCombinationsList"
				   ""
	"Fits"         ""
	               "MMSRepositoriesGui::ShowRepositoryFitsList"
				   ""
  }

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set icon [::Bitmap::get "Set"]
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list $icon [mc $name]] \
			  [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id -button no
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateRepositoryObjectsList {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_container_frame
  
  set l_container_frame [frame $f.details_RepositoryObjects]

  set _details [::MMSRepositoriesGui::bmmsobjlist $l_container_frame.details]

  grid rowconfigure $l_container_frame 0 -weight 1
  grid columnconfigure $l_container_frame 0 -weight 1
  grid $l_container_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowRepositoryObjectsList {container subclass} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_container_frame

  if {![info exists l_container_frame] || ![winfo exists $l_container_frame]} {
    CreateRepositoryObjectsList [::MMSGui::GetMMSDetails]
  } else {
    grid $l_container_frame
  }
  $l_container_frame.details configure -container $container
  $l_container_frame.details configure -subclass $subclass
  $l_container_frame.details Init
}
  
#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryObjectsList {tree id subclass} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set repository [$tree item text $parent first]
  
  _ShowRepositoryObjectsList $repository $subclass
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryDataSetsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::DataSetsGui::_ShowDataSetHelp  

  ShowRepositoryObjectsList $tree $id "DataSet"
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryModelsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::MDMGui::_ShowModelHelp  

  ShowRepositoryObjectsList $tree $id "Model"
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryEstimationsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::EstimationsGui::_ShowEstimationHelp  

  ShowRepositoryObjectsList $tree $id "Estimation"
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryForecastsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::ForecastsGui::_ShowForecastHelp  

  ShowRepositoryObjectsList $tree $id "Forecast"
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryCombinationsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::CombinationsGui::_ShowCombinationHelp  

  ShowRepositoryObjectsList $tree $id "Combination"
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowRepositoryFitsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  ::FitsGui::_ShowFitHelp  

  ShowRepositoryObjectsList $tree $id "Fit"
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsreplist {
# PURPOSE : Defines the snit widget used to
#           list the Repositories of MMS
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container to wich the repositories belongs
  option -container \
    -default "MMS" 

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mms_replist
    # mms_replist(list)  - Repositories list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Repositories of MMS
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Repositories List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]

	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Connection"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Type"]] \
        [list text -label [mc "Source"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    focus $tree
    
    set mms_replist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mms_replist(list) [LayerMMSRepositoriesGui::GetRepositoriesList \
	  $options(-details)]
    #puts "FillList mms_replist(list)=$mms_replist(list)"

    $tree item delete all

    foreach {{} it} $mms_replist(list) {
      array set vinfo $it
      #puts "FillList vinfo=[array get vinfo]"

      set icon [::ImageManager::getIconForInstance \
        [LayerMMSGui::GetObjectsAddress $vinfo(abs_id)]]	
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(type)] \
	      [list $vinfo(sour)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }

      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }
 
  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
             [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsobjlist {
# PURPOSE : Defines the snit widget used to
#           list the Objects of a Repository
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the Repository to wich the objects belongs
  option -container \
    -default "" -configuremethod "_conf-container" 

  # Subclass of the objects
  option -subclass \
    -default "" -configuremethod "_conf-subclass" 

  variable widgets
  
  variable mms_replist
    # mms_replist(list)  - Objects list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
    
  #///////////////////////////////////////////////////////////////////////////
  method _conf-subclass { _ subclass } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-subclass) $subclass 
    $self FillList
  }
    
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the objects of a Container of a Repository
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Buttons: Load
    set fbu [frame $f.fbu]
  
    Button $fbu.bLoad -image [::Bitmap::get "bopen"] -text [mc "Load"] \
      -helptext [mc "Load Objects"] -padx 1 -relief link \
      -compound left -command [list $self Load] \
      -state normal
        
	set widgets [list $fbu.bLoad]

    grid $fbu.bLoad -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 1 -weight 1
  
    # Tree frame
    label $f.lHeader -text [mc "List of Objects"] \
	  -pady 5 -padx 5
    set ft [labelframe $f.ft \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    # Button: Refresh
    set fb [frame $ft.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Objects List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set fto [frame $ft.fto]

	set _columns [ list \
      [list text] \
      [list {image text} -label [mc "Name"]] \
      [list text -label [mc "Version"]] \
      [list text -label [mc "Creation"]] \
      [list text -label [mc "Modification"]] \
      [list text -label [mc "Description"]] \
    ] 
    install tree as ::wtree $fto.tv -table 1 \
      -background white \
      -columns $_columns 

	$tree column configure tail -visible no
	$tree column configure first -visible no
	#$tree column configure last -visible no
	$tree column configure "order 1" -expand yes -weight 1

    $tree configure -filter "yes" -columnfilter {0 0 1 1}

    $tree notify bind $tree <ActiveItem> \
      "$self ItemIsActive"
    
    $tree configure -contextmenu [$self CreateCMenu]

    grid $fto.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $fto 0 -weight 1
    grid columnconfigure $fto 0 -weight 1
  
    grid rowconfigure $ft 1 -weight 1
    grid columnconfigure $ft 0 -weight 1

    grid $fb -sticky news
    grid $fto -sticky news
    grid rowconfigure    $ft 1 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $fbu -sticky news
    grid $ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    focus $tree
    
    set mms_replist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set version [LayerMMSRepositoriesGui::GetVersion $options(-container)]

    if {$version eq 0} {
      set mms_replist(list) [LayerMMSRepositoriesGui::GetObjectsList \
	    $options(-container) $options(-subclass)]
    } elseif {$version eq 1} {
      set mms_replist(list) [LayerMMSRepositoriesGui::GetObjectsCatalog \
	    $options(-container) $options(-subclass)]
    } elseif {$version eq 2} {
      set mms_replist(list) [LayerMMSRepositoriesGui::GetObjectsListIncr \
	    $options(-container) $options(-subclass)]
    }
    #puts "FillList mms_replist(list)=$mms_replist(list)"

    $tree item delete all

    foreach {{} it} $mms_replist(list) {
      array set vinfo $it
      #puts "FillList vinfo=[array get vinfo]"

      switch -- $options(-subclass) {
        "DataSet"     {set icon [::Bitmap::get "mms_dataset"]}
        "Model"       {set icon [::Bitmap::get "mms_model"]}
        "Estimation"  {set icon [::Bitmap::get "mms_estimation"]}
        "Forecast"    {set icon [::Bitmap::get "mms_forecast"]}
        "Combination" {set icon [::Bitmap::get "mms_combination"]}
        "Fit"         {set icon [::Bitmap::get "mms_fit"]}
        default       {set icon ""}
      }

      if {$version eq 0} {
 	    set row [ list \
          [list $vinfo(identifier)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(version)] \
        ] 
      } {	  
        set row [ list \
          [list $vinfo(identifier)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(version)] \
	      [list $vinfo(creationTime)] \
	      [list $vinfo(modificationTime)] \
	      [list $vinfo(description)] \  
        ] 
      }

      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }
 
  #/////////////////////////////////////////////////////////////////////////////
  method CreateCMenu { } {
  #/////////////////////////////////////////////////////////////////////////////

    menu $tree.cmenu -tearoff 0 \
      -postcommand [mymethod PostCMenu $tree.cmenu]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method PostCMenu {cmenu} {
  #/////////////////////////////////////////////////////////////////////////////

    $cmenu delete 0 end
    $cmenu add command -label [mc "Load"] \
	  -command "[mymethod Load]"
    $cmenu add command -label [mc "Delete"] \
	  -command "[mymethod Delete]"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Load { } {
  #/////////////////////////////////////////////////////////////////////////////
  
    set selected [$tree selection get]
    foreach {item} $selected {
      set name [$tree item text $item 1]
      set vers [$tree item text $item 2]
      LayerMMSRepositoriesGui::LoadObjects $name $vers \
	    [$self cget -container] [$self cget -subclass]
	  #puts "Load: name=$name, vers=$vers"
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Delete { } {
  #/////////////////////////////////////////////////////////////////////////////
  
    set answer [tk_messageBox \
      -message [mc "Do you want to delete the selected objects?"] \
	  -type yesno -icon question]
    if {$answer eq yes} {
      set selected [$tree selection get]
      foreach {item} $selected {
        set name [$tree item text $item 1]
        set vers [$tree item text $item 2]
        LayerMMSRepositoriesGui::DeleteObjects $name $vers \
	      [$self cget -container] [$self cget -subclass]
      }
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Init {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} $widgets {
      $w configure -state disabled
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ItemIsActive {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} $widgets {
      $w configure -state normal
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
             [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsrepository {
# PURPOSE : Defines the snit widget used to
#           create new repositories or view existing ones
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # The container to wich the repository belongs
  option -container \
    -default "MMS"
	
  # Identifier of the repository to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Repositories List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, New)
  option -state \
    -default "Details" -configuremethod "_conf-state"  

  variable label_state

  variable widgets
    
  variable repository_info
  # Data of the repository to be edited
   # (Class @Repository)
    # repository_info(name)         - Connection (Text _.connection)
     # repository_info(desc)        - Connection Description (Text)
     # repository_info(type)        - Connection Type (Text) (folder, mysql) 
     # repository_info(sour)        - Connection Source (Text) 
     #                              - type == "folder"
      # repository_info(path)       - Connection Path (Text) 
     #                              - type == "mysql"
      # repository_info(odbc)       - Connection Odbc (Text) 
      # repository_info(sque)       - Connection Schema (Text) 
      # repository_info(user)       - Connection User (Text) 
      # repository_info(pass)       - Connection Password (Text) 
    # (Class @RepositoryFolder)
     #                              - (Text _.path)	
     #                              - (Set _.relativePaths)
    # (Class @RepositoryDatabase)
     #                              - (Text _.odbc)	
     #                              - (Text _.schema)	
      # (Class @RepositoryMySQL)

  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item 
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "New"   {$self New}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Repository
  #///////////////////////////////////////////////////////////////////////////
    
    set f $dlg
    
    # Buttons: New
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Repository"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    grid $fbu.bNew -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 1 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lConn -text "[mc "Connection"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Type"]:" -pady 5 -padx 5
    
    ::MMSSelectorsGui::comboselector $fe.cConnSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Connection"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "Connection" \
	  -swlist "::MMSConnectionsGui::bmmsconnlist" \
	  -available "yes"
    set widgets(conn) $fe.cConnSel
    
    bind $widgets(conn) <<OnAccept>> "$self UpdateConnectionFields"
	
    entry $fe.eDesc -textvariable [myvar repository_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc
    
    entry $fe.eType -textvariable [myvar repository_info(type)] \
      -width 15 -state readonly
    set widgets(type) $fe.eType
        
    grid $fe.lConn  -row 0 -column 0 -sticky e
    grid $fe.cConnSel  -row 0 -column 1 -sticky w
    grid $fe.lDesc  -row 1 -column 0 -sticky e
    grid $fe.eDesc  -row 1 -column 1 -sticky w
    grid $fe.lType  -row 2 -column 0 -sticky e
    grid $fe.eType  -row 2 -column 1 -sticky w

    label $fe.lHSource -text [mc "Source"] -pady 5 -padx 5
    set fsource [labelframe $fe.fsource \
      -labelwidget $fe.lHSource -relief solid -bd 1]

    label $fsource.lPath -text "[mc "Path"]:" -pady 5 -padx 5
    set widgets(lpath) $fsource.lPath
    
    label $fsource.lOdbc -text "[mc "Odbc"]:" -pady 5 -padx 5
    label $fsource.lSque -text "[mc "Schema"]:" -pady 5 -padx 5
    label $fsource.lUser -text "[mc "User"]:" -pady 5 -padx 5
    label $fsource.lPass -text "[mc "Password"]:" -pady 5 -padx 5
    set widgets(lodbc) $fsource.lOdbc
    set widgets(lsque) $fsource.lSque
    set widgets(luser) $fsource.lUser
    set widgets(lpass) $fsource.lPass

    entry $fsource.ePath -textvariable [myvar repository_info(path)] \
      -width 60 -state readonly
    set widgets(path) $fsource.ePath
    
    entry $fsource.eOdbc -textvariable [myvar repository_info(odbc)] \
      -width 40 -state readonly
    set widgets(odbc) $fsource.eOdbc
    
    entry $fsource.eSque -textvariable [myvar repository_info(sque)] \
      -width 40 -state readonly
    set widgets(sque) $fsource.eSque
    
    entry $fsource.eUser -textvariable [myvar repository_info(user)] \
      -width 20 -state readonly
    set widgets(user) $fsource.eUser
    
    entry $fsource.ePass -textvariable [myvar repository_info(pass)] \
      -width 20 -state readonly
    set widgets(pass) $fsource.ePass

    grid $fsource.lOdbc  -row 0 -column 0 -sticky e
    grid $fsource.eOdbc  -row 0 -column 1 -sticky w
    grid $fsource.lSque  -row 1 -column 0 -sticky e
    grid $fsource.eSque  -row 1 -column 1 -sticky w
    grid $fsource.lUser  -row 2 -column 0 -sticky e
    grid $fsource.eUser  -row 2 -column 1 -sticky w
    grid $fsource.lPass  -row 3 -column 0 -sticky e
    grid $fsource.ePass  -row 3 -column 1 -sticky w
    foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
      grid remove $widgets($w)
    }
	
    grid $fsource.lPath -row 0 -column 0 -sticky e
    grid $fsource.ePath -row 0 -column 1 -sticky w -padx 5

    grid rowconfigure    $fsource 4 -weight 1
    grid columnconfigure $fsource 1 -weight 1

    grid $fsource -row 3 -column 0 -columnspan 2 -sticky news -pady 10 -padx 10

	grid rowconfigure    $fe 4 -weight 1
    grid columnconfigure $fe 2 -weight 1
	
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1

    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
	
    foreach {w} {accept cancel \
	             conn} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {conn \
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(conn) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateConnectionFields {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetConnection
    set ident $repository_info(name)
	
    array set repository_info [LayerMMSConnectionsGui::GetConnection $ident]

    $self UpdateTypeFields
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateTypeFields {} {
  #/////////////////////////////////////////////////////////////////////////////

    switch -- $repository_info(type) {
      "folder" {
        foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
          grid remove $widgets($w)
        }
        foreach {w} {lpath path} {
          grid $widgets($w)
        }
      }
      "mysql" {
        foreach {w} {lpath path} {
          grid remove $widgets($w)
        }
        foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
          grid $widgets($w)
        }
      }
	  "" {
        foreach {w} {lpath path} {
          grid remove $widgets($w)
        }
        foreach {w} {lodbc odbc lsque sque luser user lpass pass} {
          grid remove $widgets($w)
        }
	  }
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetConnection {} {
  #/////////////////////////////////////////////////////////////////////////////

    set repository_info(name) [$widgets(conn) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetConnection {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(conn) transient FillList
    $widgets(conn) set_info $repository_info(name)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set repository_info(name) ""
    $self SetConnection
    
	set repository_info(desc) ""
	
    set repository_info(type) ""
	$self UpdateTypeFields

    set repository_info(path) ""

    set repository_info(odbc) ""
    set repository_info(sque) ""
    set repository_info(user) ""
    set repository_info(pass) ""
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

    array set repository_info [LayerMMSRepositoriesGui::GetRepository $ident]
	
    $self SetConnection
	$self UpdateTypeFields
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Repository"]

    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    $widgets(conn) button configure -state disabled
    
	$self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    $widgets(conn) button configure -state normal

    focus $widgets(conn)
    bind $widgets(cancel) <Tab> "focus $widgets(conn) ; break"
    bind $widgets(conn) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Repository"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
    
    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $self GetConnection
	
    LayerMMSRepositoriesGui::CreateRepository repository_info
	if {$options(-parent) eq "tree"} {
      event generate $self <<Insert>>
	} else {
      event generate $self <<Refresh>>
	}
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition

	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
