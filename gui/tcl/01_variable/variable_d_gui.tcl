#/////////////////////////////////////////////////////////////////////////////
# FILE    : variable_d_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the dependent variables of a dataset
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

#<particular>
namespace eval ::VariablesDGui {

variable local
set local(name) "Dep. Variable"
set local(class) VariableD
set local(namespace) ::VariablesDGui
set local(layer) LayerVariablesDGui
#</particular>

#/////////////////////////////////////////////////////////////////////////////
proc CreateListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  variable l_details_frame
  set l_details_frame [frame $f.details_List$local(class)]
  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "$local(class)" \
    -swlist "$local(namespace)::blist" \
    -fshowmode 1 \
	-fshowitem "$local(namespace)::_ShowDetails" \
	-fshowinfo "$local(namespace)::_ShowInfo" \
	-fshowlist "$local(namespace)::_ShowListDetails"
  ]
  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
  ConfigEventsCMenu
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowListDetails {containerid} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -containerid $containerid
  $l_details_frame.details Init
  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  set container [$tree item parent $id]
  set containerid [$tree item text $container last]
  _ShowHelp  
  _ShowListDetails $containerid
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenu {tree} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New $local(name)"] \
    -command "$local(namespace)::New" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc New {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHelp {} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  set message [LayerMMSGui::GetObjectHelp $local(class)]
  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  variable details_frame
  variable view_frame
  set details_frame [frame $f.details_$local(class)]
  set view_frame [frame $f.view_$local(class)]
  set _details [bobject $details_frame.details]
  set _view [bobject $view_frame.details]
  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"
  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"
  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowDetails {ident objectid containerid state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  # Es necesario configurar -objectid antes que -item
  $_frame.details configure -objectid $objectid
  $_frame.details configure -containerid $containerid
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  if {$state ne "New"} {
    _ShowInfo $objectid
  }
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowInfo {objectid} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  set objaddr [LayerMMSGui::GetObjectsAddress $objectid]
  array set objinfo [$local(layer)::GetInfo $objectid]
  set icon [::ImageManager::getIconForInstance $objaddr]	
  ::MMSGui::ShowObjectInfo $icon "NameBlock" $objinfo(name) \
    "MMS::@$local(class)" "" $objinfo(description) $objaddr
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  set ident [$tree item text $id first]
  set objectid [$tree item text $id last]
  set container [$tree item parent [$tree item parent $id]]
  set containerid [$tree item text $container last]
  _ShowDetails $ident $objectid $containerid "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandList {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  variable local
  set container [$tree item parent $id]
  set containerid [$tree item text $container last]
  set _details "no"
  set _list [$local(layer)::GetList $containerid $_details]
  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  foreach {{} it} $_list {
    # Los elementos de la lista se esperan con "identifier" 
    # y "absoluteIdentifier"
    array set vinfo $it
    set icon [::ImageManager::getIconForInstance \
      [LayerMMSGui::GetObjectsAddress $vinfo(absoluteIdentifier)]]	
    set row [list [list $icon $vinfo(identifier)] \
	          [list "$local(class)"] \
	          [list ""] \
			  [list "$local(namespace)::ShowDetails"] \
              [list ""] \
			  [list $vinfo(absoluteIdentifier)] \
            ]
    set tags [list]
    $tree insert $row \
      -at child -relative $id -tags $tags -button no
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ConfigEventsCMenu {} {
#/////////////////////////////////////////////////////////////////////////////
  #(pgea) Se usa?
  variable local
  #::MenuManager::addObserver "MMS.Variable_Delete" "Before" \
	$local(namespace)::onDeleteVariable
  #::MenuManager::addObserver "MMS.Variable_Delete" "After" \
	$local(namespace)::onDeleteVariable
  #::MenuManager::addObserver "MMS.Variable_Delete_Group" "Before" \
	$local(namespace)::onDeleteVariable
  #::MenuManager::addObserver "MMS.Variable_Delete_Group" "After" \
	$local(namespace)::onDeleteVariable
}

#/////////////////////////////////////////////////////////////////////////////
proc onDeleteVariable {when objSelection} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  #variable containers

  set mms_tree [::MMSGui::GetMMSTree]
  
  #puts "onDeleteVariable:objSelection=$objSelection"
  if {$when eq "Before"} {
    return 1
	
    set selected [list]
    foreach {id} [$mms_tree item id all] {
	
	  set type [$mms_tree item text $id 4]
	  if {$type eq "VariableD"} {
        set absoluteIdentifier [$mms_tree item text $id 3]
	    if {$absoluteIdentifier ne ""} {
          set reference [LayerMMSGui::GetObjectAddress $absoluteIdentifier]
	      if {[lsearch $objSelection $reference] >= 0} {
            lappend selected $id
	      }
	    }
	  }
    }
    #puts "onDeleteVariable selected=$selected"
  
    set containers [list]
    foreach {id} $selected {
      set parent [$mms_tree item parent $id]
	
	  if {[lsearch $containers $parent] < 0} {
        lappend containers $parent
	  }
    }
    #puts "onDeleteVariable containers=$containers"
	
  } else {          ;# "After"

    #puts "onDeleteVariable containers=$containers"
    #puts "onDeleteVariable: mms_tree=$mms_tree"
    set containers [list]
    foreach {id} [$mms_tree item id all] {

	  set type [$mms_tree item text $id [::MMSGui::TypeColumn]]
      if {$type eq "Variables"} {
        lappend containers $id
	  }
    }

	set id_active [$mms_tree item id active]
    foreach {id} $containers {
	  
      set fexpand [$mms_tree item text $id [::MMSGui::FExpandColumn]]
      if {$fexpand ne ""} {
	    $fexpand $mms_tree $id
	    if {[$mms_tree item isopen $id]} {
	      $mms_tree item expand $id
        }
	  } 
	  if {$id eq $id_active} {
        $l_details_frame.details RefreshList
	    set _active [$l_details_frame.details GetActiveItem]
	    #puts "onDeleteVariable:_active=$_active"
		if {$_active eq ""} {
		  $l_details_frame.details Init
		}
	  }
    }

    #set sibling [$mms_tree item nextsibling $item]
    #if {$sibling != ""} {
      #set ident [$tree item text $sibling 0]
    #}
    #if {$sibling == ""} {
      #if {[$tree item count] != 1} {
        #set ident [$tree item text "rnc 0 0" 0]
      #} else {
        #set ident ""
      #}
    #}
    #$self MakeActiveItem $ident
  }
  
  return 1
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget blist {
# PURPOSE : Defines the snit widget used to list the objects
#/////////////////////////////////////////////////////////////////////////////

  #typevariable
  #<particular>
  option -layer -default LayerVariablesDGui
  #</particular>
  option -containerid \
    -default "" -configuremethod "_conf-containerid"
  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable vlist
    # vlist(tags)  - Keywords or tags
    # vlist(list)  - Objects list
  variable tree

  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor {args} {
  #///////////////////////////////////////////////////////////////////////////
    # Dialog
    install dlg as frame $win.d 
    # Apply all options passed at creation time.
    $self configurelist $args
    # Paint the window
    $self _create
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew
    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-containerid {_ containerid} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-containerid) $containerid 
    $self FillList
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-details {_ details} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-details) $details 
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create {} {
  #
  # PURPOSE: Creates the contents of a frame in order to list the objects
  #///////////////////////////////////////////////////////////////////////////
    set f $dlg
    # Button: Refresh
    set fb [frame $f.fb]
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Object List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
    # Tree in form of table
    set ft [frame $f.ft]
    #<particular>
    # Coordinado con la funci�n TOL GetList 
    # del archivo Layer "layer_gui_<object>.tol"
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text -tags IDENT -label [mc "Identifier"]] \
        [list {image text} -tags NAME -label [mc "Name"]] \
        [list text -tags TYPE -label [mc "Data Type"]] \
        [list text -tags EXPR -label [mc "Expresion"]] \
        [list text -tags DESC -label [mc "Description"]] \
        [list text -tags TAGS -label [mc "Tags"]] \
        [list text -tags ABSID -label [mc "Absolute Identifier"]]
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -tags IDENT -label [mc "Identifier"]]
      ] 
	}
    #</particular>
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}
    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    focus $tree
    set vlist(list) ""
  }

  #///////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #///////////////////////////////////////////////////////////////////////////
    #<particular>
    # Coordinado con la funci�n TOL GetList 
    # del archivo Layer "layer_gui_<object>.tol"
    set vlist(list) [[$self cget -layer]::GetList \
      $options(-containerid) $options(-details)]
    $tree item delete all
    foreach {{} it} $vlist(list) {
      array set vinfo $it
      set icon [::ImageManager::getIconForInstance \
        [LayerMMSGui::GetObjectsAddress $vinfo(absoluteIdentifier)]]	
	  if {$options(-details) eq "yes"} {
        set tags [list]
        foreach { {} t } $vinfo(tags) {
          lappend tags $t
        }
        set st_tags [join $tags ,]
	    set row [ list \
          [list $vinfo(identifier)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(type)] \
	      [list $vinfo(expression)] \
	      [list $vinfo(description)] \
	      [list $st_tags] \
	      [list $vinfo(absoluteIdentifier)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(identifier)]
        ] 
 	  }
      set tags [list]
      foreach {{} t} $vinfo(tags) {
        lappend tags $t
      }
      set id [$tree insert $row \
         -at end -relative "root" -tags $tags]
    }
    #</particular>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }

  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////
    set num_item [$tree item count]
    set cur_item 0
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
             [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }

  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////
    $tree selection clear
    $tree activate last
    $tree selection add last
  }

  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////
    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bobject {
# PURPOSE : Defines the snit widget used to
#           create new objects or editing existing ones
#/////////////////////////////////////////////////////////////////////////////
  typevariable TextDisabledBackground
  #<particular>
  option -namespace -default ::VariablesDGui  
  option -layer -default LayerVariablesDGui
  #</particular>
  # Identifier of the object
  option -item \
    -default "" -configuremethod "_conf-item"
  # Tol absolute identifier of the object
  option -objectid \
    -default "" -configuremethod "_conf-objectid"
  # Tol absolute identifier of the container
  option -containerid \
    -default "" -configuremethod "_conf-containerid"
  # Who did call me? (tree (MMS tree), list (Object List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"
  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"
  variable label_state
  variable widgets
  variable object_info
  #<particular>
  variable dependences
  variable attributes
  variable ListTypes
  #</particular>

  component dlg
  
  delegate method * to hull
  delegate option * to hull

  typeconstructor {
    set w [entry .___e___ ]
    set TextDisabledBackground [$w cget -disabledbackground]
    destroy $w
  }
  
  #///////////////////////////////////////////////////////////////////////////
  constructor {args} {
  #///////////////////////////////////////////////////////////////////////////
    # Dialog
    install dlg as frame $win.d 
    # Apply all options passed at creation time.
    $self configurelist $args
    # Paint the window
    $self _create
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew
    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-item {_ item} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-objectid {_ objectid} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-objectid) $objectid 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-containerid {_ containerid} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-containerid) $containerid
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent {_ parent} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state {_ state} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create {} {
  #
  # PURPOSE: Creates a form to edit a variable
  #///////////////////////////////////////////////////////////////////////////
    set f $dlg
    # Buttons: New, Edit, Copy
    #<particular> -helptext </particular>
    set fbu [frame $f.fbu]
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Object"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Object"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc Duplicate] \
      -helptext [mc "Duplicate Object"] -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]

    #<particular>
    # Coordinado con la funci�n GetInfo de     
    # del archivo Layer "layer_gui_<object>.tol"

    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Data Type"]:" -pady 5 -padx 5
    label $fe.lExpr -text "[mc "Expression"]:" -pady 5 -padx 5
    label $fe.lDepen -text "[mc "Dependences"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lAttr -text "[mc "Attributes"]:" -pady 5 -padx 5
    label $fe.lTags -text "[mc "Tags"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar object_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    set ListTypes [concat [list ""] [[$self cget -layer]::GetType.Possibilities]]
    ComboBox $fe.cbType -values $ListTypes \
      -textvariable [myvar object_info(type)] -editable false \
      -width 30 -state disabled
    set widgets(type) $fe.cbType

	CollapsableFrame $fe.cExpr \
	  -text "" -width 460 -height 205
	set cfExpr [$fe.cExpr getframe]
	set fExpr [frame $cfExpr.f]

    ::BayesText::CreateHLText $fExpr.t tol \
      -width 60 -height 10 -state disabled \
      -background $TextDisabledBackground -foreground black \
      -yscrollcommand "$fExpr.sy set" \
      -xscrollcommand "$fExpr.sx set" \
      -linemap 0 -wrap none
    scrollbar $fExpr.sy -orient vertical -command "$fExpr.t yview"  
    scrollbar $fExpr.sx -orient horizontal -command "$fExpr.t xview"  
    set widgets(expr) $fExpr.t

    grid $fExpr.t -row 0 -column 0 -sticky news 
    grid $fExpr.sy -row 0 -column 1 -sticky ns
    grid $fExpr.sx -row 1 -column 0 -sticky ew

	place $fExpr -x 5 -y 15

    Button $fe.bChk -image [::Bitmap::get syntaxcheck] -text [mc "Check"] \
      -helptext [mc "Syntax Check"] -padx 10 -relief link \
      -compound left -command [list $self CheckExpresion] \
      -state disabled
    set widgets(chk_expr) $fe.bChk

    CollapsableFrame $fe.cDepen \
	  -text "" -width 460 -height 140
    set widgets(depen) $fe.cDepen
    set ft [$widgets(depen) getframe]
    [$self cget -namespace]::bdependences $ft.fDepen \
        -state disabled
    set dependences $ft.fDepen
	place $dependences -x 5 -y 15
      
    bind $dependences <<OnCancel>> "$self Cancel"
    #bind $dependences <<OnDeleted>> "$self Update OnDeleted"
    #bind $dependences <<OnAdded>> "$self Update OnAdded"
    #bind $dependences <<OnChanged>> "$self Update OnChanged"

	CollapsableFrame $fe.cDesc \
	  -text "" -width 460 -height 205
    set widgets(cfdesc) $fe.cDesc
	set cfDesc [$fe.cDesc getframe]
	set fDesc [frame $cfDesc.f]

    ::BayesText::CreateHLText $fDesc.t tol \
      -width 60 -height 10 -state disabled \
      -background $TextDisabledBackground -foreground black \
      -yscrollcommand "$fDesc.sy set" \
      -xscrollcommand "$fDesc.sx set" \
      -linemap 0 -wrap none
    scrollbar $fDesc.sy -orient vertical -command "$fDesc.t yview"  
    scrollbar $fDesc.sx -orient horizontal -command "$fDesc.t xview"  
    set widgets(desc) $fDesc.t

    grid $fDesc.t -row 0 -column 0 -sticky news 
    grid $fDesc.sy -row 0 -column 1 -sticky ns
    grid $fDesc.sx -row 1 -column 0 -sticky ew

	place $fDesc -x 5 -y 15

	CollapsableFrame $fe.cAttr \
	  -text "" -width 460 -height 230
    set widgets(attr) $fe.cAttr
	set ft [$widgets(attr) getframe]
	::MMSAttributesGui::bmmsattributes $ft.fAttr \
	  -state disabled
    set attributes $ft.fAttr
	place $attributes -x 5 -y 15
      
    bind $attributes <<OnCancel>> "$self Cancel"
	
    entry $fe.eTags -textvariable [myvar object_info(st_tags)] \
      -width 60 -state readonly
    set widgets(tags) $fe.eTags
    
    grid $fe.lName       -row 0 -column 0 -sticky e
    grid $fe.eName       -row 0 -column 1 -sticky w
    grid $fe.lDesc       -row 1 -column 0 -sticky en
    grid $fe.cDesc       -row 1 -column 1 -sticky w
    grid $fe.lAttr       -row 2 -column 0 -sticky ne
    grid $fe.cAttr       -row 2 -column 1 -sticky w
    grid $fe.lTags       -row 3 -column 0 -sticky e
    grid $fe.eTags       -row 3 -column 1 -sticky w
    grid $fe.lType       -row 4 -column 0 -sticky e
    grid $fe.cbType      -row 4 -column 1 -sticky w
    grid $fe.lExpr       -row 5 -column 0 -sticky en
    grid $fe.cExpr       -row 5 -column 1 -sticky w
    grid $fe.bChk        -row 5 -column 2 -sticky w -padx 5
    grid $fe.lDepen      -row 6 -column 0 -sticky ne
    grid $fe.cDepen      -row 6 -column 1 -sticky w

    grid rowconfigure    $fe 7 -weight 1
    grid columnconfigure $fe 3 -weight 1
    #</particular>
    
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    #<particular>
    foreach {w} {name tags} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             chk_expr expr desc \
				 depen attr} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name tags \
                 accept cancel \
				 chk_expr \
				 depen attr} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
    #</particular>
  }
  
  #<particular>
  #///////////////////////////////////////////////////////////////////////////
  method GetAttributes {} {
  #///////////////////////////////////////////////////////////////////////////
    set object_info(attributes) [$attributes get_info ""]
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method SetAttributes {} {
  #///////////////////////////////////////////////////////////////////////////
    $attributes set_info $object_info(attributes)
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetDependences {} {
  #/////////////////////////////////////////////////////////////////////////////
    set object_info(dependences) [$dependences get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetDependences {} {
  #/////////////////////////////////////////////////////////////////////////////
    $dependences set_info $object_info(dependences)
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method CheckExpresion {} {
  #///////////////////////////////////////////////////////////////////////////
    set text $widgets(expr)
    [$self cget -layer]::MmsSyntaxCheck $text
  }
  #</particular>
  
  #///////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #///////////////////////////////////////////////////////////////////////////
    #<particular>
    set object_info(name) ""
    $widgets(desc) configure -state normal
    $widgets(desc) delete 1.0 end
    set object_info(st_tags) ""
	set object_info(type) ""
    set object_info(attributes) {}
    $self SetAttributes
    set object_info(dependences) {}
    $self SetDependences
    #</particular>
    $widgets(expr) configure -state normal
    $widgets(expr) delete 1.0 end
  }
     
  #///////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #///////////////////////////////////////////////////////////////////////////
    set objectid [$self cget -objectid]
    array set object_info [[$self cget -layer]::GetInfo $objectid]
    #<particular>
    #:tags
    set tags [list]
    foreach { {} t } $object_info(tags) {
      lappend tags $t
    }
    set object_info(st_tags) [join $tags ,]
    #:attributes
    set _attributes [list]
    foreach { {} r } $object_info(attributes) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _attributes $_row
    }
    set object_info(attributes) $_attributes
    $self SetAttributes
    #:dependences
    set _dependences [list]
    foreach { {} r } $object_info(dependences) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _dependences $_row
    }
    set object_info(dependences) $_dependences
    $self SetDependences
    #:expression
    $widgets(expr) configure -state normal
    $widgets(expr) delete 1.0 end
    $widgets(expr) insert 1.0 $object_info(expression)
    $widgets(expr) configure -state disabled
    #:description
    $widgets(desc) configure -state normal
    $widgets(desc) delete 1.0 end
    $widgets(desc) insert 1.0 $object_info(description)
    $widgets(desc) configure -state disabled
    #</particular>
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method Details {} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-state) "View"
    set label_state [mc "Object Details"]
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    #<particular>
    foreach {w} {desc expr type chk_expr} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name tags} {
      $widgets($w) configure -state readonly
    }
    $widgets(expr) configure -background $TextDisabledBackground
    $widgets(desc) configure -background $TextDisabledBackground
    $dependences configure -state disabled
    $attributes configure -state disabled
    #</particular>
    $self GetInfo
  }

  #///////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-state) "Edit"
    set label_state [mc "Edit Object"]
    ::MMSGui::ActivateEdition $win
    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    #<particular>
    foreach {w} {name desc expr type tags chk_expr} {
      $widgets($w) configure -state normal
    }
    $widgets(expr) configure -background white   
    $widgets(desc) configure -background white 
    $dependences configure -state normal  
    $attributes configure -state normal
    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
    #</particular>
  }

  #///////////////////////////////////////////////////////////////////////////
  method Create {} {
  #///////////////////////////////////////////////////////////////////////////
    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    #<particular>
    foreach {w} {name desc expr type tags chk_expr} {
      $widgets($w) configure -state normal
    }
    $widgets(expr) configure -background white   
    $widgets(desc) configure -background white   
    $dependences configure -state normal
    $attributes configure -state normal
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    #</particular>
  }

  #///////////////////////////////////////////////////////////////////////////
  method New {} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-state) "New"
    set label_state [mc "New Object"]
    ::MMSGui::ActivateEdition $win
    $self ClearInfo
    $self Create
  }

  #///////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #///////////////////////////////////////////////////////////////////////////
    set options(-state) "Copy"
    set label_state [mc "Duplicate Object"]
    ::MMSGui::ActivateEdition $win
    $self Create
  }

  #///////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #///////////////////////////////////////////////////////////////////////////
    ::MMSGui::DisactivateEdition
    #<particular>
    $attributes finishediting
    $self GetAttributes
    $dependences finishediting
    $self GetDependences
    set _tags [string map [list \" {} ] $object_info(st_tags)]
    set object_info(tags) [split $_tags ,]
    #(pgea) se recorta el �ltimo caracter: un retorno de carro inesperado
    #(pgea) quiz� habr�a que revisar los mecanismos del widget text
    #(pgea) http://www.tcl.tk/man/tcl8.4/TkCmd/text.htm
    set object_info(expression) [$widgets(expr) get 1.0 end-1c]
    set object_info(description) [$widgets(desc) get 1.0 end-1c]
    set object_info(containerid) [$self cget -containerid]
    #puts "Ok:options(-parent)=$options(-parent)"
    if {$options(-state) eq "Edit"} {
      set object_info(identifier) [$self cget -item]
      set new_absid [[$self cget -layer]::Edit object_info]
      set new_ident $object_info(name)
      if {$object_info(identifier) ne $new_ident} {
        if {$options(-parent) eq "tree"} {
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
        } else {
          [$self cget -namespace]::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
        }
      } elseif {$options(-parent) eq "tree"} {
        [$self cget -namespace]::_ShowInfo $object_info(absoluteIdentifier) 
      }
    } else {                                             ;# New, Copy
      [$self cget -layer]::Create object_info
      if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
      } else {
        event generate $self <<Refresh>>
      }
    }
    #</particular>
    if {$options(-parent) eq "tree"} {
      $self Details
    } else {
      event generate $self <<Ok>>
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////
	::MMSGui::DisactivateEdition
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bdependences {
# PURPOSE : Defines the snit widget used to
#           create a list of dependences
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdv_dependences
  # Dependences to be edited at the tablelist
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
    set lrow [$widgets(tbl) size]
    for {set _r 0} {$_r < $lrow} {incr _r} {
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -state $s
      set btn_plus [$widgets(tbl) windowpath $_r,col_bplus]
      if {$btn_plus ne ""} {
	    $btn_plus configure -state $s
	  }
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdv_dependences [list]

    set tablelist_options(-label) [mc "Dependences"]
    set tablelist_options(-columns) [list \
      0 [mc "Name"] left \
      0 "" center \
      0 "" center \
    ]
    set tablelist_options(-listvar) [myvar mdv_dependences]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit a list of dependences
  #///////////////////////////////////////////////////////////////////////////
   
    set ft $dlg
    # Tablelist frame

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 4 -width 60
    set widgets(tbl) $ft.tbl

    bind $widgets(tbl) <<TablelistCellUpdated>> \
        "event generate $win <<OnChanged>>"
    
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"
    
    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew
    
    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
    grid $ft -sticky news
    
    $widgets(tbl) columnconfigure 0 -name col_name -editable yes
    $widgets(tbl) columnconfigure 1 -name col_bminus -hide true \
        -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 2 -name col_bplus \
        -labelimage [::Bitmap::get plus] -labelcommand "$self AddRow"
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ConfigureColumns {nrows} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$nrows>0} {
      $widgets(tbl) columnconfigure col_bminus -hide false
      $widgets(tbl) columnconfigure col_bplus -labelimage "" \
          -labelcommand "$self DoNothing"
      $widgets(tbl) cellconfigure end,col_bplus -window [mymethod CreateButtonPlus]
      if {$nrows>=2} {
        $widgets(tbl) cellconfigure [expr $nrows-2],col_bplus -window ""
      }
      
    } else {
      $widgets(tbl) columnconfigure col_bminus -hide true
      $widgets(tbl) columnconfigure col_bplus -labelimage [::Bitmap::get plus] \
          -labelcommand "$self AddRow"
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method DeleteRow {row} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "DeleteRow:row=$row"
    $widgets(tbl) delete [list $row]
    
    set lrow [$widgets(tbl) size]
    for {set _r $row} {$_r < $lrow} {incr _r} {
      #puts "DeleteRow:_r=$_r,windowpath=[$widgets(tbl) windowpath $_r,1]"
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -command [list $self DeleteRow $_r]
    }
    $self ConfigureColumns $lrow
    
    event generate $win <<OnDeleted>>
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method AddRow {args} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "AddRow:enter"
    $widgets(tbl) finishediting
    update
    $widgets(tbl) insert end [list "" "" ""]
    
    set lrow [expr [$widgets(tbl) size]-1]
    $widgets(tbl) cellconfigure $lrow,col_bplus \
        -window [mymethod CreateButtonPlus]
    $widgets(tbl) cellconfigure $lrow,col_bminus \
        -window [mymethod CreateButtonMinus]
    $widgets(tbl) editcell $lrow,0

    $self ConfigureColumns [$widgets(tbl) size]
    
    event generate $win <<OnAdded>>
    #puts "AddRow:exit"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonMinus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get minus] -highlightthickness 0 -takefocus 0 \
        -command [list $self DeleteRow $row]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonPlus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get plus] -highlightthickness 0 -takefocus 0 \
        -command [list $self AddRow]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
    set _state $options(-state)
    $self put_state "normal"

    $widgets(tbl) delete 0 end
    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set row 0
    foreach {r} $iteminfo {
      $widgets(tbl) insert end [lrange $r 0 $idx_fixed]
      $widgets(tbl) cellconfigure $row,col_bminus \
          -window [mymethod CreateButtonMinus]
      incr row
    }
    $self ConfigureColumns $row
	
    $self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set _info [list]
    foreach {r} $mdv_dependences {
      lappend _info [lrange $r 0 [expr $idx_fixed-1]]
    }
    #puts "get_info:_info=$_info"
    return $_info
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }
  
}

}
