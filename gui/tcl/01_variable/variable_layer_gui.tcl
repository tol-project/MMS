#/////////////////////////////////////////////////////////////////////////////
# FILE:    variable_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDV (module variable definition)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerVariablesGui {

#///////////////////////////////////////////////////////////////////////////
proc GetInfo {objectid} {
#///////////////////////////////////////////////////////////////////////////
  set objectid_text \"[LayerMMSGui::TolText $objectid]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::VariablesGui::GetInfo" \
    "Set" $objectid_text
}

#/////////////////////////////////////////////////////////////////////////////
proc GetList {containerid details} {
#/////////////////////////////////////////////////////////////////////////////
  set containerid_text \"$containerid\"
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::VariablesGui::GetList" \
    "Set" $containerid_text $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc Create {variable_info tags dependences attributes containerid} {
#///////////////////////////////////////////////////////////////////////////
  upvar $variable_info arrayarg
  set tags_set [TclLst2TolSet $tags]
  #puts "CreateVariable:tags_set=$tags_set"
  set dependences_set [TclLst2TolSet $dependences -level 2]
  #puts "CreateVariable:dependences_set=$dependences_set"
  set attributes_set [TclLst2TolSet $attributes -level 2]
  set containerid_text \"$containerid\"
  LayerMMSGui::EvalTolFunArr "MMS::Layer::VariablesGui::Create" \
	"Text" arrayarg $tags_set $dependences_set $attributes_set $containerid_text
}

#///////////////////////////////////////////////////////////////////////////
proc Edit {variable_info tags dependences attributes containerid} {
#///////////////////////////////////////////////////////////////////////////
  upvar $variable_info arrayarg
  set tags_set [TclLst2TolSet $tags]
  set dependences_set [TclLst2TolSet $dependences -level 2]
  #puts "EditVariable:dependences_set=$dependences_set"
  set attributes_set [TclLst2TolSet $attributes -level 2]
  set containerid_text \"$containerid\"
  LayerMMSGui::EvalTolFunArr "MMS::Layer::VariablesGui::Edit" \
	"Text" arrayarg $tags_set $dependences_set $attributes_set $containerid_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetType.Possibilities {} {
#///////////////////////////////////////////////////////////////////////////
  set ltypes [LayerMMSGui::EvalTolFunVoid \
    "MMS::Layer::VariablesGui::GetType.Possibilities" "Set"]
  set _types [list]
  foreach { {} mt } $ltypes {
    lappend _types $mt
  }
  return $_types
}

#/////////////////////////////////////////////////////////////////////////////
proc MmsSyntaxCheck {editor} {
#/////////////////////////////////////////////////////////////////////////////
  # Basado en ::BayesText::TolSyntaxCheck (lib/toltk/bystext.tcl)
  # global toltk_script_path
  # guardamos el archivo en un temporal
  set path [file join $::env(HOME) __check__.tol]
  set texto [::BayesText::GetSelectionOrAll $editor]
  # modificamos la expresi�n de acuerdo al uso de patrones
  set textM [string map {� %} [string map {% _ � _} \
    [string map {%% �} $texto]]]
  set ok [::BayesText::SaveTxt $textM $path]
  if {$ok} {
    # evaluamos el archivo
    set check [::BayesText::TolFileSyntaxCheck $path]
    if {[lindex $check 0]} {
      tk_messageBox -type ok -icon info -title [mc "Syntax Check"] \
        -message [mc "Syntax check done successfully"]
    } else  {
      # hay errores
	  #puts "TolSyntaxCheck (hay errores)"
      ::BayesText::ShowSyntaxErrors $editor [lindex $check 1]
    }
    # borrar el archivo
    catch "file delete $path" error
  } else  {
    tk_messageBox -type ok -icon warning -title [mc Editor] \
                -message [mc "Syntax check cannot be done"]
  }
}

}
