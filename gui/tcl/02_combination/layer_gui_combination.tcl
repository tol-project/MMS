#/////////////////////////////////////////////////////////////////////////////
# FILE:    layer_gui_combination.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Combinations
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerCombinationsGui {
 
#/////////////////////////////////////////////////////////////////////////////
proc GetCombinationTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Combination 1"
    vers "1.0"
    desc "desc of Combination 1"
    dCre "2010/08/25"
  }
}
  
variable get_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetCombination {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_combination_test
  
  if { $get_combination_test } {
    return [GetCombinationTest]
  }
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::CombinationsGui::GetCombination" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetCombinationsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Combination 1"
      vers "1.0"
    }
    {}
    {
      ident "Combination 2"
      vers "2.0"
    }
  }
}
  
variable get_combinations_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetCombinationsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_combinations_test
  
  if { $get_combinations_test } {
    return [GetCombinationsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::CombinationsGui::GetCombinationsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetCombinationHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::CombinationsGui::GetCombinationHelp" \
    "Text" "?"
}

variable create_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateCombination {combination_info dsets attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_combination_test
  upvar $combination_info arrayarg
  
  set dsets_set [TclLst2TolSet $dsets]

  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_combination_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::CreateCombinationTest" \
	  "Real" arrayarg $dsets_set $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::CreateCombination" \
	  "Real" arrayarg $dsets_set $attributes_set
  }
}

variable copy_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyCombination {obj_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_combination_test
  upvar $obj_info arrayarg
  
  if { $copy_combination_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::CopyCombinationTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::CopyCombination" \
	  "Real" arrayarg
  }
}

variable edit_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditCombination {combination_info dsets attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_combination_test
  upvar $combination_info arrayarg
  
  set dsets_set [TclLst2TolSet $dsets]

  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $edit_combination_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::EditCombinationTest" \
	  "Real" arrayarg $dsets_set $attributes_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::CombinationsGui::EditCombination" \
	  "Real" arrayarg $dsets_set $attributes_set
  }
}

}
