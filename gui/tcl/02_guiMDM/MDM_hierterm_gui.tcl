#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_hierterm_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the terms of a hierarchy (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit
package require Tablelist
package require autoscroll 1.1

namespace eval ::MDMHierTermsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateHierTermsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateHierTermsListDetails"
  set l_details_frame [frame $f.details_HierTerms]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "HierarchyTerm" \
    -swlist "::MDMHierTermsGui::bmmshiertlist" \
	-fshowitem "MDMHierTermsGui::_ShowHierTermDetails" \
	-fshowlist "::MDMHierTermsGui::_ShowHierTermsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierTermsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateHierTermsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowHierTermsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set hierarchy [$tree item parent $id]
  set hierarchy_name [$tree item text $hierarchy first]
  set grandparent [$tree item parent $hierarchy]
  set model [$tree item parent $grandparent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  lappend container $hierarchy_name
  
  _ShowHierTermHelp  

  _ShowHierTermsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuHierTerms {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Hierarchy Term"] \
    -command "::MDMHierTermsGui::NewHierTerm" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewHierTerm {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierTermHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetHierTermHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateHierTermDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateHierTermDetails"
  set details_frame [frame $f.details_HierTerm]
  set view_frame [frame $f.view_HierTerm]

  set _details [bmmshierarchyterm $details_frame.details]
  set _view [bmmshierarchyterm $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierTermDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateHierTermDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowHierTermDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set hierarchy [$tree item parent $parent]
  set hierarchy_name [$tree item text $hierarchy first]
  set grandparent [$tree item parent $hierarchy]
  set model [$tree item parent $grandparent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  lappend container $hierarchy_name

  set ident [$tree item text $id first]

  _ShowHierTermDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandHierTermsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set hierarchy [$tree item parent $id]
  set hierarchy_name [$tree item text $hierarchy first]
  set grandparent [$tree item parent $hierarchy]
  set model [$tree item parent $grandparent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  lappend container $hierarchy_name
  set _details "no" 
  
  set _list [LayerMDMGui::GetHierTermsList $container $_details]
  #puts "ExpandHierTerms:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "HierarchyTerm"] \
	          [list "MDMHierTermsGui::ExpandHierTerm"] \
			  [list "MDMHierTermsGui::ShowHierTermDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandHierTerm {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set row [list [list [::Bitmap::get "Set"] [mc "Parameters"]] \
			    [list "Parameters"] \
	            [list "MDMParametersGui::ExpandParametersList"] \
			    [list "MDMParametersGui::ShowParametersListDetails"] \
			    [list ""] \
			    [list ""] \
          ]
  $tree insert $row \
    -at child -relative $id
}

 
#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshiertlist {
# PURPOSE : Defines the snit widget used to
#           list the terms of a hierarchy
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_hierterm
    # mdm_hierterm(list)  - Hierarchy Terms list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Terms of a Hierarchy
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Hierarchy Terms List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_hierterm(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_hierterm(list) [LayerMDMGui::GetHierTermsList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_hierterm(list)=$mdm_hierterm(list)"

    $tree item delete all

    foreach {{} it} $mdm_hierterm(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshcoefmatrix {
# PURPOSE : Defines the snit widget used to
#           create a hierarchy term matrix
#           in order to edit it's coefficients
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdm_matrix
  # Matrix to be edited at the tablelist
   # Hierarchy term matrix
    # Rows
     # one for each Element
    # Columns
     # one for Elements names (Child Parameters)
     # one column of Coefficients for this Term (Hyperparameter)
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
    $widgets(tbl) configure -state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_matrix [list]

    set tablelist_options(-label) [mc "Hierarchy"]
    set tablelist_options(-columns) [list \
      0 [mc "Child Parameters"] left \
      0 [mc "Coefficients"] right \
    ]
    set tablelist_options(-listvar) [myvar mdm_matrix]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit the coefficients of a hierarchy term
  #///////////////////////////////////////////////////////////////////////////
    set fs $dlg
      
    # Tablelist frame
    set ft [frame $fs.ft]

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 10 -width 93
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    grid columnconfigure $ft 0 -weight 1
    grid rowconfigure $ft 0 -weight 1

    $widgets(tbl) columnconfigure 0 -name col_name -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 1 -name col_coeffs -editable yes -labelcommand "$self DoNothing" 
    
    grid $fs.ft -sticky news

    grid rowconfigure $fs 1 -weight 1
    grid columnconfigure $fs 0 -weight 1
    grid $fs -sticky news
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
    $widgets(tbl) configure -state normal

    $widgets(tbl) delete 0 end

    foreach {r} $iteminfo {
      $widgets(tbl) insert end $r
    }
    
	$widgets(tbl) configure -state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$what ne "resume"} {
      return $mdm_matrix
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }

}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshierarchyterm {
# PURPOSE : Defines the snit widget used to
#           create new hierarchy terms or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  # The container to wich the hierarchy term belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Name of the specific hierarchy term to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable hierterm_info
  # Hierarchy Term data to be edited
   # (Class @HierarchyTerm)

    # hierterm_info(name)            - Hierarchy Term name (Text _.name)
    # hierterm_info(g_active)        - Global active (IsActive method)
    # hierterm_info(l_active)        - Local active (Real _.isActive)
    #                                - (Set _.parameterHyper) // @ParameterHyper
	 # (Class @parameterHyper)
      # hierterm_info(hyper_init)    - (Real _.initialValue)
      # hierterm_info(hyper_fix)     - (Real _.isFixed)
    #                                - (Set _.coefficients)
     # hierterm_info(coefs)          - Coefficients (List of reals)
      # hierterm_info(elems)         - List of the hierarchy elements names (Child Parameters)
      # hierterm_info(matrix)        - Hierarchy matrix of Elements and Coefficients
      #                                One element for each row of the Hierarchy matrix
    #                                - (Set _parent_; //@Hierarchy)
   
  variable matrix

  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Hierarchy Term
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Hierarchy"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Hierarchy Term"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Hierarchy Term"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lMatrix -text "[mc "Coefficients"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar hierterm_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    set factive [frame $fe.fActive]

    checkbutton $factive.chkbLActive -variable [myvar hierterm_info(l_active)] \
      -text [mc "Local"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(l_active) $factive.chkbLActive

    checkbutton $factive.chkbGActive -variable [myvar hierterm_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $factive.chkbGActive

    grid $factive.chkbLActive $factive.chkbGActive -sticky w -padx 2 -pady 2
    grid rowconfigure    $factive 0 -weight 1
    grid columnconfigure $factive 2 -weight 1

    label $fe.lHyper -text [mc "Hyperparameter"] -pady 5 -padx 5
    set fhyper [labelframe $fe.fHyper \
      -labelwidget $fe.lHyper -relief solid -bd 1]
	  
    label $fhyper.lInit -text "[mc "Initial Value"]:" -pady 5 -padx 5
	
    entry $fhyper.eInit -textvariable [myvar hierterm_info(hyper_init)] \
      -width 10 -state readonly
    set widgets(hyper_init) $fhyper.eInit
	
    checkbutton $fhyper.chkbFixed -variable [myvar hierterm_info(hyper_fix)] \
      -text [mc "Fixed"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(hyper_fix) $fhyper.chkbFixed
	
    grid $fhyper.lInit     -row 0 -column 0 -sticky e
    grid $fhyper.eInit     -row 0 -column 1 -sticky w
    grid $fhyper.chkbFixed -row 1 -column 1 -sticky w
	
    grid rowconfigure    $fhyper 2 -weight 1
    grid columnconfigure $fhyper 2 -weight 1
    
	CollapsableFrame $fe.cMatrix \
	  -text "" -width 700 -height 240
    set widgets(matrix) $fe.cMatrix
	set ft [$widgets(matrix) getframe]
	::MDMHierTermsGui::bmmshcoefmatrix $ft.fMatrix \
	  -state disabled
    set matrix $ft.fMatrix
	place $matrix -x 5 -y 15
      
    bind $matrix <<OnCancel>> "$self Cancel"
    bind $matrix <<OnChanged>> "$self UpdateMatrix"
    
    grid $fe.lName   -row 0 -column 0 -sticky e
    grid $fe.eName   -row 0 -column 1 -sticky w
    grid $fe.lActive -row 1 -column 0 -sticky e
    grid $fe.fActive -row 1 -column 1 -sticky w
    grid $fe.fHyper  -row 2 -column 0 -columnspan 2 -sticky news -pady 10 -padx 10
    grid $fe.lMatrix -row 3 -column 0 -sticky ne
    grid $fe.cMatrix -row 3 -column 1 -sticky w

    grid rowconfigure    $fe 4 -weight 1
    grid columnconfigure $fe 2 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name hyper_init \
	             hyper_fix l_active} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
                 matrix} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name hyper_init \
	             hyper_fix l_active \
                 matrix \
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetMatrix
    
    set nelem [llength $hierterm_info(elems)]
    set hierterm_info(coefs) [list]
    for {set _r 0} {$_r < $nelem} {incr _r} {
      lappend hierterm_info(coefs) [lindex [lindex $hierterm_info(matrix) $_r] 1]
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method BuildMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set _matrix [list]
    set nelem [llength $hierterm_info(elems)]
    for {set _r 0} {$_r < $nelem} {incr _r} {
      set _row [list [lindex $hierterm_info(elems) $_r] \
	                 [lindex $hierterm_info(coefs) $_r]]
      lappend _matrix $_row
    }
    set hierterm_info(matrix) $_matrix
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////

    set hierterm_info(matrix) [$matrix get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////

    $matrix set_info $hierterm_info(matrix)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
	
    set hierterm_info(name) ""
    set hierterm_info(l_active) 1
    set hierterm_info(g_active) 0
    set hierterm_info(hyper_init) "?"
    set hierterm_info(hyper_fix) 0
    
    set hierterm_info(elems) [LayerMDMGui::GetHierarchyElements \
	  [$self cget -item] $options(-container)]
    set _elems [list]
    set _coefs [list]
    foreach { {} r } $hierterm_info(elems) {
      lappend _elems $r
      lappend _coefs 1
    }
    set hierterm_info(elems) $_elems
    #puts "GetInfo:hierterm_info(elems)=$hierterm_info(elems)"
    set hierterm_info(coefs) $_coefs
    #puts "GetInfo:hierterm_info(coefs)=$hierterm_info(coefs)"
	
    $self BuildMatrix
    $self SetMatrix
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

	array set hierterm_info [LayerMDMGui::GetHierarchyTerm $ident \
      $options(-container)]
    #puts "GetInfo:hierterm_info=[array get hierterm_info]"
      
    set _elems [list]
    foreach { {} r } $hierterm_info(elems) {
      lappend _elems $r
    }
    set hierterm_info(elems) $_elems
    #puts "GetInfo:hierterm_info(elems)=$hierterm_info(elems)"

    set _coefs [list]
    foreach { {} r } $hierterm_info(coefs) {
      lappend _coefs $r
    }
    set hierterm_info(coefs) $_coefs
    #puts "GetInfo:hierterm_info(coefs)=$hierterm_info(coefs)"
      
    $self BuildMatrix
    #puts "GetInfo:hierterm_info(matrix)=$hierterm_info(matrix)"

    $self SetMatrix
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////
	
    set options(-state) "View"
    set label_state [mc "Details of the Hierarchy Term"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name hyper_init} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {hyper_fix l_active} {
      $widgets($w) configure -state disabled
    }
    $matrix configure -state disabled
    
    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Hierarchy Term"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name hyper_init \
	             hyper_fix l_active} {
      $widgets($w) configure -state normal
    }
    $matrix configure -state normal

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name hyper_init \
	             hyper_fix l_active} {
      $widgets($w) configure -state normal
    }
    $matrix configure -state normal
      
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Hierarchy Term"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Hierarchy Term"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    #$self UpdateMatrix
    $matrix finishediting
	
	set hierterm_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
	  set hierterm_info(ident) [$self cget -item]
	  
      LayerMDMGui::EditHierarchyTerm hierterm_info \
		$hierterm_info(coefs) $hierterm_info(container)

	  set new_ident $hierterm_info(name)
	  if {$hierterm_info(ident) ne $new_ident} {
	    if {$options(-parent) eq "tree"} {
          array set new_info [LayerMDMGui::GetHierarchyTerm \
            $new_ident [$self cget -container]]
	      set new_absid $new_info(abs_id)
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
	    } else {
          ::MDMHierTermsGui::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
	    }
	  }
      
    } else {                                 ;# New, Copy
      LayerMDMGui::CreateHierarchyTerm hierterm_info \
        $hierterm_info(coefs) $hierterm_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }
  
}

}
