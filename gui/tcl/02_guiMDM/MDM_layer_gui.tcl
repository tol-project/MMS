#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (module model definition)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetModelTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Model 1"
    vers "1.0"
    desc "desc of Model 1"
    dCre "2009/11/15"
  }
}
  
variable get_model_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetModel {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_model_test
  
  if { $get_model_test } {
    return [GetModelTest]
  }
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetModel" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetModelsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Model 1"
      vers "1.0"
    }
    {}
    {
      name "Model 2"
      vers "2.0"
    }
  }
}

variable get_models_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetModelsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_models_test
  
  if { $get_models_test } {
    return [GetModelsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::MDMGui::GetModelsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetModelInfo {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetModelInfo" \
    "Text" $ident_text $container_set
}

#///////////////////////////////////////////////////////////////////////////
proc GetModelHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetModelHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetModelDataSet {container} {
#///////////////////////////////////////////////////////////////////////////

  set container_set [TclLst2TolSet $container]
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetModelDataSet" \
    "Set" $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetModelSummaryTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ObjectType "Output"
      ObjectTotal "4"
      ObjectActive "3"
    }
    {}
    {
      ObjectType "BaseExpTerm"
      ObjectTotal "12"
      ObjectActive "7"
    }
  }
}

variable summary_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetModelSummary {ident container} {
#/////////////////////////////////////////////////////////////////////////////
  variable summary_test

  if { $summary_test } {
    return [GetModelSummaryTest]
  }
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetModelSummary" \
    "Set" $ident_text $container_set
}

variable create_model_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateModel {model_info dsets attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_model_test
  upvar $model_info arrayarg
  
  set dsets_set [TclLst2TolSet $dsets]

  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_model_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateModelTest" \
	  "Real" arrayarg $dsets_set $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateModel" \
	  "Real" arrayarg $dsets_set $attributes_set
  }
}

variable copy_model_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyModel {model_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_model_test
  upvar $model_info arrayarg
  
  if { $copy_model_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CopyModelTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CopyModel" \
	  "Real" arrayarg
  }
}

variable edit_model_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditModel {model_info dsets attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_model_test
  upvar $model_info arrayarg
  
  set dsets_set [TclLst2TolSet $dsets]

  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $edit_model_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditModelTest" \
	  "Real" arrayarg $dsets_set $attributes_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditModel" \
	  "Real" arrayarg $dsets_set $attributes_set
  }
}

}
