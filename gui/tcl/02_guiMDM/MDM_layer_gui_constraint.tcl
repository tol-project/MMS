#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_constraint.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (constraints)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetConstraintTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Constraint 1"
    elem "Parameter"
    e_name "Element of Constraint 1"
    inf "?"
    sup "0"
    active "1"
  }
}
  
variable get_constraint_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetConstraint {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_constraint_test
  
  if { $get_constraint_test } {
    return [GetConstraintTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetConstraint" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetConstraintsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Constraint 1"
    }
    {}
    {
      ident "Constraint 2"
    }
  }
}

variable constraints_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetConstraintsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable constraints_list_test
  
  if { $constraints_list_test } {
    return [GetConstraintsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetConstraintsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetConstraintHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetConstraintHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetConstraintTerms {comb_name container} {
#///////////////////////////////////////////////////////////////////////////

  set container_set [TclLst2TolSet $container]
  set comb_name_text \"[LayerMMSGui::TolText $comb_name]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetConstraintTerms" \
    "Set" $comb_name_text $container_set
}

variable create_constraint_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateConstraint {constraint_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_constraint_test
  upvar $constraint_info constraint_array

  set container_set [TclLst2TolSet $container]

  if { $create_constraint_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateConstraintTest" \
      "Real" constraint_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateConstraint" \
      "Real" constraint_array $container_set
  }
}

variable edit_constraint_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditConstraint {constraint_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_constraint_test
  upvar $constraint_info constraint_array
  
  set container_set [TclLst2TolSet $container]

  if { $edit_constraint_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditConstraintTest" \
      "Real" constraint_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditConstraint" \
      "Real" constraint_array $container_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc RemoveConstraint {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::RemoveConstraint" \
    "Real" $ident_text $container_set
}

}
