#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_expterm.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (Explanatory terms)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetExpTermTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "ExpTerm 1"
    type "Linear"
    desc "desc of ExpTerm 1"
    active "1"
    inp ""
  }
}
  
variable get_expterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetExpTerm {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_expterm_test
  
  if { $get_expterm_test } {
    return [GetExpTermTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetExpTerm" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetExpTermsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "ExpTerm 1"
      type "Linear"
    }
    {}
    {
      name "ExpTerm 2"
      type "MultiLinear"
    }
  }
}

variable expterms_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetExpTermsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable expterms_list_test
  
  if { $expterms_list_test } {
    return [GetExpTermsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetExpTermsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetExpTermHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetExpTermHelp" \
    "Text" "?"
}

variable create_expterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateExpTerm {expterm_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_expterm_test
  upvar $expterm_info expterm_array

  set container_set [TclLst2TolSet $container]

  if { $create_expterm_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateExpTermTest" \
      "Real" expterm_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateExpTerm" \
      "Real" expterm_array $container_set
  }
}

variable edit_expterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditExpTerm {expterm_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_expterm_test
  upvar $expterm_info expterm_array
  
  set container_set [TclLst2TolSet $container]

  if { $edit_expterm_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditExpTermTest" \
      "Real" expterm_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditExpTerm" \
      "Real" expterm_array $container_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc RemoveExpTerm {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::RemoveExpTerm" \
    "Real" $ident_text $container_set
}

}
