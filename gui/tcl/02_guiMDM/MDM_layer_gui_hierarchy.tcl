#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_hierarchy.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (hierarchies)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchyTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Hierarchy 1"
    desc "Description of Hierarchy 1"
    active "1"
    terms {}
    elems {}
	sigmas {}
	coefs {}
  }
}
  
variable get_hierarchy_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetHierarchy {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_hierarchy_test
  
  if { $get_hierarchy_test } {
    return [GetHierarchyTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierarchy" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchiesListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Hierarchy 1"
    }
    {}
    {
      ident "Hierarchy 2"
    }
  }
}

variable hierarchies_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchiesList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable hierarchies_list_test
  
  if { $hierarchies_list_test } {
    return [GetHierarchiesListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierarchiesList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetHierarchyHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierarchyHelp" \
    "Text" "?"
}

variable create_hierarchy_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateHierarchy {hierarchy_info elems sigmas container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_hierarchy_test
  upvar $hierarchy_info hierarchy_array

  set elems_set [TclLst2TolSet $elems -level 2]
  #puts "CreateHierarchy:elems_set=$elems_set"
  
  #puts "CreateHierarchy:sigmas=$sigmas"
  set sigmas_set [TclLst2TolSet $sigmas]
  #puts "CreateHierarchy:sigmas_set=$sigmas_set"
  
  set container_set [TclLst2TolSet $container]

  if { $create_hierarchy_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateHierarchyTest" \
      "Real" hierarchy_array $elems_set $sigmas_set $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateHierarchy" \
      "Real" hierarchy_array $elems_set $sigmas_set $container_set
  }
}

variable edit_hierarchy_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditHierarchy {hierarchy_info elems sigmas container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_hierarchy_test
  upvar $hierarchy_info hierarchy_array
  
  set elems_set [TclLst2TolSet $elems -level 2]
  #puts "CreateHierarchy:elems_set=$elems_set"
  
  set sigmas_set [TclLst2TolSet $sigmas]
  #puts "CreateHierarchy:sigmas_set=$sigmas_set"
  
  set container_set [TclLst2TolSet $container]

  if { $edit_hierarchy_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditHierarchyTest" \
      "Real" hierarchy_array $elems_set $sigmas_set $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditHierarchy" \
      "Real" hierarchy_array $elems_set $sigmas_set $container_set
  }
}

}
