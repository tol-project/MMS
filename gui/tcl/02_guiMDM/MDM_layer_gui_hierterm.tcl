#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_hierterm.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (hierarchy terms)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchyTermTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Hierarchy Term 1"
    active "1"
	coefs {}
  }
}
  
variable get_hierterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetHierarchyTerm {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_hierterm_test
  
  if { $get_hierterm_test } {
    return [GetHierarchyTermTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierarchyTerm" \
    "Set" $ident_text $container_set
}

#///////////////////////////////////////////////////////////////////////////
proc GetHierarchyElements {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierarchyElements" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetHierTermsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Hierarchy Term 1"
    }
    {}
    {
      ident "Hierarchy Term 2"
    }
  }
}

variable hierterms_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetHierTermsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable hierterms_list_test
  
  if { $hierterms_list_test } {
    return [GetHierTermsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierTermsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetHierTermHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetHierTermHelp" \
    "Text" "?"
}

variable create_hierarchyterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateHierarchyTerm {hierarchyterm_info coefs container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_hierarchyterm_test
  upvar $hierarchyterm_info hierarchyterm_array

  set coefs_set [TclLst2TolSet $coefs]
  #puts "CreateHierarchy:coefs_set=$coefs_set"
  
  set container_set [TclLst2TolSet $container]

  if { $create_hierarchyterm_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateHierarchyTermTest" \
      "Real" hierarchyterm_array $coefs_set $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateHierarchyTerm" \
      "Real" hierarchyterm_array $coefs_set $container_set
  }
}

variable edit_hierarchyterm_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditHierarchyTerm {hierarchyterm_info coefs container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_hierarchyterm_test
  upvar $hierarchyterm_info hierarchyterm_array
  
  set coefs_set [TclLst2TolSet $coefs]
  #puts "CreateHierarchy:coefs_set=$coefs_set"
  
  set container_set [TclLst2TolSet $container]

  if { $edit_hierarchyterm_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditHierarchyTermTest" \
      "Real" hierarchyterm_array $coefs_set $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditHierarchyTerm" \
      "Real" hierarchyterm_array $coefs_set $container_set
  }
}

}
