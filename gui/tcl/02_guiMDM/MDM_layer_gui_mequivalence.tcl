#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_mequivalence.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (Mequivalences)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetMEquivalenceTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "MEquivalence 1"
    desc "description of MEquivalence 1"
    active "1"
    terms {}
  }
}
  
variable get_equivalence_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetMEquivalence {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_equivalence_test
  
  if { $get_equivalence_test } {
    return [GetMEquivalenceTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMEquivalence" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetMEquivalencesListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "MEquivalence 1"
    }
    {}
    {
      ident "MEquivalence 2"
    }
  }
}

variable mequivalences_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetMEquivalencesList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable mequivalences_list_test
  
  if { $mequivalences_list_test } {
    return [GetMEquivalencesListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMEquivalencesList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetMEquivalenceHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMEquivalenceHelp" \
    "Text" "?"
}

variable create_equivalence_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateMEquivalence {equivalence_info terms container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable create_equivalence_test
  upvar $equivalence_info equivalence_array

  set terms_set [TclLst2TolSet $terms -level 2]
  #puts "CreateEquivalence:terms_set=$terms_set"
  
  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $create_equivalence_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMEquivalenceTest" \
      "Real" equivalence_array $terms_set $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMEquivalence" \
      "Real" equivalence_array $terms_set $container_set $prior_set $constraint_set
  }
}

variable edit_equivalence_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditMEquivalence {equivalence_info container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_equivalence_test
  upvar $equivalence_info equivalence_array
  
  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $edit_equivalence_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMEquivalenceTest" \
      "Real" equivalence_array $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMEquivalence" \
      "Real" equivalence_array $container_set $prior_set $constraint_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc RemoveMEquivalence {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::RemoveMEquivalence" \
    "Real" $ident_text $container_set
}

}
