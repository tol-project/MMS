#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_mvariable.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (model variables)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetMVariableTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "MVariable 1"
	var ""
	transf {}
  }
}
  
variable get_mvariable_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetMVariable {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_mvariable_test
  
  if { $get_mvariable_test } {
    return [GetMVariableTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMVariable" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetMVariablesListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "MVariable 1"
    }
    {}
    {
      ident  "MVariable 2"
    }
  }
}

variable mvariables_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetMVariablesList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable mvariables_list_test
  
  if { $mvariables_list_test } {
    return [GetMVariablesListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMVariablesList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetMVariableHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMVariableHelp" \
    "Text" "?"
}

variable create_mvariable_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateMVariable {mvariable_info transf_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_mvariable_test
  upvar $mvariable_info mvariable_array
  array set transf_array $transf_info 

  set container_set [TclLst2TolSet $container]

  if { $create_mvariable_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMVariableTest" \
      "Real" mvariable_array [::LayerMMSGui::TclArr2TolSetNamed transf_array] \
	   $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMVariable" \
      "Real" mvariable_array [::LayerMMSGui::TclArr2TolSetNamed transf_array] \
	   $container_set
  }
}

variable edit_mvariable_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditMVariable {mvariable_info transf_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_mvariable_test
  upvar $mvariable_info mvariable_array
  array set transf_array $transf_info 
  
  set container_set [TclLst2TolSet $container]

  if { $edit_mvariable_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMVariableTest" \
      "Real" mvariable_array [::LayerMMSGui::TclArr2TolSetNamed transf_array] \
	   $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMVariable" \
      "Real" mvariable_array [::LayerMMSGui::TclArr2TolSetNamed transf_array] \
	   $container_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc GetMVariableGrammar {ident container} {
#///////////////////////////////////////////////////////////////////////////

  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMVariableGrammar" \
    "Text" $ident_text $container_set
}

#///////////////////////////////////////////////////////////////////////////
proc PasteMVariables {model} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFun "MMS::Layer::MDMGui::PasteMVariables" \
    "Real" $model
}

}
