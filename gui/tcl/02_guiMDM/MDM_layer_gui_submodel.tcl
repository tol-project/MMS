#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_submodel.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (submodels)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetSubmodelTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Submodel 1"
    desc "desc of submodel 1"
	type "Linear"
  }
}
  
variable get_submodel_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetSubmodel {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_submodel_test
  
  if { $get_submodel_test } {
    return [GetSubmodelTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetSubmodel" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetSubmodelsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Submodel 1"
	  type "Linear"
    }
    {}
    {
      name  "Submodel 2"
	  type "Linear"
    }
  }
}

variable submodels_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetSubmodelsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable submodels_list_test
  
  if { $submodels_list_test } {
    return [GetSubmodelsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetSubmodelsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetSubmodelHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetSubmodelHelp" \
    "Text" "?"
}

variable create_submodel_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateSubmodel {submodel_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_submodel_test
  upvar $submodel_info submodel_array

  set container_set [TclLst2TolSet $container]

  if { $create_submodel_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateSubmodelTest" \
      "Real" submodel_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateSubmodel" \
      "Real" submodel_array $container_set
  }
}

variable copy_submodel_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopySubmodel {submodel_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_submodel_test
  upvar $submodel_info submodel_array
  
  set container_set [TclLst2TolSet $container]

  if { $copy_submodel_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CopySubmodelTest" \
	  "Real" submodel_array $container_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CopySubmodel" \
	  "Real" submodel_array $container_set
  }
}

variable edit_submodel_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditSubmodel {submodel_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_submodel_test
  upvar $submodel_info submodel_array
  
  set container_set [TclLst2TolSet $container]

  if { $edit_submodel_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditSubmodelTest" \
      "Real" submodel_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditSubmodel" \
      "Real" submodel_array $container_set
  }
}

#////////////////////////////////////////////////////////////////////////////
proc GetFactorsFromLabel {label} {
#////////////////////////////////////////////////////////////////////////////

  #puts "LayerMDMGui::GetFactorsFromLabel label=$label"
  LayerMMSGui::EvalTolFun "MMS::Layer::MDMGui::GetFactorsFromLabel" \
    "Set" $label
}

#////////////////////////////////////////////////////////////////////////////
proc GetLabelFromFactors {factors} {
#////////////////////////////////////////////////////////////////////////////

  #puts "LayerMDMGui::GetLabelFromFactors factors=$factors"
  set factors_set [TclLst2TolSet $factors -level 2]
  #puts "LayerMDMGui::GetLabelFromFactors factors_set=$factors_set"

  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetLabelFromFactors" \
    "Text" $factors_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetSubmodelTypePossibilitiesTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    "Linear"
    "Logit"
    "Probit"
  }
}

variable get_submtype_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetSubmodelTypePossibilities {} {
#///////////////////////////////////////////////////////////////////////////
  variable get_submtype_test
  
  if { $get_submtype_test } {
    return [GetSubmodelTypePossibilitiesTest]
  }
  set ltypes [LayerMMSGui::EvalTolFunVoid "MMS::Layer::MDMGui::GetSubmodelTypePossibilities" \
    "Set"]
	
  set _types [list]
  foreach { {} mt } $ltypes {
    lappend _types $mt
  }
  return $_types
}

}
