#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_mcombination_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the combinations of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMMCombinationsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateMCombinationsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateMCombinationsListDetails"
  set l_details_frame [frame $f.details_MCombinations]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "MCombination" \
    -swlist "::MDMMCombinationsGui::bmmsmcomblist" \
	-fshowitem "::MDMMCombinationsGui::_ShowMCombinationDetails" \
	-fshowlist "::MDMMCombinationsGui::_ShowMCombinationsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMCombinationsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateMCombinationsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowMCombinationsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  
  _ShowMCombinationHelp  

  _ShowMCombinationsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuMCombinations {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Combination"] \
    -command "::MDMMCombinationsGui::NewMCombination" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewMCombination {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMCombinationHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetMCombinationHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateMCombinationDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateMCombinationDetails"
  set details_frame [frame $f.details_MCombination]
  set view_frame [frame $f.view_MCombination]

  set _details [bmmsmcombination $details_frame.details]
  set _view [bmmsmcombination $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMCombinationDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateMCombinationDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowMCombinationDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set model [$tree item parent $parent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  set ident [$tree item text $id first]

  _ShowMCombinationDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandMCombinationsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  set _details "no" 
	
  set _list [LayerMDMGui::GetMCombinationsList $container $_details]
  #puts "ExpandMCombinationsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "MCombination"] \
	          [list ""] \
			  [list "MDMMCombinationsGui::ShowMCombinationDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id -button no
  }
}

 
#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsmcomblist {
# PURPOSE : Defines the snit widget used to
#           list the Mcombinations of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_comblist
    # mdm_comblist(list)  - Model combinations list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the combinations of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Combinations List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Terms"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Mean"]] \
        [list text -label [mc "Sigma"]] \
        [list text -label [mc "Inferior value"]] \
        [list text -label [mc "Superior value"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_comblist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_comblist(list) [LayerMDMGui::GetMCombinationsList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_comblist(list)=$mdm_comblist(list)"

    $tree item delete all

    foreach {{} it} $mdm_comblist(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(terms)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(mean)] \
	      [list $vinfo(sigma)] \
	      [list $vinfo(inf)] \
	      [list $vinfo(sup)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsmcombconstructor {
# PURPOSE : Defines the snit widget used to
#           construct combinations of parameters of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets
  
  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  # Identifier of the model to wich the combination belongs
  option -model \
    -default "" -configuremethod "_conf-model"  

  variable mdm_parlist
    # mdm_parlist(list)         - Parameters list
    # mdm_parlist(left_list)    - Parameters left list (Name, Type)
    # mdm_parlist(right_list)   - Parameters right list (Coeficient, Name)
    
  variable swaptablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create
    
    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-model { _ mod } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-model) $mod 
	#puts "bmmsmcombconstructor:_conf-model:options(-model)=$options(-model)"
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl configure -state $s
    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl configure -state $s
	$widgets(tree).lr.left configure -state $s
	$widgets(tree).lr.right configure -state $s
	
    foreach {w} {bref} {
      $widgets($w) configure -state $s
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the swaptablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_parlist(right_list) [list]
	
    set swaptablelist_options(-llabel) [mc "Parameters"]
    set swaptablelist_options(-rlabel) [mc "Selected Parameters"]
    set swaptablelist_options(-lcolumns) [list \
      [list 0 "Coef" left] \
      [list 0 [mc "Name"] left] \
      [list 0 [mc "Type"] left] \
    ]
    set swaptablelist_options(-rcolumns) [list \
      [list 0 [mc "Coefficient"] right] \
      [list 0 [mc "Name"] left] \
    ]
	set swaptablelist_options(-keycolumns) [list \
	  col_name \
    ]

    set swaptablelist_options(-height) 15
    set swaptablelist_options(-lwidth) 41
    set swaptablelist_options(-rwidth) 41
    set swaptablelist_options(-llistvar) [list]
    set swaptablelist_options(-rlistvar) [myvar mdm_parlist(right_list)]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          select parameters of a model
  #///////////////////////////////////////////////////////////////////////////

    set fs $dlg

    # Button: Refresh
    set fb [frame $fs.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Parameters List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
    set widgets(bref) $fb.bRef
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Swaptablelist frame
    set ft [frame $fs.ft]

    #puts "_create:swaptablelist_options=[array get swaptablelist_options]"
    set widgets(tree) [eval [list Swaptablelist::create_swap_list $ft] [array get swaptablelist_options]]
    #puts "_create:widgets(tree)=$widgets(tree)"    

	bind $widgets(tree) <<SwaptablelistUpdated>> \
	  "event generate $win <<OnChangedParameter>>"
	
    set ltbl [Swaptablelist::getLTable $widgets(tree)]
    $ltbl columnconfigure 0 -name col_coef -editable no -hide 1
    $ltbl columnconfigure 1 -name col_name -editable no -sortmode dictionary
    $ltbl columnconfigure 2 -name col_type -editable no -sortmode dictionary

    set rtbl [Swaptablelist::getRTable $widgets(tree)]
    $rtbl columnconfigure 0 -name col_coef -editable yes -labelcommand "$self DoNothing"
    $rtbl columnconfigure 1 -name col_name -editable no -labelcommand "$self DoNothing"

	bind $rtbl <<TablelistCellUpdated>> \
	  "event generate $win <<OnChangedCoefficient>>"
	    
    grid $fs.fb -sticky news
    grid $fs.ft -sticky news

    grid rowconfigure $fs 1 -weight 1
    grid columnconfigure $fs 0 -weight 1
    grid $fs -sticky news

    foreach {w} {tree bref} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    
    set mdm_parlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_parlist(list) [LayerMDMGui::GetParametersList \
      $options(-model) "no"]
    #puts "FillList mdm_parlist(list)=$mdm_parlist(list)"

	set _state $options(-state)
	$self put_state "normal"

    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl delete 0 end

    set mdm_parlist(left_list) [list]
    foreach {{} it} $mdm_parlist(list) {
      array set vinfo $it
      set row [list 1 $vinfo(ident) $vinfo(type)]
      lappend mdm_parlist(left_list) $row
    }

    Swaptablelist::insert $tbl $mdm_parlist(left_list)
      
    $self put_state $_state
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    $self FillList
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////
 
    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
	$self put_state "normal"

    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl delete 0 end

    Swaptablelist::insert $tbl $iteminfo
      
    $self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "get_info:mdm_parlist(right_list)=$mdm_parlist(right_list)"
    return $mdm_parlist(right_list)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl finishediting
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsmcombination {
# PURPOSE : Defines the snit widget used to
#           create new combinations or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  typevariable TextDisabledBackground

  # The container to wich the mcombination belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Name of the specific combination to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable combination_info
  # MCombination data to be edited
   # (Class @MCombination)           - (: @MElement)
    # combination_info(name)         - Combination name (Text _.name)
    # combination_info(desc)         - Description (Text _.description)
    # combination_info(g_active)     - Global active (IsActive method)
    #                                - (Set _.parameters)(@Parameter parameter)
    #                                - (Set _.coefficients)(Real coefficient)
    # combination_info(terms)        - List of terms (Coefficient, Parameter name)
    # combination_info(st_exp)       - Expresion of the combination as text
    # combination_info(prior)        - Prior {Mean Sigma IsActive} (H: Set _.prior)
	# combination_info(constraint)   - Constraint {InferiorValue SuperiorValue IsActive} (H: Set _.constraint)
    # combination_info(initial)      - Initial value

  variable terms
  variable priorvalues
  variable constraintvalues
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  typeconstructor {

    set w [entry .___e___ ]
    set TextDisabledBackground [$w cget -disabledbackground]
    destroy $w
  }
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Combination
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Combination"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Combination"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Combination"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1
	
    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lTerms -text "[mc "Terms"]:" -pady 5 -padx 5
    label $fe.lComb -text "[mc "Combination"]:" -pady 5 -padx 5
    label $fe.lInit -text "[mc "Initial Value"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar combination_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    entry $fe.eDesc -textvariable [myvar combination_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc

    checkbutton $fe.chkbActive -variable [myvar combination_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $fe.chkbActive

	CollapsableFrame $fe.cTerms \
	  -text "" -width 700 -height 390
    set widgets(terms) $fe.cTerms
	set ft [$widgets(terms) getframe]
	::MDMMCombinationsGui::bmmsmcombconstructor $ft.fTerms \
	  -state disabled
    set terms $ft.fTerms
	place $terms -x 5 -y 15
      
    bind $terms <<OnCancel>> "$self Cancel"
    bind $terms <<OnChangedCoefficient>> "$self UpdateCombExpr Coefficient"
    bind $terms <<OnChangedParameter>> "$self UpdateCombExpr Parameter"
	    
    set fExpr [frame $fe.fExpr]
    ::BayesText::CreateHLText $fExpr.t tol \
      -width 90 -height 3 -state disabled \
      -background $TextDisabledBackground -foreground black \
      -yscrollcommand "$fExpr.sy set" \
      -xscrollcommand "$fExpr.sx set" \
      -linemap 0 -wrap none
    scrollbar $fExpr.sy -orient vertical -command "$fExpr.t yview"  
    scrollbar $fExpr.sx -orient horizontal -command "$fExpr.t xview"  
    set widgets(st_exp) $fe.fExpr.t

    grid $fExpr.t -row 0 -column 0 -sticky news 
    grid $fExpr.sy -row 0 -column 1 -sticky ns
    grid $fExpr.sx -row 1 -column 0 -sticky ew

    entry $fe.eInit -textvariable [myvar combination_info(initial)] \
      -width 10 -state readonly
    set widgets(initial) $fe.eInit

    grid $fe.lName -row 0 -column 0 -sticky e
    grid $fe.eName -row 0 -column 1 -sticky w
    grid $fe.lDesc -row 1 -column 0 -sticky e
    grid $fe.eDesc -row 1 -column 1 -sticky w
    grid $fe.lActive -row 2 -column 0 -sticky e
    grid $fe.chkbActive -row 2 -column 1 -sticky w
    grid $fe.lTerms -row 3 -column 0 -sticky ne
    grid $fe.cTerms -row 3 -column 1 -sticky w
    grid $fe.lComb -row 4 -column 0 -sticky ne
    grid $fe.fExpr -row 4 -column 1 -sticky w
    grid $fe.lInit -row 5 -column 0 -sticky e
    grid $fe.eInit -row 5 -column 1 -sticky w

    # Prior
    label $fe.lPrior -text [mc "Prior"] -pady 5 -padx 5
    set fprior [labelframe $fe.fPrior \
      -labelwidget $fe.lPrior -relief solid -bd 1]
    set widgets(prior) $fe.fPrior
    
	::MDMPriorsGui::bmmspriorvalues $fprior.fPrior \
	  -state readonly
    set priorvalues $fprior.fPrior
      
    bind $priorvalues <<OnAccept>> "$self Ok"
    bind $priorvalues <<OnCancel>> "$self Cancel"

    grid rowconfigure $fprior 0 -weight 1
    grid columnconfigure $fprior 0 -weight 1
	
    grid $fprior -row 6 -column 0 -columnspan 2 \
	             -sticky news -pady 10 -padx 10

    # Constraint
    label $fe.lCnst -text [mc "Constraint"] -pady 5 -padx 5
    set fcnst [labelframe $fe.fCnst \
      -labelwidget $fe.lCnst -relief solid -bd 1]
    set widgets(constraint) $fe.fCnst

	::MDMConstraintsGui::bmmsconstraintvalues $fcnst.fCnst \
	  -state readonly
    set constraintvalues $fcnst.fCnst
      
    bind $constraintvalues <<OnAccept>> "$self Ok"
    bind $constraintvalues <<OnCancel>> "$self Cancel"

    grid rowconfigure    $fcnst 0 -weight 1
    grid columnconfigure $fcnst 0 -weight 1

    grid $fcnst -row 7 -column 0 -columnspan 2 \
	            -sticky news -pady 10 -padx 10

    grid rowconfigure    $fe 8 -weight 1
    grid columnconfigure $fe 2 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name desc} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel terms \
	             prior constraint} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name desc \
                 terms \
	             prior constraint
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetCombinationExpr {_terms} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set st_exp ""
    foreach {t} $_terms {
      set coef [lindex $t 0]
      set par [lindex $t 1]
      if {$coef<0} {
        set st_exp "$st_exp $coef*$par"
      } else {
        set st_exp "$st_exp +$coef*$par"
      }
    }
    return $st_exp
  }
 
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateCombExpr {field} {
  #/////////////////////////////////////////////////////////////////////////////
    variable terms_changed
    
    $self GetComb
   
    #puts "UpdateCombExpr:field=$field"
    #puts "UpdateCombExpr:combination_info(terms)=$combination_info(terms)"
    set combination_info(st_exp) [$self GetCombinationExpr $combination_info(terms)] 
        
    set text $widgets(st_exp)
    $text configure -state normal
    $text delete 1.0 end
    $text insert 1.0 $combination_info(st_exp)
    $text configure -state disabled
	
    set terms_changed 1
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetComb {} {
  #/////////////////////////////////////////////////////////////////////////////

    set combination_info(terms) [$terms get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetComb {} {
  #/////////////////////////////////////////////////////////////////////////////

	$terms configure -model $options(-container)
    $terms set_info $combination_info(terms)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetPriorValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    set combination_info(prior) [$priorvalues get_info ""]
    #puts "GetPriorValues: combination_info(prior)=$combination_info(prior)"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetPriorValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    $priorvalues set_info $combination_info(prior)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetConstraintValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    set combination_info(constraint) [$constraintvalues get_info ""]
    #puts "GetPriorValues: combination_info(constraint)=$combination_info(constraint)"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetConstraintValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    $constraintvalues set_info $combination_info(constraint)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set combination_info(name) ""
    set combination_info(desc) ""
    set combination_info(g_active) 0
    
    set combination_info(terms) {}
    $self SetComb

    set combination_info(st_exp) "" 
    set text $widgets(st_exp)
    $text configure -state normal
    $text delete 1.0 end
    $text configure -state disabled

    set combination_info(prior) {"" "" 0}
    $self SetPriorValues
	
    set combination_info(constraint) {"" "" 0}
    $self SetConstraintValues
	
    set combination_info(initial) ""
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

	array set combination_info [LayerMDMGui::GetMCombination $ident \
      $options(-container)]
    #puts "GetInfo:combination_info=[array get combination_info]"

    set _terms [list]
    foreach { {} t } $combination_info(terms) {
      lappend _terms [list [lindex $t 1] [lindex $t 3]]
    }
    set combination_info(terms) $_terms
    #puts "GetInfo:combination_info(terms)=$combination_info(terms)"
      
    $self SetComb
    
    set combination_info(st_exp) [$self GetCombinationExpr $combination_info(terms)] 
    set text $widgets(st_exp)
    $text configure -state normal
    $text delete 1.0 end
    $text insert 1.0 $combination_info(st_exp)
    $text configure -state disabled
	
    set _priorvalues [list]
    foreach { {} p } $combination_info(prior) {
      lappend _priorvalues $p
    }
    set combination_info(prior) $_priorvalues
	$self SetPriorValues
	
    set _constraintvalues [list]
    foreach { {} p } $combination_info(constraint) {
      lappend _constraintvalues $p
    }
    set combination_info(constraint) $_constraintvalues
	$self SetConstraintValues
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Combination"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name desc} {
      $widgets($w) configure -state readonly
    }
    $terms configure -state disabled
    $priorvalues configure -state readonly
    $constraintvalues configure -state readonly

    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable terms_changed

    set options(-state) "Edit"
    set label_state [mc "Edit Combination"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc} {
      $widgets($w) configure -state normal
    }
    $terms configure -state normal
    $priorvalues configure -state normal
    $constraintvalues configure -state normal

    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
	
	set terms_changed 0
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc} {
      $widgets($w) configure -state normal
    }
    $terms configure -state normal
    $priorvalues configure -state normal
    $constraintvalues configure -state normal
      
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Combination"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Combination"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable terms_changed

	::MMSGui::DisactivateEdition
	
	$terms finishediting
	  
    $self GetPriorValues
    $self GetConstraintValues

	set combination_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
	  set combination_info(ident) [$self cget -item]
	  #puts "Ok:terms_changed=$terms_changed"
	  if {$terms_changed eq 0} {
        LayerMDMGui::EditMCombination combination_info $combination_info(container) \
	      $combination_info(prior) $combination_info(constraint)

	    set new_ident $combination_info(name)
	    if {$combination_info(ident) ne $new_ident} {
	      if {$options(-parent) eq "tree"} {
          array set new_info [LayerMDMGui::GetMCombination \
            $new_ident [$self cget -container]]
	      set new_absid $new_info(abs_id)
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
	      } else {
            ::MDMMCombinationsGui::ChangeContainerTree $new_ident
            event generate $self <<Refresh>>
	      }
	    }

  	  } else {
	    LayerMDMGui::RemoveMCombination $combination_info(ident) \
           $combination_info(container)
        LayerMDMGui::CreateMCombination combination_info \
          $combination_info(terms) $combination_info(container) \
	      $combination_info(prior) $combination_info(constraint)
	    if {$options(-parent) eq "tree"} {
          event generate $self <<Insert>>
	    } else {

	      set new_ident $combination_info(name)
	      if {$combination_info(ident) ne $new_ident} {
		    ::MDMMCombinationsGui::ChangeContainerTree $new_ident
          }
		  
		  event generate $self <<Refresh>>
	    }
	  }
      
    } else {                                 ;# New, Copy
      LayerMDMGui::CreateMCombination combination_info \
	    $combination_info(terms) $combination_info(container) \
	    $combination_info(prior) $combination_info(constraint)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition

	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
