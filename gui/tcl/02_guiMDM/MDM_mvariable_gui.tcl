#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_mvariable_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the variables of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMMVariablesGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateMVariablesListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateMVariablesListDetails"
  set l_details_frame [frame $f.details_MVariables]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "MVariable" \
    -swlist "::MDMMVariablesGui::bmmsmvarlist" \
	-fshowitem "::MDMMVariablesGui::_ShowMVariableDetails" \
	-fshowlist "::MDMMVariablesGui::_ShowMVariablesListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMVariablesListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateMVariablesListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowMVariablesListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  _ShowMVariableHelp  

  _ShowMVariablesListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuMVariables {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Model Variable"] \
    -command "::MDMMVariablesGui::NewMVariable" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewMVariable {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc PasteVariables {tree} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  set id_active [$tree item id active]
  set parent [$tree item parent $id_active]
  set model [$tree item text $parent first]

  LayerMDMGui::PasteMVariables $model

  set fexpand [$tree item text $id_active [::MMSGui::FExpandColumn]]
  $fexpand $tree $id_active
  if {[$tree item isopen $id_active]} {
	$tree item expand $id_active
  }
  
  $l_details_frame.details RefreshList
}
	
#/////////////////////////////////////////////////////////////////////////////
proc _ShowMVariableHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetMVariableHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateMVariableDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateMVariableDetails"
  set details_frame [frame $f.details_MVariable]
  set view_frame [frame $f.view_MVariable]

  set _details [bmmsmvariable $details_frame.details]
  set _view [bmmsmvariable $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowMVariableDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateMVariableDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowMVariableDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set model [$tree item parent $parent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  set ident [$tree item text $id first]

  _ShowMVariableDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandMVariablesList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  set _details "no" 
	
  set _list [LayerMDMGui::GetMVariablesList $container $_details]
  #puts "ExpandMVariablesList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
	set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]	
    set row [list [list $icon $vinfo(ident)] \
	          [list "MVariable"] \
	          [list "MDMMVariablesGui::ExpandMVariable"] \
			  [list "MDMMVariablesGui::ShowMVariableDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandMVariable {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set row [list [list [::Bitmap::get "Set"] [mc "Parameters"]] \
			    [list "Parameters"] \
	            [list "MDMParametersGui::ExpandParametersList"] \
			    [list "MDMParametersGui::ShowParametersMissingListDetails"] \
			    [list "MDMParametersGui::CMenuParameters"] \
			    [list ""] \
          ]
  $tree insert $row \
    -at child -relative $id
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsmvarlist {
# PURPOSE : Defines the snit widget used to
#           list the model variables
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_mvarlist
    # mdm_mvarlist(list)  - Model Variables list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the variables of a model
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Model Variables List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Variable"]] \
        [list text -label [mc "Grammar"]] \
        [list text -label [mc "Transformation"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    focus $tree
    
    set mdm_mvarlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_mvarlist(list) [LayerMDMGui::GetMVariablesList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_mvarlist(list)=$mdm_mvarlist(list)"

    $tree item delete all

    foreach {{} it} $mdm_mvarlist(list) {
      array set vinfo $it
	  set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(var)] \
	      [list $vinfo(grammar)] \
	      [list $vinfo(transf)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    #if {$cur_item != 0} {
      #$tree activate $cur_item
      #$tree selection add $cur_item
    #}
    $tree activate $cur_item
    $tree selection add $cur_item
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsmvariable {
# PURPOSE : Defines the snit widget used to
#           create new model variables or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  #typevariable
  
  # The container to wich the variable belongs
  option -container \
    -default "" -configuremethod "_conf-container"  
  # If its Model belongs to MMS - {"MMS" <ident-Model>} 
  # If its Model belongs to an estimation - {"Estimation" <ident-Estimation>}
  # If its Model belongs to a forecast - {"Forecast" <ident-Forecast>}

  # Identifier of the specific model variable to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
  
  variable mvariable_info
  # Model variable data to be edited
   # (Class @MVariable)
    #                                  - (Set _parent_ // @ModelKernel)
    # mvariable_info(name)             - Model variable name (Text _.name)
    # mvariable_info(g_active)         - Global active (IsActive method)
    # mvariable_info(grammar)          - Grammar (Serie, Matrix, Real)
    #                                  - (Set _.variable_ // @Variable)
     # (Class @Variable)
      # mvariable_info(var)            - Variable identifier
    #                                  - (Set _.transformation // @Transformation)
     # (Class @Transformation)
      # mvariable_info(transf)         - Transformation info (It�s a named list)
	#                                  - (Set _.baseParameterMissing)
    #                                  - (Set _.parametersMissing)

  variable transformation

  component dlg
  
  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Model variable
  #///////////////////////////////////////////////////////////////////////////
    
    set f $dlg
    
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Model Variable"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Model Variable"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Model Variable"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lVar -text "[mc "Variable"]:" -pady 5 -padx 5
    label $fe.lGram -text "[mc "Grammar"]:" -pady 5 -padx 5
  
    entry $fe.eName -textvariable [myvar mvariable_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName

    checkbutton $fe.chkbActive -variable [myvar mvariable_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $fe.chkbActive

    ::MMSSelectorsGui::comboselector $fe.cVarSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Variable"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "Variable" \
	  -swlist "::VariablesGui::blist"
    set widgets(var) $fe.cVarSel

    bind $widgets(var) <<OnAccept>> "$self UpdateVariableGrammar"

    entry $fe.eGram -textvariable [myvar mvariable_info(grammar)] \
      -width 30 -state readonly
    set widgets(grammar) $fe.eGram

    label $fe.lTransf -text [mc "Transformation"] -pady 5 -padx 5
    set ft [labelframe $fe.fTransf \
      -labelwidget $fe.lTransf -relief solid -bd 1]
    set widgets(transf) $fe.fTransf

	::MDMTransformationsGui::bmmstransformation $ft.fTransf \
	  -state readonly
    set transformation $ft.fTransf
      
    bind $transformation <<OnAccept>> "$self Ok"
    bind $transformation <<OnCancel>> "$self Cancel"
    #bind $transformation <<OnChanged>> "$self ChangeFrameLabel"

    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
	
    grid $fe.lName    -row 0 -column 0 -sticky e
    grid $fe.eName    -row 0 -column 1 -sticky w
    grid $fe.lActive  -row 1 -column 0 -sticky e
    grid $fe.chkbActive -row 1 -column 1 -sticky w
    grid $fe.lVar     -row 2 -column 0 -sticky e
    grid $fe.cVarSel  -row 2 -column 1 -sticky w
    grid $fe.lGram    -row 3 -column 0 -sticky e
    grid $fe.eGram    -row 3 -column 1 -sticky w
    grid $fe.fTransf  -row 4 -column 0 -columnspan 2 \
	                  -sticky news -pady 10 -padx 10

    grid rowconfigure    $fe 5 -weight 1
    grid columnconfigure $fe 2 -weight 1
    
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    foreach {w} {name} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             var transf} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name \
	             var transf \
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateVariableGrammar {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetVariable
	set dataset_container $options(-container)
	if {[lindex $dataset_container 0] eq "MMS"} {
	  set dataset_container [lreplace $dataset_container 0 0 "Model"]
	}
    #! set mvariable_info(grammar) [::LayerVariablesGui::GetVariableGrammar \
	#!   $mvariable_info(var) $dataset_container]
    #! if {$mvariable_info(name) eq ""} {
    #!   set mvariable_info(name) $mvariable_info(var)
	#! }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetVariable {} {
  #/////////////////////////////////////////////////////////////////////////////

    set mvariable_info(var) [$widgets(var) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetVariable {} {
  #/////////////////////////////////////////////////////////////////////////////

	set dataset_container $options(-container)
	if {[lindex $dataset_container 0] eq "MMS"} {
	  set dataset_container [lreplace $dataset_container 0 0 "Model"]
	}
	$widgets(var) transient configure -containerid $dataset_container
    $widgets(var) set_info $mvariable_info(var)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetTransformation {} {
  #/////////////////////////////////////////////////////////////////////////////

    set mvariable_info(transf) [$transformation get_info ""]
    #puts "GetTransformation: mvariable_info(transf)=$mvariable_info(transf)"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetTransformation {} {
  #/////////////////////////////////////////////////////////////////////////////

    $transformation set_info $mvariable_info(transf)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set mvariable_info(name) ""
    set mvariable_info(g_active) 0

    set mvariable_info(var) ""
    $self SetVariable

	set mvariable_info(grammar) ""

    set mvariable_info(transf) {}
    $self SetTransformation
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]
	
    array set mvariable_info [LayerMDMGui::GetMVariable $ident \
      $options(-container)]
	#puts "GetInfo: mvariable_info=[array get mvariable_info]"

	$self SetTransformation
    $self SetVariable
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Model Variable"]

    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name} {
      $widgets($w) configure -state readonly
    }
    $transformation configure -state readonly
    $widgets(var) button configure -state disabled

    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Model Variable"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name} {
      $widgets($w) configure -state normal
    }
    $transformation configure -state normal
    $widgets(var) button configure -state disabled

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name} {
      $widgets($w) configure -state normal
    }
    $transformation configure -state normal
    $widgets(var) button configure -state normal

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(name) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Model Variable"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
    
    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Model Variable"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $self GetTransformation
	#puts "Ok:mvariable_info=[array get mvariable_info]"
    $self GetVariable
	
	set mvariable_info(container) [$self cget -container]
    
	if {$options(-state) eq "Edit"} {
	  set mvariable_info(ident) [$self cget -item]
      LayerMDMGui::EditMVariable mvariable_info $mvariable_info(transf) \
	     $mvariable_info(container)
	  
	  set new_ident $mvariable_info(name)
	  if {$mvariable_info(ident) ne $new_ident} {
	    if {$options(-parent) eq "tree"} {
          array set new_info [LayerMDMGui::GetMVariable \
            $new_ident [$self cget -container]]
	      set new_absid $new_info(abs_id)
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
	    } else {
          ::MDMMVariablesGui::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
	    }
	  }

    } else {                                             ;# New, Copy
      LayerMDMGui::CreateMVariable mvariable_info $mvariable_info(transf) \
	     $mvariable_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
    
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }
  
}

}
