#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_prior_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the priors of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMPriorsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreatePriorsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreatePriorsListDetails"
  set l_details_frame [frame $f.details_Priors]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Prior" \
    -swlist "::MDMPriorsGui::bmmspriorlist" \
	-fshowitem "::MDMPriorsGui::_ShowPriorDetails" \
	-fshowlist "::MDMPriorsGui::_ShowPriorsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowPriorsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreatePriorsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowPriorsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  
  _ShowPriorHelp  

  _ShowPriorsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuPriors {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Prior"] \
    -command "::MDMPriorsGui::NewPrior" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewPrior {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowPriorHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetPriorHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreatePriorDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreatePriorDetails"
  set details_frame [frame $f.details_Prior]
  set view_frame [frame $f.view_Prior]

  set _details [bmmsprior $details_frame.details]
  set _view [bmmsprior $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowPriorDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreatePriorDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowPriorDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set model [$tree item parent $parent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  set ident [$tree item text $id first]

  _ShowPriorDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandPriorsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  set _details "no" 
	
  set _list [LayerMDMGui::GetPriorsList $container &_details]
  #puts "ExpandPriorsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }

  foreach {{} it} $_list {
    array set vinfo $it
	set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "Prior"] \
	          [list ""] \
			  [list "MDMPriorsGui::ShowPriorDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id -button no
  }
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmspriorlist {
# PURPOSE : Defines the snit widget used to
#           list the priors of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_priorlist
    # mdm_priorlist(list)  - Priors list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the priors of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Priors List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Element"]] \
        [list text -label [mc "Mean"]] \
        [list text -label [mc "Sigma"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_priorlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_priorlist(list) [LayerMDMGui::GetPriorsList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_priorlist(list)=$mdm_priorlist(list)"

    $tree item delete all

    foreach {{} it} $mdm_priorlist(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list [mc $vinfo(elem)]] \
	      [list $vinfo(mean)] \
	      [list $vinfo(sigma)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmspriorvalues {
# PURPOSE : Defines the snit widget used to
#           edit the values of the prior of a
#           Parameter, MCombination or MEquivalence
#/////////////////////////////////////////////////////////////////////////////

  #typevariable
  
  # State (readonly, normal)
  option -state \
    -default "readonly" -configuremethod "_conf-state" 

  variable widgets

  variable priorvalues_info
  # Values of the Prior to be edited
   # (Class @Prior)
    # priorvalues_info(mean)       - Mean (Real _.mean)
    # priorvalues_info(sigma)      - Sigma (Real _.sigma)
    # priorvalues_info(active)     - Active (Real _.isActive)

  component dlg
  
  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky news

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
    foreach w {mean sigma} {
      $widgets($w) configure -state $s
    }
    foreach w {del active} {
      if {$s eq "normal"} {
	    $widgets($w) configure -state "normal"
	  } else {
	    $widgets($w) configure -state "disabled"
	  }
    }
  }
    
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          edit the values of a Prior
  #///////////////////////////////////////////////////////////////////////////
   
    set fe $dlg

    label $fe.lMean -text "[mc "Mean"]:" -pady 5 -padx 5
    label $fe.lSigma -text "[mc "Sigma"]:" -pady 5 -padx 5

    entry $fe.eMean -textvariable [myvar priorvalues_info(mean)] \
      -width 10 -state readonly
    set widgets(mean) $fe.eMean
    
    entry $fe.eSigma -textvariable [myvar priorvalues_info(sigma)] \
      -width 10 -state readonly
    set widgets(sigma) $fe.eSigma
        
    Button $fe.bDel -image [::Bitmap::get delete] -text [mc "Delete"] \
      -helptext "[mc "Delete"] [mc "Prior"]" \
	  -padx 1 -relief link \
      -compound left -command [list $self Delete] \
      -state disabled
    set widgets(del) $fe.bDel

    checkbutton $fe.chkbActive -variable [myvar priorvalues_info(active)] \
      -text [mc "Active"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(active) $fe.chkbActive

    grid $fe.lMean      -row 0 -column 0 -sticky e
    grid $fe.eMean      -row 0 -column 1 -sticky w
    grid $fe.bDel       -row 0 -column 2 -sticky w -padx 5
    grid $fe.lSigma     -row 1 -column 0 -sticky e
    grid $fe.eSigma     -row 1 -column 1 -sticky w
    grid $fe.chkbActive -row 1 -column 2 -sticky w -padx 5

    grid rowconfigure    $fe 2 -weight 1
    grid columnconfigure $fe 3 -weight 1
    grid $fe -sticky news

    foreach {w} {mean sigma \
	             active} {
      bind $widgets($w) <Return> "$self Ok ; break"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {del} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {mean sigma \
	             del active} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Delete {} {
  #/////////////////////////////////////////////////////////////////////////////

	set priorvalues_info(mean) ""
	set priorvalues_info(sigma) ""
    set priorvalues_info(active) 0
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////
 
    #puts "iteminfo=$iteminfo"
	set priorvalues_info(mean) [lindex $iteminfo 0]
	set priorvalues_info(sigma) [lindex $iteminfo 1]
	set priorvalues_info(active) [lindex $iteminfo 2]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    return [list $priorvalues_info(mean) $priorvalues_info(sigma) \
	             $priorvalues_info(active)]
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsprior {
# PURPOSE : Defines the snit widget used to
#           create new prior or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  typevariable TextDisabledBackground

  # The container to wich the prior belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Name of the specific prior to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable prior_info
  # Prior data to be edited
   # (Class @Prior)
    # prior_info(name)            - Prior identifier
    # prior_info(g_active)        - Global active (IsActive method)
    # prior_info(l_active)        - Local active (H: Real _.isActive)
    #                             - (Set _parent_ // @MElement
     # prior_info(elem)           - Class of element (Parameter, Combination, Equivalence)
      # prior_info(e_name)        - Element name (Text _.name)
    # prior_info(mean)            - Mean (Real _.mean)
    # prior_info(sigma)           - Sigma (Real _.sigma)
    # prior_info(st_exp)          - Expresion of the prior as text

  component dlg

  delegate method * to hull
  delegate option * to hull

  typeconstructor {

    set w [entry .___e___ ]
    set TextDisabledBackground [$w cget -disabledbackground]
    destroy $w
  }
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Prior
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Prior"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Prior"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Prior"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lElem -text "[mc "Element"]:" -pady 5 -padx 5
    label $fe.lMean -text "[mc "Mean"]:" -pady 5 -padx 5
    label $fe.lSigma -text "[mc "Sigma"]:" -pady 5 -padx 5
    label $fe.lExpr -text "[mc "Prior"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar prior_info(name)] \
      -width 50 -state readonly
    set widgets(name) $fe.eName
    
    set factive [frame $fe.fActive]

    checkbutton $factive.chkbLActive -variable [myvar prior_info(l_active)] \
      -text [mc "Local"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(l_active) $factive.chkbLActive

    checkbutton $factive.chkbGActive -variable [myvar prior_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $factive.chkbGActive

    grid $factive.chkbLActive $factive.chkbGActive -sticky w -padx 2 -pady 2
    grid rowconfigure    $factive 0 -weight 1
    grid columnconfigure $factive 2 -weight 1

    set felem [frame $fe.fElem]
    foreach item {Parameter Combination Equivalence} {
      radiobutton $felem.rbElem$item -text [mc $item] -value $item \
        -variable [myvar prior_info(elem)] \
        -state disabled \
        -command "$self UpdateElement"
      pack $felem.rbElem$item -side left
    }
    set widgets(elem_par) $felem.rbElemParameter
    set widgets(elem_comb) $felem.rbElemCombination
    set widgets(elem_equiv) $felem.rbElemEquivalence

    ::MMSSelectorsGui::comboselector $fe.cParSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Parameter"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "Parameter" \
	  -swlist "::MDMParametersGui::bmmsparlist"

    set widgets(par) $fe.cParSel

    bind $widgets(par) <<OnAccept>> "$self UpdatePriorExpr Element"
    
    ::MMSSelectorsGui::comboselector $fe.cCombSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Combination"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "MCombination" \
	  -swlist "::MDMMCombinationsGui::bmmsmcomblist"

    set widgets(comb) $fe.cCombSel

    bind $widgets(comb) <<OnAccept>> "$self UpdatePriorExpr Element"
    
    ::MMSSelectorsGui::comboselector $fe.cEquivSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select an Equivalence"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "MEquivalence" \
	  -swlist "::MDMMEquivalencesGui::bmmsmequivlist"
    set widgets(equiv) $fe.cEquivSel

    bind $widgets(equiv) <<OnAccept>> "$self UpdatePriorExpr Element"
    
    set widgets(e_name) $widgets(par)
    
    entry $fe.eMean -textvariable [myvar prior_info(mean)] \
      -width 10 -state readonly
    set widgets(mean) $fe.eMean
    set prior_info(mean) ""
    trace add variable prior_info(mean) write "$self ChangedValue"
    
    entry $fe.eSigma -textvariable [myvar prior_info(sigma)] \
      -width 10 -state readonly
    set widgets(sigma) $fe.eSigma
    set prior_info(sigma) ""
    trace add variable prior_info(sigma) write "$self ChangedValue"
        
    set fExpr [frame $fe.fExpr]
    ::BayesText::CreateHLText $fExpr.t tol \
      -width 60 -height 3 -state disabled \
      -background $TextDisabledBackground -foreground black \
      -yscrollcommand "$fExpr.sy set" \
      -xscrollcommand "$fExpr.sx set" \
      -linemap 0 -wrap none
    scrollbar $fExpr.sy -orient vertical -command "$fExpr.t yview"  
    scrollbar $fExpr.sx -orient horizontal -command "$fExpr.t xview"  
    set widgets(st_exp) $fe.fExpr.t

    grid $fExpr.t -row 0 -column 0 -sticky news 
    grid $fExpr.sy -row 0 -column 1 -sticky ns
    grid $fExpr.sx -row 1 -column 0 -sticky ew

    grid $fe.lName -row 0 -column 0 -sticky e
    grid $fe.eName -row 0 -column 1 -sticky w
    grid $fe.lActive -row 1 -column 0 -sticky e
    grid $fe.fActive -row 1 -column 1 -sticky w
    grid $fe.lElem -row 2 -column 0 -sticky e
    grid $fe.fElem -row 2 -column 1 -sticky w
    grid $fe.cEquivSel -row 3 -column 1 -sticky w
    grid remove $fe.cEquivSel
    grid $fe.cCombSel -row 3 -column 1 -sticky w
    grid remove $fe.cCombSel
    grid $fe.cParSel -row 3 -column 1 -sticky w
    grid $fe.lMean -row 4 -column 0 -sticky e
    grid $fe.eMean -row 4 -column 1 -sticky w
    grid $fe.lSigma -row 5 -column 0 -sticky e
    grid $fe.eSigma -row 5 -column 1 -sticky w
    grid $fe.lExpr -row 6 -column 0 -sticky ne
    grid $fe.fExpr -row 6 -column 1 -sticky w -pady 10

    grid rowconfigure    $fe 7 -weight 1
    grid columnconfigure $fe 2 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {mean sigma \
                 elem_par elem_comb elem_equiv \
                 l_active} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {mean sigma \
                 elem_par elem_comb elem_equiv \
                 l_active \
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(l_active) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ChangedValue {args} {
  #/////////////////////////////////////////////////////////////////////////////

    $self UpdatePriorExpr "Value"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateElement {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    grid remove $widgets(par)
    grid remove $widgets(comb)
    grid remove $widgets(equiv)
    switch -- $prior_info(elem) {
      "Parameter"   {
        set widgets(e_name) $widgets(par)
	  }
      "Combination" {
        set widgets(e_name) $widgets(comb)
	  }
      "Equivalence" {
        set widgets(e_name) $widgets(equiv)
	  }
    }
    grid $widgets(e_name)
	$widgets(e_name) transient configure -container $options(-container)
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method GetPriorExpr {field} {
  #/////////////////////////////////////////////////////////////////////////////
    variable st_element
    
	if {$field eq "Element"} {
      switch -- $prior_info(elem) {
        "Parameter"   {
          set st_element "$prior_info(e_name) "
	    }
        "Combination" {
          set terms [LayerMDMGui::GetPriorTerms $prior_info(e_name) \
            $options(-container)]
          #puts "GetPriorExpr:terms=$terms"
          set st_element ""
          foreach {{} t} $terms {
            set coef [lindex $t 1]
            set par [lindex $t 3]
            if {$coef<0} {
              set st_element "$st_element$coef*$par "
            } else {
              set st_element "$st_element+$coef*$par "
            }
          }
	    }
        "Equivalence" {
          set st_element "$prior_info(e_name) "
	    }
      }
	}
	
    set st_exp "$st_element~ N($prior_info(mean), $prior_info(sigma))"
    return $st_exp
  }
 
  #/////////////////////////////////////////////////////////////////////////////
  method UpdatePriorExpr {field} {
  #/////////////////////////////////////////////////////////////////////////////
    variable elem_changed
    
    if {$field eq "Element"} {
      $self GetElem
	  set elem_changed 1
    }
	
    set prior_info(st_exp) [$self GetPriorExpr $field] 
        
    set text $widgets(st_exp)
    $text configure -state normal
    $text delete 1.0 end
    $text insert 1.0 $prior_info(st_exp)
    $text configure -state disabled
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetElem {} {
  #/////////////////////////////////////////////////////////////////////////////

    set prior_info(e_name) [$widgets(e_name) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetElem {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(comb) set_info ""
    $widgets(equiv) set_info ""
    $widgets(par) set_info ""
	
	$widgets(e_name) transient configure -container $options(-container)
    $widgets(e_name) set_info $prior_info(e_name)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable st_element
    
    set st_element ""
    
    set prior_info(name) ""
    set prior_info(l_active) 1
    set prior_info(g_active) 0
    
    set prior_info(elem) "Parameter"
    set prior_info(e_name) ""
    $self UpdateElement
    $self SetElem 
    
    set prior_info(mean) ""
    set prior_info(sigma) ""

    $self UpdatePriorExpr "Element"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable st_element
    
    set ident [$self cget -item]

	set st_element ""
    array set prior_info [LayerMDMGui::GetPrior $ident \
      $options(-container)]
    #puts "GetInfo:prior_info=[array get prior_info]"

    $self UpdateElement
    $self SetElem

    $self UpdatePriorExpr "Element"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Prior"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {mean sigma} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {elem_par elem_comb elem_equiv \
                 l_active} {
      $widgets($w) configure -state disabled
    }
    $widgets(comb) button configure -state disabled
    $widgets(equiv) button configure -state disabled
    $widgets(par) button configure -state disabled
    
    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable elem_changed

    set options(-state) "Edit"
    set label_state [mc "Edit Prior"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {mean sigma \
	             elem_par elem_comb elem_equiv \
				 l_active} {
      $widgets($w) configure -state normal
    }
    $widgets(comb) button configure -state normal
    $widgets(equiv) button configure -state normal
    $widgets(par) button configure -state normal

    focus $widgets(l_active)
    bind $widgets(cancel) <Tab> "focus $widgets(l_active) ; break"
    bind $widgets(l_active) <Shift-Tab> "focus $widgets(cancel) ; break"
	
	set elem_changed 0
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {mean sigma \
                 l_active \
                 elem_par elem_comb elem_equiv} {
      $widgets($w) configure -state normal
    }
    $widgets(comb) button configure -state normal
    $widgets(equiv) button configure -state normal
    $widgets(par) button configure -state normal
      
    focus $widgets(l_active)
    bind $widgets(cancel) <Tab> "focus $widgets(l_active) ; break"
    bind $widgets(l_active) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Prior"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Prior"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable elem_changed

	::MMSGui::DisactivateEdition
	
	set prior_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
	  if {$elem_changed==0} {
        LayerMDMGui::EditPrior prior_info $prior_info(container)
      } else {
	    LayerMDMGui::RemovePrior $prior_info(name) \
          $prior_info(container)
        LayerMDMGui::CreatePrior prior_info $prior_info(container)
	    if {$options(-parent) eq "tree"} {
          event generate $self <<Insert>>
	    } else {
          event generate $self <<Refresh>>
	    }
	  }
      
    } else {                                 ;# New, Copy
      LayerMDMGui::CreatePrior prior_info $prior_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
