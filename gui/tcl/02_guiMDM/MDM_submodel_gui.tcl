#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_submodel_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the submodels of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMSubmodelsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateSubmodelsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateSubmodelsListDetails"
  set l_details_frame [frame $f.details_Submodels]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Submodel" \
    -swlist "::MDMSubmodelsGui::bmmssubmlist" \
	-fshowitem "::MDMSubmodelsGui::_ShowSubmodelDetails" \
	-fshowlist "::MDMSubmodelsGui::_ShowSubmodelsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowSubmodelsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateSubmodelsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowSubmodelsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  
  _ShowSubmodelHelp  

  _ShowSubmodelsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuSubmodels {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Submodel"] \
    -command "::MDMSubmodelsGui::NewSubmodel" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewSubmodel {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowSubmodelHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetSubmodelHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateSubmodelDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateSubmodelDetails"
  set details_frame [frame $f.details_Submodel]
  set view_frame [frame $f.view_Submodel]

  set _details [bmmssubmodel $details_frame.details]
  set _view [bmmssubmodel $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowSubmodelDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateSubmodelDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowSubmodelDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set model [$tree item parent $parent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  set ident [$tree item text $id first]

  _ShowSubmodelDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandSubmodelsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  set _details "no" 
	
  set _list [LayerMDMGui::GetSubmodelsList $container $_details]
  #puts "ExpandSubmodelsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }

  foreach {{} it} $_list {
    array set vinfo $it
    set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "Submodel"] \
	          [list "MDMSubmodelsGui::ExpandSubmodel"] \
	          [list "MDMSubmodelsGui::ShowSubmodelDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandSubmodel {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }

  set _list [list \
	"Explanatory Terms"       "MDMExpTermsGui::ExpandExpTermsList" \
        	                  "MDMExpTermsGui::ShowExpTermsListDetails" \
        	                  "MDMExpTermsGui::CMenuExpTerms" \
	"Parameters"              "MDMParametersGui::ExpandParametersList" \
        	                  "MDMParametersGui::ShowParametersListDetails" \
        	                  "" \
  ]
  
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list [::Bitmap::get "Set"] [mc $name]] \
			  [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmssubmlist {
# PURPOSE : Defines the snit widget used to
#           list the submodels of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_submlist
    # mdm_submlist(list)  - Submodels list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the submodels of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Submodels List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]

	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Type"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Output"]] \
        [list text -label [mc "Grammar"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_submlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_submlist(list) [LayerMDMGui::GetSubmodelsList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_submlist(list)=$mdm_submlist(list)"

    $tree item delete all

    foreach {{} it} $mdm_submlist(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(type)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(out)] \
	      [list $vinfo(grammar)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
			  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
  
}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsfactors {
# PURPOSE : Defines the snit widget used to
#           edit the ARIMA factors
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdm_factors
  # Factors to be edited at the tablelist
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
    set lrow [$widgets(tbl) size]
    for {set _r 0} {$_r < $lrow} {incr _r} {
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -state $s
      set btn_plus [$widgets(tbl) windowpath $_r,col_bplus]
      if {$btn_plus ne ""} {
	    $btn_plus configure -state $s
	  }
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_factors [list]

    set tablelist_options(-label) [mc "Factors"]
    set tablelist_options(-columns) [list \
      0 [mc "Period"] right \
      0 [mc "Dif.Order"] right \
      0 [mc "AR"] right \
      0 [mc "MA"] right \
      0 "" center \
      0 "" center \
    ]
    set tablelist_options(-listvar) [myvar mdm_factors]   
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit a list of factors
  #///////////////////////////////////////////////////////////////////////////
   
    set ft $dlg
    # Tablelist frame

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 5 -width 60
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    $widgets(tbl) columnconfigure 0 -name col_period -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 1 -name col_diff -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 2 -name col_ar -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 3 -name col_ma -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 4 -name col_bminus -hide true -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 5 -name col_bplus -labelimage [::Bitmap::get plus] \
	                                -labelcommand "$self AddFactor"
    
    grid rowconfigure $ft 1 -weight 1
    grid columnconfigure $ft 0 -weight 1
    grid $ft -sticky news
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ConfigureColumns {nrows} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$nrows>0} {
	  $widgets(tbl) columnconfigure col_bminus -hide false
	  $widgets(tbl) columnconfigure col_bplus -labelimage "" \
	    -labelcommand "$self DoNothing"
      $widgets(tbl) cellconfigure end,col_bplus -window [mymethod CreateButtonPlus]
	  if {$nrows>=2} {
        $widgets(tbl) cellconfigure [expr $nrows-2],col_bplus -window ""
	  }

    } else {
	  $widgets(tbl) columnconfigure col_bminus -hide true
	  $widgets(tbl) columnconfigure col_bplus -labelimage [::Bitmap::get plus] \
	    -labelcommand "$self AddFactor"
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method DeleteRow {row} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "DeleteRow:row=$row"
    $widgets(tbl) delete [list $row]
    
    set lrow [$widgets(tbl) size]
    for {set _r $row} {$_r < $lrow} {incr _r} {
      #puts "DeleteRow:_r=$_r,windowpath=[$widgets(tbl) windowpath $_r,1]"
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -command [list $self DeleteRow $_r]
    }
	$self ConfigureColumns $lrow
    
	event generate $win <<OnDeleted>>
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method AddFactor {args} {
  #/////////////////////////////////////////////////////////////////////////////

	$widgets(tbl) finishediting
	update
    $widgets(tbl) insert end [list "0" "0" "1" "1"]
	
    set lrow [expr [$widgets(tbl) size]-1]
    $widgets(tbl) cellconfigure $lrow,col_bplus -window [mymethod CreateButtonPlus]
    $widgets(tbl) cellconfigure $lrow,col_bminus -window [mymethod CreateButtonMinus]
    $widgets(tbl) editcell $lrow,0

	$self ConfigureColumns [$widgets(tbl) size]
    
	event generate $win <<OnAdded>>
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonMinus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get minus] -highlightthickness 0 -takefocus 0 \
      -command [list $self DeleteRow $row]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonPlus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get plus] -highlightthickness 0 -takefocus 0 \
      -command [list $self AddFactor]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
	$self put_state "normal"
	
    $widgets(tbl) delete 0 end
    #set mdm_factors $iteminfo
    set idx_button [expr [$widgets(tbl) columncount]-2] 
    set row 0
    foreach {r} $iteminfo {
      $widgets(tbl) insert end [lrange $r 0 [expr $idx_button-1]]
      $widgets(tbl) cellconfigure $row,col_bminus -window [mymethod CreateButtonMinus]
      incr row
    }
	$self ConfigureColumns $row

	$self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set idx_button [expr [$widgets(tbl) columncount]-2] 
    set _factors [list]
    foreach {r} $mdm_factors {
      lappend _factors [lrange $r 0 [expr $idx_button-1]]
    }
    #puts "Ok:_factors=$_factors"
    return $_factors
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmssubmodel {
# PURPOSE : Defines the snit widget used to
#           create new submodels or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  typevariable ListTypes
  # Types of submodels (Linear, Logit, Probit)

  # The container to wich the variable belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Name of the specific submodel to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable submodel_info
  # Submodel data to be edited
   # (Class @Submodel)              - : @MNode
	#                               - (H: Set _parent_ // @Model)
    # submodel_info(name)           - Submodel name (H: Text _.name)
    # submodel_info(desc)           - Description (H: Text _.description)
    # submodel_info(active)         - Global & Local active (IsActive method)&(H: Real _.isActive)
	#                               - (Set _.output_)
    # submodel_info(output)         - Output Identifier (@MVariable _.output[1])
    # submodel_info(grammar)        - Grammar (Serie, Matrix, Real)
    #                               - (Set _.weight_)
    #                               - (Set _.expTerms)
    # submodel_info(type)           - Submodel type (Text _.type)(Linear, Logit, Probit)
    #                               - (Set _.begin.) // Submodel interval
    #                               - (Set _.end.)
    # submodel_info(serbegin)       - Begin Date for Serie
    # submodel_info(begin)          - Value is TheBegin
    # submodel_info(serend)         - End Date for Serie
    # submodel_info(end)            - Value is TheEnd
    # submodel_info(rowbegin)       - Begin Row for Matrix
    # submodel_info(colbegin)       - Begin Column
    # submodel_info(rowend)         - End Row for Matrix
    # submodel_info(colend)         - End Column
	#                               - (H: Set _.noise)
     # (Class @Noise)               -
      #                             - (Set _.sigma2 //@ParameterSigma2)
       # (Class @ParameterSigma2)   - : @Parameter
        # submodel_info(sigma0)     - (_.sigma) Initial Sigma2 value (Real _.initialValue)
        # submodel_info(sigmaw)     - (_.sigmaFixed) Sigma weight, =1->Is Fixed (Real _.isFixed)
      # (Class @NoiseNormal)        - : @Noise
       #                            - Covariance structure (Set _.relativeSigmas)
      # (Class @NoiseARIMA)         - : @Noise
       #                            - Covariance structure and parameters (Set _.arimaBlocks)
       # (Class @ARIMABlock)        -
        #                           - (Real _.period)
        #                           - (Real _.differenceDegree)
        #                           - (Set _.parametersARIMA)
        # submodel_info(fixed)      - Fixed ARIMA Parameters?

  variable factors
  	  
  component dlg
  
  delegate method * to hull
  delegate option * to hull

  typeconstructor {

	#set ListTypes [::LayerMDMGui::GetSubmodelTypePossibilities]
  }
	
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Submodel
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Submodel"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Submodel"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Submodel"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Type"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lOut  -text "[mc "Output"]:" -pady 5 -padx 5
    label $fe.lGram -text "[mc "Grammar"]:" -pady 5 -padx 5
    label $fe.lBData -text "[mc "Begin"]:" -pady 5 -padx 5
    label $fe.lEData -text "[mc "End"]:" -pady 5 -padx 5
    label $fe.lSigma -text "[mc "Prior of Sigma2"]:" -pady 5 -padx 5
    label $fe.lARIMA -text "[mc "ARIMA Information"]:" -pady 5 -padx 5
    set widgets(lARIMA) $fe.lARIMA
    
    entry $fe.eName -textvariable [myvar submodel_info(name)] \
      -width 40 -state readonly 
    set widgets(name) $fe.eName

	set ListTypes [::LayerMDMGui::GetSubmodelTypePossibilities]
    ComboBox $fe.cbType -values $ListTypes \
      -textvariable [myvar submodel_info(type)] -editable false \
      -width 15 -state disabled \
	  -modifycmd [string map [list %W $fe.cbType %M [mymethod ShowFType]] {
        set idx [%W getvalue]
        if {$idx != -1} {   
		  %M
        }
      }]
    set widgets(type) $fe.cbType

    entry $fe.eDesc -textvariable [myvar submodel_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc

    checkbutton $fe.chkbActive -variable [myvar submodel_info(active)] \
      -onvalue 1 -offvalue 0 -state disabled
    set widgets(active) $fe.chkbActive

    ::MMSSelectorsGui::comboselector $fe.cOutSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Model Variable"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "MVariable" \
	  -swlist "::MDMMVariablesGui::bmmsmvarlist"
    set widgets(out) $fe.cOutSel

    bind $widgets(out) <<OnAccept>> "$self UpdateVariableGrammar"

    entry $fe.eGram -textvariable [myvar submodel_info(grammar)] \
      -width 30 -state readonly
    set widgets(grammar) $fe.eGram

    # Begin
    set fBSerie [frame $fe.fBSerie]
    set widgets(fbserie) $fBSerie
    
    label $fBSerie.lDate -text "[mc "Date"]:" -padx 5

    ::datefield::datefield $fBSerie.df -textvariable [myvar submodel_info(serbegin)] \
      -state disabled -format y/m/d
    set widgets(serbegin) $fBSerie.df
      
    checkbutton $fBSerie.chkb -variable [myvar submodel_info(begin)] \
      -text [mc "TheBegin"] -onvalue 1 -offvalue 0 -state disabled \
      -command [string map [list %W $fBSerie.chkb %f $fBSerie.df] {
        set vname [%W cget -variable]
        upvar \#0 $vname value
        if {$value == 0} {   
          grid %f
          %f configure -state normal
        } else {
          grid remove %f
        }
      } ]
    set widgets(begin) $fBSerie.chkb

    grid $fBSerie.lDate $fBSerie.df $fBSerie.chkb -sticky e
    grid rowconfigure    $fBSerie 0 -weight 1
    grid columnconfigure $fBSerie 0 -weight 1

    set fBMatrix [frame $fe.fBMatrix]
    set widgets(fbmatrix) $fBMatrix
    
    label $fBMatrix.lRow -text "[mc "Row"]:" -padx 5
    label $fBMatrix.lCol -text "[mc "Column"]:" -padx 5

    entry $fBMatrix.eBRow -textvariable [myvar submodel_info(rowbegin)] \
      -width 5 -state readonly
    set widgets(rowbegin) $fBMatrix.eBRow
      
    entry $fBMatrix.eBCol -textvariable [myvar submodel_info(colbegin)] \
      -width 3 -state readonly
    set widgets(colbegin) $fBMatrix.eBCol

    grid $fBMatrix.lRow $fBMatrix.eBRow $fBMatrix.lCol $fBMatrix.eBCol -sticky e
    grid rowconfigure    $fBMatrix 0 -weight 1
    grid columnconfigure $fBMatrix 0 -weight 1

    # End
    set fESerie [frame $fe.fESerie]
    set widgets(feserie) $fESerie
      
    label $fESerie.lDate -text "[mc "Date"]:" -padx 5

    ::datefield::datefield $fESerie.df -textvariable [myvar submodel_info(serend)] \
      -state disabled -format y/m/d
    set widgets(serend) $fESerie.df
      
    checkbutton $fESerie.chkb -variable [myvar submodel_info(end)] \
      -text [mc "TheEnd"] -onvalue 1 -offvalue 0 -state disabled \
      -command [string map [list %W $fESerie.chkb %f $fESerie.df] {
        set vname [%W cget -variable]
        upvar \#0 $vname value
        if {$value == 0} {   
          grid %f
          %f configure -state normal
        } else {
          grid remove %f
        }
      } ]
    set widgets(end) $fESerie.chkb

    grid $fESerie.lDate $fESerie.df $fESerie.chkb -sticky e
    grid rowconfigure    $fESerie 0 -weight 1
    grid columnconfigure $fESerie 0 -weight 1

    set fEMatrix [frame $fe.fEMatrix]
    set widgets(fematrix) $fEMatrix
    
    label $fEMatrix.lRow -text "[mc "Row"]:" -padx 5
    label $fEMatrix.lCol -text "[mc "Column"]:" -padx 5

    entry $fEMatrix.eBRow -textvariable [myvar submodel_info(rowend)] \
      -width 5 -state readonly
    set widgets(rowend) $fEMatrix.eBRow
      
    entry $fEMatrix.eBCol -textvariable [myvar submodel_info(colend)] \
      -width 3 -state readonly
    set widgets(colend) $fEMatrix.eBCol

    grid $fEMatrix.lRow $fEMatrix.eBRow $fEMatrix.lCol $fEMatrix.eBCol -sticky e
    grid rowconfigure    $fEMatrix 0 -weight 1
    grid columnconfigure $fEMatrix 0 -weight 1

    # Prior of Sigma2
	CollapsableFrame $fe.cSigma \
	  -text "" -width 200 -height 85
	set cfSigma [$fe.cSigma getframe]
	set fSigma [frame $cfSigma.f]

    label $fSigma.lSigma0 -text "[mc "Sigma0"]:" -pady 5 -padx 5
    label $fSigma.lSigmaW -text "[mc "Weight"]:" -pady 5 -padx 5

    entry $fSigma.eSigma0 -textvariable [myvar submodel_info(sigma0)] \
      -width 10 -state readonly 
    set widgets(sigma0) $fSigma.eSigma0

    entry $fSigma.eSigmaW -textvariable [myvar submodel_info(sigmaw)] \
      -width 10 -state readonly 
    set widgets(sigmaw) $fSigma.eSigmaW
  
    grid $fSigma.lSigma0 -row 0 -column 0 -sticky e
    grid $fSigma.eSigma0 -row 0 -column 1 -sticky w
    grid $fSigma.lSigmaW -row 1 -column 0 -sticky e
    grid $fSigma.eSigmaW -row 1 -column 1 -sticky w
	
    grid rowconfigure    $fSigma 2 -weight 1
    grid columnconfigure $fSigma 1 -weight 1

	place $fSigma -x 5 -y 15
	
    # ARIMA Information
	CollapsableFrame $fe.cARIMA \
	  -text "" -width 550 -height 220
	set cfARIMA [$fe.cARIMA getframe]
	set fARIMA [frame $cfARIMA.f]
    set widgets(cARIMA) $fe.cARIMA

    radiobutton $fARIMA.rbFactors -text "[mc "Factors"]:" -value "Factors" \
      -variable [myvar submodel_info(arima)] \
      -state disabled \
      -command "$self UpdateArima"
    set widgets(rbfactors) $fARIMA.rbFactors
    
	set ft [frame $fARIMA.cFactors]
	::MDMSubmodelsGui::bmmsfactors $ft.tFactors \
	  -state disabled
    set factors $ft.tFactors
      
    bind $factors <<OnCancel>> "$self Cancel"
    bind $factors <<OnDeleted>> "$self UpdateLabel"
    bind $factors <<OnAdded>> "$self UpdateLabel"
    bind $factors <<OnChanged>> "$self UpdateLabel"
	
    radiobutton $fARIMA.rbLabel -text "[mc "Label"]:" -value "Label" \
      -variable [myvar submodel_info(arima)] \
      -state disabled \
      -command "$self UpdateArima"
    set widgets(rblabel) $fARIMA.rbLabel
	
    entry $fARIMA.eLabel -textvariable [myvar submodel_info(label)] \
      -width 40 -state readonly 
    set widgets(label) $fARIMA.eLabel

    Button $fARIMA.bSync -text [mc "Synchronize"] \
	  -image [::Bitmap::get bundo] -relief link -compound left \
	  -command [list $self UpdateFactors] \
      -state disabled
    set widgets(sync) $fARIMA.bSync

    checkbutton $fARIMA.chkfix -variable [myvar submodel_info(fixed)] \
      -text [mc "Fixed Parameters"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(fixed) $fARIMA.chkfix

    grid $fARIMA.rbFactors -row 0 -column 0 -sticky ne -pady 5 -padx 5
    grid $fARIMA.cFactors  -row 0 -column 1 -columnspan 2 -sticky w
    grid $fARIMA.rbLabel   -row 1 -column 0 -sticky e -pady 5 -padx 5
    grid $fARIMA.eLabel    -row 1 -column 1 -sticky ew
    grid $fARIMA.bSync     -row 1 -column 2 -sticky w
    grid $fARIMA.chkfix    -row 2 -column 1 -sticky w -padx 5 -pady 5
	
    grid rowconfigure    $fARIMA 3 -weight 1
    grid columnconfigure $fARIMA 3 -weight 1

	place $fARIMA -x 5 -y 15
	
    grid $fe.lName      -row 0 -column 0 -sticky e
    grid $fe.eName      -row 0 -column 1 -sticky w
    grid $fe.lType      -row 1 -column 0 -sticky e
    grid $fe.cbType     -row 1 -column 1 -sticky w
    grid $fe.lDesc      -row 2 -column 0 -sticky e
    grid $fe.eDesc      -row 2 -column 1 -sticky w
    grid $fe.lActive    -row 3 -column 0 -sticky e
    grid $fe.chkbActive -row 3 -column 1 -sticky w
    grid $fe.lOut       -row 4 -column 0 -sticky e
    grid $fe.cOutSel    -row 4 -column 1 -sticky w
    grid $fe.lGram      -row 5 -column 0 -sticky e
    grid $fe.eGram      -row 5 -column 1 -sticky w
    grid $fe.lBData     -row 6 -column 0 -sticky e
    grid $fe.fBMatrix   -row 6 -column 1 -sticky w
    grid remove $fe.fBMatrix
    grid $fe.fBSerie     -row 6 -column 1 -sticky w
    grid remove $fe.fBSerie
    grid $fe.lEData     -row 7 -column 0 -sticky e
    grid $fe.fEMatrix   -row 7 -column 1 -sticky w
    grid remove $fe.fEMatrix
    grid $fe.fESerie    -row 7 -column 1 -sticky w
    grid remove $fe.fESerie
    grid $fe.lSigma     -row 8 -column 0 -sticky en
    grid $fe.cSigma     -row 8 -column 1 -sticky w
    grid $fe.lARIMA     -row 9 -column 0 -sticky en
    grid $fe.cARIMA     -row 9 -column 1 -sticky w

    grid rowconfigure    $fe 10 -weight 1
    grid columnconfigure $fe 2 -weight 1
    
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name desc \
                 active begin end \
				 serbegin serend rowbegin rowend colbegin colend \
                 sigma0 sigmaw \
                 label fixed} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             type out \
				 sync} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name desc \
	             out \
				 active begin end \
				 serbegin serend rowbegin rowend colbegin colend \
                 accept cancel \
                 sigma0 sigmaw \
                 label fixed \
				 sync} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  
  #/////////////////////////////////////////////////////////////////////////////
  method ShowFType {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    if {$submodel_info(grammar) eq "Serie" && $submodel_info(type) eq "Linear" } {
      grid $widgets(lARIMA)
      grid $widgets(cARIMA)
    } else {
      grid remove $widgets(cARIMA)
      grid remove $widgets(lARIMA)
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateBeginEnd {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    grid remove $widgets(fbserie)
    grid remove $widgets(feserie)
    grid remove $widgets(fbmatrix)
    grid remove $widgets(fematrix)
    if {$submodel_info(grammar) == "Serie"} {
      grid $widgets(fbserie)
      grid $widgets(feserie)
      if {$submodel_info(begin) == 0} {
        grid $widgets(serbegin)
      } else {
        grid remove $widgets(serbegin)
      }
      if {$submodel_info(end) == 0} {
        grid $widgets(serend)
      } else {
        grid remove $widgets(serend)
      }
    } elseif {$submodel_info(grammar) == "Matrix"} { 
      grid $widgets(fbmatrix)
      grid $widgets(fematrix)
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateVariableGrammar {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetOutput
    set submodel_info(grammar) [::LayerMDMGui::GetMVariableGrammar \
	  $submodel_info(out) [$self cget -container]]
    if {$submodel_info(name) eq ""} {
      set submodel_info(name) $submodel_info(out)
	}
    $self ShowFType
    $self ClearBeginEnd
	$self UpdateBeginEnd
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateArima {} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$submodel_info(arima) eq "Label"} {
      $factors finishediting
      $factors configure -state disabled
	  $widgets(label) configure -state normal
	  $widgets(sync) configure -state normal
    } else {                 #; Factors
      $factors configure -state normal
	  $widgets(label) configure -state readonly
	  $widgets(sync) configure -state disabled
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateFactors {} {
  #/////////////////////////////////////////////////////////////////////////////

    set submodel_info(factors) [list]
    if {$submodel_info(label) ne ""} {
	  set _factors [::LayerMDMGui::GetFactorsFromLabel \
        $submodel_info(label)]

      foreach { {} r } $_factors {
        set block [list]
        foreach { {} t } $r {
          lappend block $t
        }
        lappend submodel_info(factors) $block
      }
    }
    $self SetFactors
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateLabel {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetFactors
	if {[llength $submodel_info(factors)]==0} {
	  set submodel_info(label) ""
	} else {
	  set submodel_info(label) [::LayerMDMGui::GetLabelFromFactors \
        $submodel_info(factors)]
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetOutput {} {
  #/////////////////////////////////////////////////////////////////////////////

    set submodel_info(out) [$widgets(out) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetOutput {} {
  #/////////////////////////////////////////////////////////////////////////////

	$widgets(out) transient configure -container $options(-container)
    $widgets(out) set_info $submodel_info(out)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetFactors {} {
  #/////////////////////////////////////////////////////////////////////////////

    set submodel_info(factors) [$factors get_info "no_resume"]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetFactors {} {
  #/////////////////////////////////////////////////////////////////////////////

    $factors set_info $submodel_info(factors)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearBeginEnd {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set submodel_info(serbegin) "2000/01/01"
    set submodel_info(begin) 1
    set submodel_info(serend) "2010/12/01"
    set submodel_info(end) 1
    set submodel_info(rowbegin) "1"
    set submodel_info(colbegin) "1"
    set submodel_info(rowend) "-1"
    set submodel_info(colend) "-1"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set submodel_info(name) ""
    set submodel_info(desc) ""
    set submodel_info(active) 1

    set submodel_info(sigma0) "?"
    set submodel_info(sigmaw) "0"

    set submodel_info(out) ""
    $self SetOutput
	set submodel_info(grammar) ""
    $self ClearBeginEnd
	$self UpdateBeginEnd
	
    set submodel_info(type) "Linear"

    set submodel_info(label) "PDIFARMA"
    set submodel_info(factors) {}
    $self SetFactors
	set submodel_info(arima) "Factors"
	set submodel_info(fixed) 0
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]
	
    array set submodel_info [LayerMDMGui::GetSubmodel $ident \
      $options(-container)]
		
	$self UpdateBeginEnd

    $self SetOutput
	
    set _factors [list]
    foreach { {} r } $submodel_info(factors) {
      set block [list]
      foreach { {} t } $r {
        lappend block $t
      }
      lappend _factors $block
    }
    set submodel_info(factors) $_factors
    #puts "GetInfo:submodel_info(factors)=$submodel_info(factors)"
    $self SetFactors
	set submodel_info(arima) "Factors"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Submodel"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {type active begin end \
				 serbegin serend \
                 fixed rbfactors rblabel sync} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name desc \
				 rowbegin rowend colbegin colend \
                 sigma0 sigmaw \
                 label} {
      $widgets($w) configure -state readonly
    }
    $widgets(out) button configure -state disabled
    $factors configure -state disabled
	
    $self GetInfo
	$self ShowFType
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Submodel"]
    ::MMSGui::ActivateEdition $win
	
    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc \
                 type active begin end \
				 serbegin serend rowbegin rowend colbegin colend \
                 sigma0 sigmaw \
                 rbfactors rblabel fixed} {
      $widgets($w) configure -state normal
    }
    $widgets(out) button configure -state normal
    $self UpdateArima

    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc \
                 type active begin end \
				 serbegin serend rowbegin rowend colbegin colend \
                 sigma0 sigmaw \
                 rbfactors rblabel fixed} {
      $widgets($w) configure -state normal
    }
    $widgets(out) button configure -state normal
    $self UpdateArima
      
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(name) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Submodel"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
	$self ShowFType

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Submodel"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $self GetOutput
	
	set submodel_info(container) [$self cget -container]

	if {$submodel_info(type) eq "Linear"} {
	  if {$submodel_info(grammar) eq "Serie"} {
        if {$submodel_info(arima) ne "Label"} {
	      $factors finishediting
        }
	  } else {
	    set submodel_info(label) ""
	  }
    } else {
	  set submodel_info(label) ""
	}
	
    if {$options(-state) eq "Edit"} {
	  set submodel_info(ident) [$self cget -item]
      LayerMDMGui::EditSubmodel submodel_info $submodel_info(container)
      
	  set new_ident $submodel_info(name)
	  if {$submodel_info(ident) ne $new_ident} {
	    if {$options(-parent) eq "tree"} {
          array set new_info [LayerMDMGui::GetSubmodel \
            $new_ident [$self cget -container]]
	      set new_absid $new_info(abs_id)
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
	    } else {
          ::MDMSubmodelsGui::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
	    }
	  }

    } elseif {$options(-state) eq "New"} {
      LayerMDMGui::CreateSubmodel submodel_info $submodel_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }

    } else {                             ;# Copy
	  set submodel_info(ident) [$self cget -item]
      LayerMDMGui::CopySubmodel submodel_info $submodel_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
	}
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
