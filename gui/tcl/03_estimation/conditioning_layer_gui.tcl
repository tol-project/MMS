#/////////////////////////////////////////////////////////////////////////////
# FILE:    conditioning_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          the Conditioning of an Estimation
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerConditioningGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetConditioningTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Conditioning 1"
	  value "3"
    }
    {}
    {
      name "Conditioning 2"
	  value "text1"
    }
  }
}

variable conditioning_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetConditioning {estim} {
#/////////////////////////////////////////////////////////////////////////////
  variable conditioning_test
  
  if { $conditioning_test } {
    return [GetConditioningTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetConditioning" \
    "Set" $estim
}

#///////////////////////////////////////////////////////////////////////////
proc GetConditioningHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetConditioningHelp" \
    "Text" "?"
}

variable edit_conditioning_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditConditioning {conditioning_info conditioning} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_conditioning_test
  upvar $conditioning_info conditioning_array
  
  set conditioning_set [TclLst2TolSet $conditioning -level 2]
  #puts "EditConditioning:conditioning_set=$conditioning_set"
  
  if { $edit_conditioning_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditConditioningTest" \
      "Real" conditioning_array $conditioning_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditConditioning" \
      "Real" conditioning_array $conditioning_set
  }
}

}
