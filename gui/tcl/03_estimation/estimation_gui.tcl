#/////////////////////////////////////////////////////////////////////////////
# FILE    : estimation_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Module Estimation Definition layer
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::EstimationsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateEstimationsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateEstimationsListDetails"
  set l_details_frame [frame $f.details_Estimations]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Estimation" \
    -swlist "::EstimationsGui::bmmsestimlist" \
	-fshowitem "::EstimationsGui::_ShowEstimationDetails" \
	-fshowlist "::EstimationsGui::_ShowEstimationsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowEstimationsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateEstimationsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details Init
  $l_details_frame.details FillList
  
  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowEstimationsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  _ShowEstimationHelp  

  _ShowEstimationsListDetails "MMS"
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuEstimations {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Estimation"] \
    -command "::EstimationsGui::NewEstimation" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewEstimation {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowEstimationHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerEstimationsGui::GetEstimationHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateEstimationDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateEstimationDetails"
  set details_frame [frame $f.details_Estimation]
  set view_frame [frame $f.view_Estimation]

  set _details [bmmsestimation $details_frame.details]
  set _view [bmmsestimation $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowEstimationDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateEstimationDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowEstimationDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set ident [$tree item text $id first]

  _ShowEstimationDetails "MMS" $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandEstimationsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _details "no" 

  set _list [LayerEstimationsGui::GetEstimationsList $_details]
  #puts "ExpandEstimationsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    if {$vinfo(saved)} {
      set icon [::Bitmap::get "mms_estimation"]
	} else {
      set icon [::Bitmap::get "mms_estimation-M"]
	}
    set row [list [list $icon $vinfo(ident)] \
			  [list "Estimation"] \
	          [list "EstimationsGui::ExpandEstimation"] \
			  [list "EstimationsGui::ShowEstimationDetails"] \
			  [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandEstimation {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set ident [$tree item text $id 0]
  array set model [LayerEstimationsGui::GetEstimationModel $ident]
  array set settings [LayerEstimationsGui::GetEstimationSettings $ident]
  switch -- $settings(subclass) {
    "BSR" {
      set setting_icon "mms_settings_bsr"
    }
    "MultiMLE" {
      set setting_icon "mms_settings_mle"
    }
    default {
	  set setting_icon "mms_settings"
	}
  }
  
  set _list [list \
	"$model(ident)"      "MDMGui::ExpandModel" \
	                     "MDMGui::ShowModelDetails" \
	                     "$model(abs_id)" \
						 "mms_model" \
						 "Model" \
						 "yes" \
	"Conditioning"       "" \
        	             "ConditioningGui::ShowConditioningDetails" \
	                     "" \
						 "list" \
						 "Conditioning" \
						 "no" \
	"Settings"           "" \
	                     "SettingsGui::ShowSettingsDetails" \
	                     "" \
						 "$setting_icon" \
						 "Settings" \
						 "no" \
  ]

  set hasresults [LayerEstimationsGui::HasResults $ident]
  if {$hasresults} {
    array set model_results [LayerEstimationsGui::GetModelResults $ident]
	
	set _list_results [list \
	  "Parameters"                "" \
	                              "ParameterEstimGui::ShowParameterEstimDetails" \
	                              "" \
		                          "list" \
						          "EstimationParameters" \
								  "no" \
	  "$model_results(ident)"     "ModelResultsGui::ExpandModelResults" \
	                              "" \
	                              "$model_results(abs_id)" \
						          "mms_model_results" \
						          "ModelResults" \
								  "yes" \
    ]
    set _list [concat $_list $_list_results]
  }
  
  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {name fexpand fdetails abs_id icon type button} $_list {
    set row [list [list [::Bitmap::get $icon] [mc $name]] \
			  [list $type] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list ""] \
			  [list $abs_id] \
            ]
    $tree insert $row \
      -at child -relative $id -button $button
  }
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsestimlist {
# PURPOSE : Defines the snit widget used to
#           list the estimations of MMS
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container to wich the estimations belongs
  option -container \
    -default "MMS" 

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mms_estimlist
    # mms_estimlist(list)  - Estimations list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the estimations of MMS
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Button: Refresh
    set fb [frame $f.fb]
  
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Estimations List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1

    # Tree in form of table
    set ft [frame $f.ft]
  
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Version"]] \
        [list text -label [mc "Creation date"]] \
        [list text -label [mc "Modification date"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Source"]] \
        [list text -label [mc "Strategy"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 
      
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1

    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
  
    focus $tree
	
    set mms_estimlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mms_estimlist(list) [LayerEstimationsGui::GetEstimationsList \
	  $options(-details)]
    #puts "FillList mms_estimlist(list)=$mms_estimlist(list)"

    $tree item delete all

    foreach {{} it} $mms_estimlist(list) {
      array set vinfo $it
	  
      if {$vinfo(saved)} {
        set icon [::Bitmap::get "mms_estimation"]
	  } else {
        set icon [::Bitmap::get "mms_estimation-M"]
	  }
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(vers)] \
	      [list $vinfo(dCre)] \
	      [list $vinfo(dMod)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(source)] \
	      [list $vinfo(strategy)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }

      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
             [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsestimation {
# PURPOSE : Defines the snit widget used to
#           create new Estimations or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  typevariable ListTypes
  # Types of Strategy (BSR, MultiMLE, MultiNLO)

  # Identifier of the Estimation to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Esimations List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  

  variable label_state

  variable widgets
    
  variable estimation_info
  # Data of the Estimation to edit
   # (Class @Estimation)            - (: @MainObject)
    # estimation_info(name)         - Name (Text _.name)
    # estimation_info(vers)         - Version (Text _.version)
    # estimation_info(dCre)         - Creation date (Date _.creationTime)
    # estimation_info(dMod)         - (Date _.modificationTime)
    # estimation_info(dNow)         - Current system date
	# estimation_info(saved)        - Is saved? (Real _.isSaved)
	# estimation_info(source)       - (Text _.source)
    #                               - (Copy of the Model)(Set _.model)(@Model)
	# estimation_info(model)        - Model Identifier (Only for New)
    # estimation_info(strategy)     - Strategy (Set _.strategy)(@Strategy) (BSR, MultiMLE)
    # estimation_info(desc)         - Description (H: Text _.description)
    # estimation_info(attr)         - Attributes (H: Set _.attributes)
    # estimation_info(tags)         - Tags (H: Set _.tags)
    # estimation_info(st_tags)      - Tags as text
    #                               - (Set _.conditioning)(SetOf{Real|RandVar::@Real.Random})
    #                               - (Set _.results)

  variable attributes

  component dlg

  delegate method * to hull
  delegate option * to hull

  typeconstructor {

    set ListTypes {BSR MultiMLE MultiNLO}
  }
	
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item 
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a MMS Estimation
  #///////////////////////////////////////////////////////////////////////////
    
    set f $dlg
    
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Estimation"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
    
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Estimation"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Estimation"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1
	
    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lVers -text "[mc "Version"]:" -pady 5 -padx 5
    label $fe.ldCre -text "[mc "Creation date"]:" -pady 5 -padx 5
    label $fe.ldMod -text "[mc "Modification date"]:" -pady 5 -padx 5
    label $fe.lSour -text "[mc "Source"]:" -pady 5 -padx 5
    label $fe.lStrategy -text "[mc "Strategy"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lAttr -text "[mc "Attributes"]:" -pady 5 -padx 5
    label $fe.lModel -text "[mc "Model"]:" -pady 5 -padx 5
    set widgets(lmodel) $fe.lModel
    
    entry $fe.eName -textvariable [myvar estimation_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    entry $fe.eVers -textvariable [myvar estimation_info(vers)] \
      -width 20 -state readonly
    set widgets(vers) $fe.eVers
  
    ::datefield::datefield $fe.dfCre -textvariable [myvar estimation_info(dCre)] \
      -state normal -format y/m/d
    set estimation_info(dNow) $estimation_info(dCre)
    $fe.dfCre configure -state disabled
    set widgets(dCre) $fe.dfCre
    
    ::datefield::datefield $fe.dfMod -textvariable [myvar estimation_info(dMod)] \
      -state normal -format y/m/d
    set estimation_info(dNow) $estimation_info(dMod)
    $fe.dfMod configure -state disabled
    set widgets(dMod) $fe.dfMod
    
    entry $fe.eSour -textvariable [myvar estimation_info(source)] \
      -width 80 -state readonly
    set widgets(source) $fe.eSour
    
    checkbutton $fe.chkbSaved -variable [myvar estimation_info(saved)] \
      -text [mc "Saved"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(saved) $fe.chkbSaved

    ComboBox $fe.cbStrategy -values $ListTypes \
      -textvariable [myvar estimation_info(strategy)] -editable false \
      -width 15 -state disabled
    set widgets(strategy) $fe.cbStrategy
      
    entry $fe.eDesc -textvariable [myvar estimation_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc
    
	CollapsableFrame $fe.cAttr \
	  -text "" -width 460 -height 230
    set widgets(attr) $fe.cAttr
	set ft [$widgets(attr) getframe]
	::MMSAttributesGui::bmmsattributes $ft.fAttr \
	  -state disabled
    set attributes $ft.fAttr
	place $attributes -x 5 -y 15
      
    bind $attributes <<OnCancel>> "$self Cancel"
	
    ::MMSSelectorsGui::comboselector $fe.cModelSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select Model"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "Model" \
	  -swlist "::MDMGui::bmmsmodlist"
    set widgets(model) $fe.cModelSel

    grid $fe.lName      -row 0 -column 0 -sticky e
    grid $fe.eName      -row 0 -column 1 -sticky w
    grid $fe.lVers      -row 1 -column 0 -sticky e
    grid $fe.eVers      -row 1 -column 1 -sticky w
    grid $fe.ldCre      -row 2 -column 0 -sticky e
    grid $fe.dfCre      -row 2 -column 1 -sticky w
    grid $fe.ldMod      -row 3 -column 0 -sticky e
    grid $fe.dfMod      -row 3 -column 1 -sticky w
    grid $fe.lSour      -row 4 -column 0 -sticky e
    grid $fe.eSour      -row 4 -column 1 -sticky w
    grid $fe.chkbSaved  -row 5 -column 1 -sticky w
    grid $fe.lStrategy  -row 6 -column 0 -sticky e
    grid $fe.cbStrategy -row 6 -column 1 -sticky w
    grid $fe.lDesc      -row 7 -column 0 -sticky e
    grid $fe.eDesc      -row 7 -column 1 -sticky w
    grid $fe.lAttr      -row 8 -column 0 -sticky ne
    grid $fe.cAttr      -row 8 -column 1 -sticky w
    grid $fe.lModel     -row 9 -column 0 -sticky e
    grid $fe.cModelSel  -row 9 -column 1 -sticky w

	grid rowconfigure    $fe 10 -weight 1
    grid columnconfigure $fe 2 -weight 1
	
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    foreach {w} {lmodel model} {
      grid remove $widgets($w)
    }

    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name vers desc} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             strategy model attr} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name vers desc \
                 accept cancel \
	             strategy model attr} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetModel {} {
  #/////////////////////////////////////////////////////////////////////////////

    set estimation_info(model) [$widgets(model) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetModel {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(model) transient FillList
    $widgets(model) set_info $estimation_info(model)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetAttributes {} {
  #/////////////////////////////////////////////////////////////////////////////

    set estimation_info(attr) [$attributes get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetAttributes {} {
  #/////////////////////////////////////////////////////////////////////////////

    $attributes set_info $estimation_info(attr)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set estimation_info(name) ""
    set estimation_info(vers) "1.0"
    set estimation_info(dCre) $estimation_info(dNow)
    set estimation_info(dMod) $estimation_info(dNow)
    set estimation_info(desc) ""
    set estimation_info(source) ""
    set estimation_info(saved) 0
	
	set estimation_info(strategy) "BSR"
	
    set estimation_info(model) ""
    $self SetModel
	
    set estimation_info(attr) {}
    $self SetAttributes
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

    array set estimation_info [LayerEstimationsGui::GetEstimation $ident]

    set _attributes [list]
    foreach { {} r } $estimation_info(attr) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _attributes $_row
    }
    set estimation_info(attr) $_attributes
    $self SetAttributes
	
    set estimation_info(model) ""
	$self SetModel
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Estimation"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
	foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {strategy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name vers desc} {
      $widgets($w) configure -state readonly
    }
    $attributes configure -state disabled

    foreach {w} {lmodel model} {
      grid remove $widgets($w)
    }

    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Estimation"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name vers desc} {
      $widgets($w) configure -state normal
    }
    $attributes configure -state normal

    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name vers desc} {
      $widgets($w) configure -state normal
    }
    $attributes configure -state normal

    if {$options(-state) eq "New"} {
      foreach {w} {lmodel model} {
        grid $widgets($w)
      }
      $widgets(model) button configure -state normal
      foreach {w} {strategy} {
        $widgets($w) configure -state normal
      }
      $attributes configure -state normal
	} else {            ;# Copy
      $attributes configure -state disabled
	}

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(vers) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Estimation"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Estimation"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $attributes finishediting
    $self GetAttributes

    if {$options(-state) eq "Edit"} {
	  set estimation_info(ident) [$self cget -item]
      LayerEstimationsGui::EditEstimation estimation_info $estimation_info(attr)

	  set new_ident [LayerMMSGui::NamesToIdentifier \
	    [list $estimation_info(name) $estimation_info(vers)]]

	  if {$estimation_info(ident) ne $new_ident} {
	    if {$options(-parent) eq "tree"} {
          array set new_info [LayerEstimationsGui::GetEstimation \
            $new_ident]
	      set new_absid $new_info(abs_id)
          ::MMSGui::ChangeMMSTree $new_ident $new_absid
	    } else {
          ::EstimationsGui::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
	    }
	  }

    } elseif {$options(-state) eq "New"} {
	  $self GetModel
      LayerEstimationsGui::CreateEstimation estimation_info $estimation_info(attr)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }

    } else {                             ;# Copy
	  set estimation_info(ident) [$self cget -item]
      LayerEstimationsGui::CopyEstimation estimation_info
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
	}
    
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }
  
}

}
