#/////////////////////////////////////////////////////////////////////////////
# FILE:    estimation_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Module Estimation Definition
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerEstimationsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetEstimationTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Estimation 1"
    vers "1.0"
    desc "desc of Estimation 1"
    dCre "2010/08/25"
  }
}
  
variable get_estimation_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetEstimation {ident} {
#///////////////////////////////////////////////////////////////////////////
  variable get_estimation_test
  
  if { $get_estimation_test } {
    return [GetEstimationTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetEstimation" \
    "Set" $ident
}

#/////////////////////////////////////////////////////////////////////////////
proc GetEstimationsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Estimation 1"
      vers "1.0"
    }
    {}
    {
      name "Estimation 2"
      vers "2.0"
    }
  }
}

variable get_estimations_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetEstimationsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_estimations_test
  
  if { $get_estimations_test } {
    return [GetEstimationsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetEstimationsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetEstimationHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetEstimationHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetEstimationModel {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetEstimationModel" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc GetEstimationSettings {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetEstimationSettings" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc GetModelResults {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetModelResults" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc HasResults {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::HasResults" \
    "Real" $ident
}

variable create_estim_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateEstimation {estim_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_estim_test
  upvar $estim_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_estim_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::CreateEstimationTest" \
	  "Real" arrayarg $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::CreateEstimation" \
	  "Real" arrayarg $attributes_set
  }
}

variable copy_estim_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyEstimation {estim_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_estim_test
  upvar $estim_info arrayarg
  
  if { $copy_estim_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::CopyEstimationTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::CopyEstimation" \
	  "Real" arrayarg
  }
}

variable edit_estim_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditEstimation {estim_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_estim_test
  upvar $estim_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $edit_estim_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditEstimationTest" \
	  "Real" arrayarg $attributes_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditEstimation" \
	  "Real" arrayarg $attributes_set
  }
}

}
