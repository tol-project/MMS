#/////////////////////////////////////////////////////////////////////////////
# FILE:    hierarchyresults_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Hierarchy Results of an Estimation
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerHierarchyResultsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchyResultsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "HierarchyResults 1"
    }
    {}
    {
      name "HierarchyResults 2"
    }
  }
}

variable hierarchies_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetHierarchyResultsList {estim} {
#/////////////////////////////////////////////////////////////////////////////
  variable hierarchies_list_test
  
  if { $hierarchies_list_test } {
    return [GetHierarchyResultsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetHierarchyResultsList" \
    "Set" $estim
}

#///////////////////////////////////////////////////////////////////////////
proc GetHierarchyResultsHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetHierarchyResultsHelp" \
    "Text" "?"
}

}
