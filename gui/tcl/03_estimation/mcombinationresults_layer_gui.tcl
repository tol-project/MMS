#/////////////////////////////////////////////////////////////////////////////
# FILE:    mcombinationresults_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MCombination Results of an Estimation
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMCombinationResultsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetMCombinationResultsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "MCombinationResults 1"
    }
    {}
    {
      name "MCombinationResults 2"
    }
  }
}

variable mcombination_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetMCombinationResultsList {estim} {
#/////////////////////////////////////////////////////////////////////////////
  variable mcombination_list_test
  
  if { $mcombination_list_test } {
    return [GetMCombinationResultsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetMCombinationResultsList" \
    "Set" $estim
}

#///////////////////////////////////////////////////////////////////////////
proc GetMCombinationResultsHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetMCombinationResultsHelp" \
    "Text" "?"
}

}
