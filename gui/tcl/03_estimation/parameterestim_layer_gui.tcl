#/////////////////////////////////////////////////////////////////////////////
# FILE:    parameterestim_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Parameters of an Estimation
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerParameterEstimGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetParameterEstimTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "ParameterEstim 1"
	  abs_id ""
    }
    {}
    {
      name "ParameterEstim 2"
	  abs_id ""
    }
  }
}

variable parameter_estim_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetParameterEstim {estim} {
#/////////////////////////////////////////////////////////////////////////////
  variable parameter_estim_test
  
  if { $parameter_estim_test } {
    return [GetParameterEstimTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetParameterEstim" \
    "Set" $estim
}

#///////////////////////////////////////////////////////////////////////////
proc GetParameterEstimHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetParameterEstimHelp" \
    "Text" "?"
}

}
