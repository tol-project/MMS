#/////////////////////////////////////////////////////////////////////////////
# FILE    : settings_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Settings
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::SettingsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateSettingsDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateSettingsDetails"
  set details_frame [frame $f.details_Settings]
  set view_frame [frame $f.view_Settings]

  set _details [bmmssettings $details_frame.details]
  set _view [bmmssettings $view_frame.details]

  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowSettingsDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  set parent [$tree item parent $id]
  set parent_name [$tree item text $parent 0]
  
  set grandparent [$tree item parent $parent]
  set grandparent_name [$tree item text $grandparent 0]
  if {$grandparent_name eq [mc "Estimations"]} {
    set container "Estimation"
  } else {           ;# [mc "Forecasts"]
    set container "Forecast"
  }

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateSettingsDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -parent $parent_name
  
  _ShowSettingsHelp  

  return $_frame
}
  
#/////////////////////////////////////////////////////////////////////////////
proc _ShowSettingsHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerSettingsGui::GetSettingsHelp]

  ::MMSGui::ShowInfo $message
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmssettings {
# PURPOSE : Defines the snit widget used to
#           edit the settings
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # The container to wich the settings belongs (Estimation, Forecast)
  option -container \
    -default "" -configuremethod "_conf-container"  
  # Identifier of the Estimation or Forecast 
  option -parent \
    -default "" -configuremethod "_conf-parent"  

  variable label_state

  variable widgets
    
  variable _settings
  # Settings to be edited at the tablelist
   # Class @Settings
    # Rows
     # one for each Setting
    # Columns
     # 1- Setting name
     # 2- Setting value

  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set _settings [list]

    set tablelist_options(-columns) [list \
      0 [mc "Name"] left \
      0 [mc "Value"] left \
    ]
	
    set tablelist_options(-listvar) [myvar _settings]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Edit
    set fbu [frame $f.fbu]
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Settings"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    grid $fbu.bEdit -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 1 -weight 1
	
    # Entry frame
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    # Tablelist
    set ft [frame $fe.ft]

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 27 -width 90
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "$self Changed"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 1 -column 0 -sticky ew

    grid columnconfigure $ft 0 -weight 1
    grid rowconfigure $ft 0 -weight 1

    $widgets(tbl) columnconfigure 0 -name col_name
    $widgets(tbl) columnconfigure 1 -name col_value -editable yes
    
    grid $fe.ft -sticky news -padx 5 -pady 2

    grid rowconfigure    $fe 1 -weight 1
    grid columnconfigure $fe 0 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
	
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Changed {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
    set changed 1
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
    set changed 0
	
    set l_settings [LayerSettingsGui::GetSettings \
      $options(-container) $options(-parent)]
    #puts "FillList l_settings=$l_settings"

    $widgets(tbl) delete 0 end

    foreach {{} it} $l_settings {
      array set vinfo $it
      set row [list \
	    [list $vinfo(ident)] \
        [list $vinfo(value)] \
	  ]
      $widgets(tbl) insert end $row
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set label_state [mc "Details of the Settings"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {edit} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }

	$self put_state "normal"
    $self FillList
	$self put_state "disabled"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set label_state [mc "Edit Settings"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {edit} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
	$self put_state "normal"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
	::MMSGui::DisactivateEdition
  
    $widgets(tbl) finishediting
	
	if {$changed == 1} {
	  #puts "Ok:_settings=$_settings"
	  set settings_info(container) [$self cget -container]
	  set settings_info(parent) [$self cget -parent]
      LayerSettingsGui::EditSettings settings_info $_settings
	}
    $self Details
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
  
    $widgets(tbl) finishediting
	
    $self Details
  }

}  

}
