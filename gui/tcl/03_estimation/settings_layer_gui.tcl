#/////////////////////////////////////////////////////////////////////////////
# FILE:    settings_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Settings
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerSettingsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetSettingsTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Settings 1"
	  value "3"
    }
    {}
    {
      name "Settings 2"
	  value "text1"
    }
  }
}

variable settings_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetSettings {container parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable settings_test
  
  if { $settings_test } {
    return [GetSettingsTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::EstimationsGui::GetSettingsˇ" \
    "Set" $container $parent
}

#///////////////////////////////////////////////////////////////////////////
proc GetSettingsHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::EstimationsGui::GetSettingsHelp" \
    "Text" "?"
}

variable edit_settings_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditSettings {settings_info settings} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_settings_test
  upvar $settings_info settings_array
  
  set settings_set [TclLst2TolSet $settings -level 2]
  #puts "EditSettings:settings_set=$settings_set"
  
  if { $edit_settings_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditSettingsTest" \
      "Real" settings_array $settings_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::EstimationsGui::EditSettings" \
      "Real" settings_array $settings_set
  }
}

}
