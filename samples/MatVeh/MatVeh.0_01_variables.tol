//////////////////////////////////////////////////////////////////////////////
// FILE:    MatVeh.0_01_variables.tol
// OLDFILE: matVeh_MMS_MDV.tol
// PURPOSE: Definici�n de las variables del ejemplo MatVeh
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Creaci�n de dos datasets para las variables

MMS::@DataSet DS.O = MMS::Container::ReplaceDataSet([[
  Text _.name = "MatVeh.Outputs";
  Text _.description = "Conjunto de datos con las variables output del "
    <<"ejemplo MatVeh."
]]);

MMS::@DataSet DS.I = MMS::Container::ReplaceDataSet([[
  Text _.name = "MatVeh.Inputs";
  Text _.description = "Conjunto de datos con las variables input del "
    <<"ejemplo MatVeh."
]]);
// Intervalo para las variables input definidas como series infinitas
Real DS.I::AppendInterval("Mensual", y1980, y2012);

//////////////////////////////////////////////////////////////////////////////
// Variables output

Set EvalSet(SetTipVeh, Anything (Set reg) {
  Text code = reg[1];
  Text name = reg[2];
  Anything DS.O::CreateVariable([[
    Text _.name = code;
    Text _.description = 
      "Serie mensual de veh�culos matriculados de tipo "<<name;
    Text _.expression = 
      "Serie AlgGetDataMatVeh("<<Qt(code)<<", Empty)";
    Set _.tags = SetOfText("matriculaci�n", "veh�culos")
  ]])
});

//////////////////////////////////////////////////////////////////////////////
// Variables Input

Anything DS.I::ReplaceVariable([[
  Text _.name = "EU.created";
  Text _.description = 
    "EU setup";
  Text _.expression = 
    "Serie Pulse(y1993m01d01, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "SpaJoiEEC";
  Text _.description = 
    "Spain joins EEC";
  Text _.expression = 
    "Serie Pulse(y1986m01d01, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "Crisis.200801";
  Text _.description = 
    "Crisis 200801";
  Text _.expression = 
    "Serie Step(y2008m01d01, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "Crisis.200805";
  Text _.description = 
    "Crisis 200805";
  Text _.expression = 
    "Serie Step(y2008m05d01, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "Pib.es.Men";
  Text _.description = 
    "PIB Espa�a Mensual";
  Text _.expression = 
    "Serie InterpolaMMS(B:BaseIndex(ExtendYears("
    "Pib.es.TasVarInterTrimestral, 1, 2)), Mensual);"
]]);

// Input PIB transformado
Anything DS.I::ReplaceVariable([[
  Text _.name = "Pib.es.Men_LogM";
  Text _.expression = "Serie Log(�1) - FirstS(Log(�1))";
  Set _.dependences = [["Pib.es.Men"]]
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "Vig.Imp.Mat";
  Text _.description = 
    "Vigencia del impuesto de matriculaci�n";
  Text _.expression = 
    "Serie Step(y1993m01d01, Mensual)"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "Lab.es.Men";
  Text _.description = "Indicador mensual de laborables en Espa�a";
  Text _.expression = 
    "Serie CalVar(C-WD(6)-WD(7)-CtFes.es, Mensual);"
]]);
  
// Plan Renove
Anything DS.I::ReplaceVariable([[
  Text _.name = "PlanRenoveI";
  Text _.description = 
    "Indicador mensual del Plan Renove I" "\n"
    "http://www.elmundo.es/elmundomotor/2002/10/20/usuarios/1035136728.html";
  Text _.expression = 
    "Serie CalInd(CtPlanRenoveI, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "PlanRenoveII";
  Text _.description = 
    "Indicador mensual del Plan Renove II" "\n"
    "http://www.elpais.com/articulo/economia/"
      "MINISTERIO_DE_INDUSTRIA_Y_ENERGiA/PODER_EJECUTIVO/_GOBIERNO_PSOE_/"
      "1993-1996/Plan/Renove/amplia/coches/anos/elpepieco/"
      "19941001elpepieco_2/Tes/\n"
    "http://www.elmundo.es/elmundomotor/2002/08/20/usuarios/1029869037.html";
  Text _.expression = 
    "Serie CalInd(CtPlanRenoveII, Mensual);"
]]);

// Input inventado para introducir como ejemplo de inputs en previsi�n
MMS::@VariableI _variable = DS.I::ReplaceVariable([[
  Text _.name = "PlanRenoveIII";
  Text _.description = 
    "Indicador mensual del Plan Renove III" "\n"
    "Inventado para introducir como ejemplo de inputs en previsi�n";
  Text _.expression = 
    "Serie Pulse(y2009m08d01, Mensual);"
]]);
// aleatorio
Anything _variable::ReplaceVScenario([[
  Text _.name = "Random";
  Text _.description = 
    "Indicador mensual del Plan Renove III" "\n"
    "Inventado para introducir como ejemplo de inputs en previsi�n";
  Text _.type = "RandVar::@Serie.Normal";
  Text _.expression = 
    "RandVar::@Serie.Normal::Default(Pulse(y2009m08d01, Mensual), " 
    "Pulse(y2009m08d01, Mensual)*0.1);"
]]);

// Plan Prever
Anything DS.I::ReplaceVariable([[
  Text _.name = "PlanPreverI";
  Text _.description = 
    "Indicador mensual del Plan Prever I" "\n"
    "http://www.elmundo.es/elmundomotor/2002/08/20/usuarios/1029869037.html";
  Text _.expression = 
    "Serie CalInd(CtPlanPreverI, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "PlanPreverII";
  Text _.description = 
    "Indicador mensual del Plan Prever II" "\n"
    "http://www.elmundo.es/elmundomotor/2003/11/25/usuarios/1069779423.html";
  Text _.expression = 
    "Serie CalInd(CtPlanPreverII, Mensual);"
]]);

Anything DS.I::ReplaceVariable([[
  Text _.name = "PlanPreverIII";
  Text _.description = 
    "Indicador mensual del Plan Prever III" "\n"
    "http://www.elmundo.es/elmundomotor/2004/09/29/usuarios/1096444416.html";
  Text _.expression = 
    "Serie CalInd(CtPlanPreverIII, Mensual);"
]]);

//////////////////////////////////////////////////////////////////////////////


