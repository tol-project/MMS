//////////////////////////////////////////////////////////////////////////////
// FILE:    MatVeh.1_E_03_estimation.tol
// OLDFILE: matVeh_MMS_MEM_Estimate.tol
// PURPOSE: Estimaci�n del primer modelo con Estimate.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Estimaci�n del modelo

MMS::@Estimation estimation = MMS::Container::ReplaceEstimation([[
  Text _.name = "Mat.Veh_Estimate";
  MMS::@Model _.model = MMS::Container::GetModel([["Mat.Veh","1.0"]]);
  MMS::@SettingsMultiMLE _.settings = [[
    Real _.showTraces = False // True
  ]]
]]);

// Ejecuci�n de la estimaci�n:
Real estimation::Execute(?);

//////////////////////////////////////////////////////////////////////////////
// Acceso a resultados:

// Par�metros estimados
Set rPars = estimation::GetParameters(?);

// Tabla con la informaci�n de los par�metros estimados
Set rPars.Table = EvalSet(rPars, Set(Anything rPar) {
  Set {[[ 
    Text identifier = Name(rPar);
    Real value = If(RandVar::IsRandom(rPar), rPar::GetMean(?), rPar);
    Real sigma = If(RandVar::IsRandom(rPar), rPar::GetSigma(?), ?)
  ]]}
});

MMS::@Model.Results modelR = estimation::GetModel.Results(?);

//////////////////////////////////////////////////////////////////////////////
// Submodelo "Veh.Tur.Mat"
// Usaremos a modo de ejemplo el submodelo "Veh.Tur.Mat".

MMS::@Submodel.Results submodelR = modelR::GetSubmodel("Veh.Tur.Mat");

// Resultados principales
Serie submodelR::GetOutput(?);
Serie submodelR::GetNoise(?);
Serie submodelR::GetFilter(?);
Serie submodelR::GetPrediction(?);
Serie submodelR::GetResiduals(?);

// Efectos relativos a cada t�rmino explicativo
Set submodelR::GetEffects(?);

// Resultados extendidos
Serie submodelR::GetOutput.Extended(?);
Serie submodelR::GetFilter.Extended(?);

// La estructura ARIMA
Set submodelR.arima = submodelR::GetARIMA(?);
Polyn ARIMAGetARI(submodelR.arima);
Polyn ARIMAGetMA(submodelR.arima);

// El par�metro sigma2 del modelo
Anything submodelR.sigma2 = submodelR::GetParameterSigma2(?)::GetValue(?);

//////////////////////////////////////////////////////////////////////////////
