//////////////////////////////////////////////////////////////////////////////
// FILE:    MatVeh.2_B_03_estimation.tol
// PURPOSE: Estimaci�n del segundo modelo con BSR.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Estimaci�n del modelo
MMS::@Estimation estBSR2 =  MMS::Container::ReplaceEstimation([[
  Text _.name = "Mat.Veh.2_BSR";
  MMS::@Model _.model = MMS::Container::GetModel("Mat.Veh.2");
  MMS::@SettingsBSR _.settings = [[
    // Se usa un valor bajo de 'mcmc.sampleLength' para agilizar pruebas
    Real mcmc.sampleLength = 100
  ]]
]]);

// Ejecuci�n de la estimaci�n:
Real estBSR2::Execute(?);

//////////////////////////////////////////////////////////////////////////////
// Acceso a resultados:

// Par�metros estimados
Set rPars2.bsr = estBSR2::GetParameters(?);

// Tabla con la informaci�n de los par�metros estimados
Set rPars2.bsr.Table = EvalSet(rPars2.bsr, Set(Anything rPar) {
  Set {[[ 
    Text identifier = Name(rPar);
    Real value = If(RandVar::IsRandom(rPar), rPar::GetMean(?), rPar);
    Real sigma = If(RandVar::IsRandom(rPar), rPar::GetSigma(?), ?)
  ]]}
});

MMS::@Model.Results modelR2.bsr = estBSR2::GetModel.Results(?);

//////////////////////////////////////////////////////////////////////////////
// Submodelo "Veh.XXX.Mat"

MMS::@Submodel.Results submodelR2.bsr = modelR2.bsr::GetSubmodel("Veh.XXX.Mat");

// Resultados principales
Serie submodelR2.bsr::GetOutput(?);
Serie submodelR2.bsr::GetNoise(?);
Serie submodelR2.bsr::GetFilter(?);
Serie submodelR2.bsr::GetPrediction(?);
Serie submodelR2.bsr::GetResiduals(?);

// Efectos relativos a cada t�rmino explicativo
Set submodelR2.bsr::GetEffects(?);

// Resultados extendidos
Serie submodelR2.bsr::GetOutput.Extended(?);
Serie submodelR2.bsr::GetFilter.Extended(?);

// La estructura ARIMA:
Set submodelR2.arima.bsr = submodelR2.bsr::GetARIMA(?);
Polyn ARIMAGetARI(submodelR2.arima.bsr);
Polyn ARIMAGetMA(submodelR2.arima.bsr);

// El par�metro sigma2 del modelo
Anything submodelR2.sigma2.bsr = 
  submodelR2.bsr::GetParameterSigma2(?)::GetValue(?);

//////////////////////////////////////////////////////////////////////////////
