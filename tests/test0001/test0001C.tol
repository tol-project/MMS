
//////////////////////////////////////////////////////////////////////////////
// Test0001C
// Se comprueba si el m�todo SetName funciona adecuadamente en el
// m�dulo de modelos.
// Se crea un modelo con al menos una cosa de cada tipo
// V�ase el tique #528
//////////////////////////////////////////////////////////////////////////////

MMS::@Model model = MMS::Container::CreateModel([[
  Text _.name = "Model"
]]);

MMS::@DataSet dataSet = model::GetDataSet(?);

MMS::@Variable variable1 = dataSet::CreateVariable([[
  Text _.name = "Variable1";
  Text _.type = "Serie"
]]);
MMS::@Variable variable1b = dataSet::CreateVariable([[
  Text _.name = "Variable1b";
  Text _.type = "Serie";
  Real _.isTrivial = True;
  Text _.expression = "Serie _1 * 2;";
  Set _.dependences = [[ "Variable1" ]]
]]);
MMS::@Variable variable2 = dataSet::CreateVariable([[
  Text _.name = "Variable2";
  Text _.type = "Serie"
]]);
MMS::@Variable variableM1 = dataSet::CreateVariable([[
  Text _.name = "VariableM1";
  Text _.type = "Matrix"
]]);
MMS::@Variable variableM2 = dataSet::CreateVariable([[
  Text _.name = "VariableM2";
  Text _.type = "Matrix"
]]);

MMS::@MVariable mVariable1 = model::CreateMVariable([[
  Text _.name = "MVariable1";
  Text _.variable = "Variable1"
]]);
MMS::@MVariable mVariable1b = model::CreateMVariable([[
  Text _.name = "MVariable1b";
  Text _.variable = "Variable1b"
]]);
MMS::@ParameterMissing parameterMissing = mVariable1b::CreateParameterMissing([[
  Date _.position = y2000
]]);
MMS::@MVariable mVariable2 = model::CreateMVariable([[
  Text _.name = "MVariable2";
  Text _.variable = "Variable2"
]]);
MMS::@MVariable mVariableM1 = model::CreateMVariable([[
  Text _.name = "MVariableM1";
  Text _.variable = "VariableM1"
]]);
MMS::@MVariable mVariableM2 = model::CreateMVariable([[
  Text _.name = "MVariableM2";
  Text _.variable = "VariableM2"
]]);

MMS::@Submodel submodel = model::CreateSubmodel([[
  Text _.name = "Submodel";
  Text _.outputName = "MVariable1";
  NameBlock _.noise = [[
    Text _.type = "ARIMA";
    Text _.arimaLabel = "P1DIF1AR2MA3"
  ]]
]]);
MMS::@ExpTerm expTerm = submodel::CreateExpTerm_TransferFunction([[
  Text _.name = "ExpTerm";
  Text _.inputName = "MVariable2";
  Polyn _.transferFunction = 0.1 + 0.1*B
]]);
MMS::@ExpTerm expTerm2 = submodel::CreateExpTerm_Coefficient([[
  Text _.name = "ExpTerm2";
  Text _.inputName = "MVariable1b";
  Real _.coefficient = 0.1
]]);

MMS::@Submodel submodelM = model::CreateSubmodel([[
  Text _.name = "SubmodelM";
  Text _.outputName = "MVariableM1";
  NameBlock _.noise = [[
    Text _.type = "Normal"
  ]]
]]);
MMS::@ExpTerm expTermM = submodelM::CreateExpTerm_Coefficient([[
  Text _.name = "ExpTermM";
  Text _.inputName = "MVariableM2";
  Real _.coefficient = 0.1
]]);

MMS::@MEquivalence mEquivalence = model::CreateMEquivalence([[
  Text _.name = "MEquivalence";
  Set _.parameters = [[ expTerm::GetParameterLinear(1), 
    expTermM::GetParameterLinear(1) ]]
]]);

MMS::@MCombination mCombination = model::CreateMCombination([[
  Text _.name = "MCombination";
  Set _.parameters = [[ expTerm::GetParameterLinear(1), 
    expTerm::GetParameterLinear(2) ]];
  Set _.coefficients = [[ 1, 1 ]]
]]);

MMS::@Hierarchy hierarchy = model::CreateHierarchy([[
  Text _.name = "Hierarchy";
  Set _.mElements = [[ mCombination, 
    expTerm2::GetParameterLinear(1) ]];
  NameBlock _.noise = [[
    Text _.type = "Normal"
  ]]
]]);
MMS::@HierarchyTerm hierarchyTerm = hierarchy::CreateHierarchyTerm([[
  Text _.name = "HierarchyTerm"
]]);

//////////////////////////////////////////////////////////////////////////////
// Se comprueba que al menos las llamadas no producen errores

Real mVariable1::SetName("A");
Real mVariable1b::SetName("B");
Real mVariable2::SetName("C");
Real mVariableM1::SetName("D");
Real mVariableM2::SetName("E");
Real submodel::SetName("F");
Real submodel::SetName("G");
Real submodel::SetName("H");
Real submodelM::SetName("I");
Real hierarchy::SetName("J");
Real expTerm::SetName("K");
Real expTerm2::SetName("L");
Real expTermM::SetName("M");
Real hierarchyTerm::SetName("N");
Real mEquivalence::SetName("O");
Real mCombination::SetName("P");

Real model::Delete(?);

//////////////////////////////////////////////////////////////////////////////
